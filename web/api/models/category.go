package models

import (
	"gitlab.com/abibino-lab/mobal/web/api/middleware/mymongo"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// New is a function for ...
func (cat *Category) New() {
	cat.ID = bson.NewObjectId()
	cat.CreatedAt = cat.ID.Time()
	cat.UpdatedAt = cat.ID.Time()
}

// Insert a function for add new data category
func (cat *Category) Insert() (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()
	// Collection Item
	c := ds.Session.DB("mobal").C("categories")

	// Index
	index := mgo.Index{
		Key:        []string{"node", "parentNode", "name", "keyword", "languageCulture", "languageCultureSort"},
		Unique:     false,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	err = c.EnsureIndex(index)
	code = -1

	if err != nil {
		return code, err
	}

	err = c.Insert(cat)

	if err != nil {
		if mgo.IsDup(err) {
			return ErrDupRows, err
		}
		return ErrDatabase, err
	}

	return 0, err
}

// GetParent a function for add new data category
func (cat *Category) GetParent() (res Category, err error) {
	return res, nil
}
