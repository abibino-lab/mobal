package spider

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"path"
	"strings"
	"sync"
	"time"
	"unsafe"

	"golang.org/x/net/html/charset"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/app/pipeline/collector/data"
	"gitlab.com/abibino-lab/mobal/common/goquery"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/logs"
)

// Context is a struct ...
type Context struct {
	spider   *Spider           // rules
	Request  *request.Request  // original request
	Response *http.Response    // response stream, where URL is copied from * request.Request
	text     []byte            // download the body of the byte stream format
	dom      *goquery.Document // Download the contents of the body when the html can be converted to Dom object
	items    []data.DataCell   // store the result data output in text form
	files    []data.FileCell   // store the file you want to export directly ("name": string; "body": io.ReadCloser)
	err      error             // error mark
	sync.Mutex
}

var (
	contextPool = &sync.Pool{
		New: func() interface{} {
			return &Context{
				items: []data.DataCell{},
				files: []data.FileCell{},
			}
		},
	}
)

// **************************************** Initialization ******************************************* \\

func GetContext(sp *Spider, req *request.Request) *Context {
	ctx := contextPool.Get().(*Context)
	ctx.spider = sp
	ctx.Request = req
	return ctx
}

func PutContext(ctx *Context) {
	ctx.items = ctx.items[:0]
	ctx.files = ctx.files[:0]
	ctx.spider = nil
	ctx.Request = nil
	ctx.Response = nil
	ctx.text = nil
	ctx.dom = nil
	ctx.err = nil
	contextPool.Put(ctx)
}

func (context *Context) SetResponse(resp *http.Response) *Context {
	context.Response = resp
	return context
}

// mark the download error.
func (context *Context) SetError(err error) {
	context.err = err
}

// **************************************** Set with Exec class method **************************************** \\

// generate and add a request to the queue.
// Request.URL and Request.Rule must be set.
// Request.Spider does not need to be set manually (set by the system automatically).
// Request.EnableCookie is set in the Spider field and invalidated in the rule request.
// The following fields have default values, not set:
// Request.Method defaults to the GET method;
// Request.DialTimeout defaults to the constant request.DefaultDialTimeout, less than 0 does not limit the waiting time;
// Request.ConnTimeout defaults to the constant request.DefaultConnTimeout, less than 0 when the download timeout is not restricted;
// Request.TryTimes defaults to the request request.DefaultTryTimes, less than 0 does not limit the number of failed overloads;
// Request.RedirectTimes by default does not limit the number of redirects, less than 0 to prohibit redirects;
// Request.RetryPause defaults to constant request.DefaultRetryPause;
// Request.DownloaderID specified downloader ID, 0 for the default Surf high concurrent downloader, full functionality, 1 for the PhantomJS downloader, features strong break, slow, low concurrent.
// default auto fill Referer.
func (context *Context) AddQueue(req *request.Request) *Context {
	// If the task has been terminated, the crash crawler association
	context.spider.tryPanic()

	err := req.
		SetSpiderName(context.spider.GetName()).
		SetEnableCookie(context.spider.GetEnableCookie()).
		Prepare()

	if err != nil {
		logs.Log.Error(err.Error())
		return context
	}

	// automatically set Referer
	if req.GetReferer() == "" && context.Response != nil {
		req.SetReferer(context.GetURL())
	}

	context.spider.RequestPush(req)
	return context
}

// for dynamic rule add request.
func (context *Context) JsAddQueue(jreq map[string]interface{}) *Context {
	// If the task has been terminated, the crash crawler association
	context.spider.tryPanic()

	req := &request.Request{}
	u, ok := jreq["URL"].(string)
	if !ok {
		return context
	}
	req.URL = u
	req.Rule, _ = jreq["Rule"].(string)
	req.Method, _ = jreq["Method"].(string)
	req.Header = http.Header{}
	if header, ok := jreq["Header"].(map[string]interface{}); ok {
		for k, values := range header {
			if vals, ok := values.([]string); ok {
				for _, v := range vals {
					req.Header.Add(k, v)
				}
			}
		}
	}
	req.PostData, _ = jreq["PostData"].(string)
	req.Reloadable, _ = jreq["Reloadable"].(bool)
	if t, ok := jreq["DialTimeout"].(int64); ok {
		req.DialTimeout = time.Duration(t)
	}
	if t, ok := jreq["ConnTimeout"].(int64); ok {
		req.ConnTimeout = time.Duration(t)
	}
	if t, ok := jreq["RetryPause"].(int64); ok {
		req.RetryPause = time.Duration(t)
	}
	if t, ok := jreq["TryTimes"].(int64); ok {
		req.TryTimes = int(t)
	}
	if t, ok := jreq["RedirectTimes"].(int64); ok {
		req.RedirectTimes = int(t)
	}
	if t, ok := jreq["Priority"].(int64); ok {
		req.Priority = int(t)
	}
	if t, ok := jreq["DownloaderID"].(int64); ok {
		req.DownloaderID = int(t)
	}
	if t, ok := jreq["Temp"].(map[string]interface{}); ok {
		req.Temp = t
	}

	err := req.
		SetSpiderName(context.spider.GetName()).
		SetEnableCookie(context.spider.GetEnableCookie()).
		Prepare()

	if err != nil {
		logs.Log.Error(err.Error())
		return context
	}

	if req.GetReferer() == "" && context.Response != nil {
		req.SetReferer(context.GetURL())
	}

	context.spider.RequestPush(req)
	return context
}

// Output text results.
// item type is map [int] interface {}, according to ruleName existing ItemFields field output,
// When the item type is map [string] interface {}, the ItemFields field that does not exist for ruleName will be automatically added,
// When the rule name is empty, the default current rule.
func (context *Context) Output(item interface{}, ruleName ...string) {
	_ruleName, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("Spider %s calls Output(), the specified rule name does not exist!", context.spider.GetName())
		return
	}
	var _item map[string]interface{}
	switch item2 := item.(type) {
	case map[int]interface{}:
		_item = context.CreatItem(item2, _ruleName)
	case request.Temp:
		for k := range item2 {
			context.spider.UpsertItemField(rule, k)
		}
		_item = item2
	case map[string]interface{}:
		for k := range item2 {
			context.spider.UpsertItemField(rule, k)
		}
		_item = item2
	}
	context.Lock()
	if context.spider.NotDefaultField {
		context.items = append(context.items, data.GetDataCell(_ruleName, _item, "", "", ""))
	} else {
		context.items = append(context.items, data.GetDataCell(_ruleName, _item, context.GetURL(), context.GetReferer(), time.Now().Format("2006-01-02 15:04:05")))
	}
	context.Unlock()
}

// FileOutput ...
// name Specifies the file name, which is the default to keep the original file name unchanged.
func (context *Context) FileOutput(name ...string) {
	// read the full file stream
	bytes, err := ioutil.ReadAll(context.Response.Body)
	context.Response.Body.Close()
	if err != nil {
		panic(err.Error())
		return
	}

	// Smart set the full file name
	_, s := path.Split(context.GetURL())
	n := strings.Split(s, "?")[0]

	baseName := strings.Split(n, ".")[0]

	var ext string
	if len(name) > 0 {
		p, n := path.Split(name[0])
		if baseName2 := strings.Split(n, ".")[0]; baseName2 != "" {
			baseName = p + baseName2
		}
		ext = path.Ext(n)
	}
	if ext == "" {
		ext = path.Ext(n)
	}
	if ext == "" {
		ext = ".html"
	}

	// Save to file temporary queue
	context.Lock()
	context.files = append(context.files, data.GetFileCell(context.GetRuleName(), baseName+ext, bytes))
	context.Unlock()
}

// CreatItem -> Generate text results.
// specify the matching ItemFields field with ruleName, and defaults to the current rule when it is empty.
func (context *Context) CreatItem(item map[int]interface{}, ruleName ...string) map[string]interface{} {
	_, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("Spider %s call CreatItem(), the specified rule name does not exist!", context.spider.GetName())
		return nil
	}

	var item2 = make(map[string]interface{}, len(item))
	for k, v := range item {
		field := context.spider.GetItemField(rule, k)
		item2[field] = v
	}
	return item2
}

// Save the temporary data in the request.
func (context *Context) SetTemp(key string, value interface{}) *Context {
	context.Request.SetTemp(key, value)
	return context
}

func (context *Context) SetURL(url string) *Context {
	context.Request.URL = url
	return context
}

func (context *Context) SetReferer(referer string) *Context {
	context.Request.Header.Set("Referer", referer)
	return context
}

// dynamically append the result field name to the specified rule and get the index position,
// already exists to get the original index position,
// If ruleName is empty, the default is the current rule.
func (context *Context) UpsertItemField(field string, ruleName ...string) (index int) {
	_, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("Spider %s calls UpsertItemField() when the specified rule name does not exist!", context.spider.GetName())
		return
	}
	return context.spider.UpsertItemField(rule, field)
}

// call the specified function under the auxiliary function AidFunc ().
// specify the matching AidFunc with ruleName, and defaults to the current rule when it is empty.
func (context *Context) Aid(aid map[string]interface{}, ruleName ...string) interface{} {
	// If the task has been terminated, the crash crawler association
	context.spider.tryPanic()

	_, rule, found := context.getRule(ruleName...)
	if !found {
		if len(ruleName) > 0 {
			logs.Log.Error("Call spider %s does not exist for the rule: %s", context.spider.GetName(), ruleName[0])
		} else {
			logs.Log.Error("Call spider %s when Aid() is not specified for the rule name", context.spider.GetName())
		}
		return nil
	}
	if rule.AidFunc == nil {
		logs.Log.Error("Spider %s rule %s not defined AidFunc", context.spider.GetName(), ruleName[0])
		return nil
	}
	return rule.AidFunc(context, aid)
}

// parse the response flow.
// specify the matching ParseFunc field with ruleName, and the default call to Root ().
func (context *Context) Parse(ruleName ...string) *Context {
	// If the task has been terminated, the crash crawler association
	context.spider.tryPanic()

	_ruleName, rule, found := context.getRule(ruleName...)
	if context.Response != nil {
		context.Request.SetRuleName(_ruleName)
	}
	if !found {
		context.spider.RuleTree.Root(context)
		return context
	}
	if rule.ParseFunc == nil {
		logs.Log.Error("Spider %s rule %s not defined ParseFunc", context.spider.GetName(), ruleName[0])
		return context
	}
	rule.ParseFunc(context)
	return context
}

// set the custom configuration.
func (context *Context) SetKeyIn(keyIn string) *Context {
	context.spider.SetKeyIn(keyIn)
	return context
}

// set the acquisition limit.
func (context *Context) SetLimit(max int) *Context {
	context.spider.SetLimit(int64(max))
	return context
}

// Custom pause interval (random: Pausetime / 2 ~ Pausetime * 2), higher priority than external pass.
// overwrite existing values ​​if and only if runtime [0] is true.
func (context *Context) SetPausetime(pause int64, runtime ...bool) *Context {
	context.spider.SetPausetime(pause, runtime...)
	return context
}

// set the timer,
// @id is a unique identifier for the timer,
// @ Bell == nil when the countdown, then @ tol for sleep long,
// @bell! = Nil for the alarm, this time @ tol used to specify the time to wake up (from now encountered from the first bell to the bell).
func (context *Context) SetTimer(id string, tol time.Duration, bell *Bell) bool {
	return context.spider.SetTimer(id, tol, bell)
}

// Start the timer and get if the timer can continue to use.
func (context *Context) RunTimer(id string) bool {
	return context.spider.RunTimer(id)
}

// reset the downloaded text content,
func (context *Context) ResetText(body string) *Context {
	x := (*[2]uintptr)(unsafe.Pointer(&body))
	h := [3]uintptr{x[0], x[1], x[1]}
	context.text = *(*[]byte)(unsafe.Pointer(&h))
	context.dom = nil
	return context
}

// **************************************** Get class public method **** *************************************** \\

// Get download error.
func (context *Context) GetError() error {
	// If the task has been terminated, the crash crawler association
	context.spider.tryPanic()
	return context.err
}

// Get the log interface instance.
func (*Context) Log() logs.Logs {
	return logs.Log
}

// Get the spider name.
func (context *Context) GetSpider() *Spider {
	return context.spider
}

// Get the response flow.
func (context *Context) GetResponse() *http.Response {
	return context.Response
}

// Get the response status code.
func (context *Context) GetStatusCode() int {
	return context.Response.StatusCode
}

// Get the original request.
func (context *Context) GetRequest() *request.Request {
	return context.Request
}

// Get a copy of the original request.
func (context *Context) CopyRequest() *request.Request {
	return context.Request.Copy()
}

// Get the list of result field names.
func (context *Context) GetItemFields(ruleName ...string) []string {
	_, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("Spider %s call GetItemFields(), the specified rule name does not exist!", context.spider.GetName())
		return nil
	}
	return context.spider.GetItemFields(rule)
}

// By the index index to obtain the result field name, do not exist when the empty string,
// If ruleName is empty, the default is the current rule.
func (context *Context) GetItemField(index int, ruleName ...string) (field string) {
	_, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("When the spider %s calls GetItemField(), the specified rule name does not exist", context.spider.GetName())
		return
	}
	return context.spider.GetItemField(rule, index)
}

// Get the index subscript from the result field name, the index is -1 when there is no,
// If ruleName is empty, the default is the current rule.
func (context *Context) GetItemFieldIndex(field string, ruleName ...string) (index int) {
	_, rule, found := context.getRule(ruleName...)
	if !found {
		logs.Log.Error("Spider %s call GetItemField(), the specified rule name does not exist!", context.spider.GetName())
		return
	}
	return context.spider.GetItemFieldIndex(rule, field)
}

func (context *Context) PullItems() (ds []data.DataCell) {
	context.Lock()
	ds = context.items
	context.items = []data.DataCell{}
	context.Unlock()
	return
}

func (context *Context) PullFiles() (fs []data.FileCell) {
	context.Lock()
	fs = context.files
	context.files = []data.FileCell{}
	context.Unlock()
	return
}

// Get the custom configuration.
func (context *Context) GetKeyIn() string {
	return context.spider.GetKeyIn()
}

// Get the acquisition limit.
func (context *Context) GetLimit() int {
	return int(context.spider.GetLimit())
}

// Get the spider name.
func (context *Context) GetName() string {
	return context.spider.GetName()
}

// Get the rule tree.
func (context *Context) GetRules() map[string]*Rule {
	return context.spider.GetRules()
}

// Get the specified rule.
func (context *Context) GetRule(ruleName string) (*Rule, bool) {
	return context.spider.GetRule(ruleName)
}

// Get the current rule name.
func (context *Context) GetRuleName() string {
	return context.Request.GetRuleName()
}

// Get the temporary cache data in the request
// defaultValue can not interface {} (nil)
func (context *Context) GetTemp(key string, defaultValue interface{}) interface{} {
	return context.Request.GetTemp(key, defaultValue)
}

// Get all the cached data in the request
func (context *Context) GetTemps() request.Temp {
	return context.Request.GetTemps()
}

// Get a copy of the requested cache data.
func (context *Context) CopyTemps() request.Temp {
	temps := make(request.Temp)
	for k, v := range context.Request.GetTemps() {
		temps[k] = v
	}
	return temps
}

// Get the URL from the original request to ensure that the URL before and after the request is exactly equal and the Chinese is not encoded.
func (context *Context) GetURL() string {
	return context.Request.URL
}

func (context *Context) GetMethod() string {
	return context.Request.GetMethod()
}

func (context *Context) GetHost() string {
	return context.Response.Request.URL.Host
}

// Get the response header information.
func (context *Context) GetHeader() http.Header {
	return context.Response.Header
}

// Get the request header information.
func (context *Context) GetRequestHeader() http.Header {
	return context.Response.Request.Header
}

func (context *Context) GetReferer() string {
	return context.Response.Request.Header.Get("Referer")
}

// Get the response to the cookie.
func (context *Context) GetCookie() string {
	return context.Response.Header.Get("Set-Cookie")
}

// GetHtmlParser returns goquery object binded to target crawl result.
func (context *Context) GetDom() *goquery.Document {
	if context.dom == nil {
		context.initDom()
	}
	return context.dom
}

// GetBodyStr returns plain string crawled.
func (context *Context) GetText() string {
	if context.text == nil {
		context.initText()
	}
	return util.Bytes2String(context.text)
}

// **************************************** Private Method ************************************* \\

// Get rules.
func (context *Context) getRule(ruleName ...string) (name string, rule *Rule, found bool) {
	if len(ruleName) == 0 {
		if context.Response == nil {
			return
		}
		name = context.GetRuleName()
	} else {
		name = ruleName[0]
	}
	rule, found = context.spider.GetRule(name)
	return
}

// GetHtmlParser returns goquery object binded to target crawl result.
func (context *Context) initDom() *goquery.Document {
	if context.text == nil {
		context.initText()
	}
	var err error
	context.dom, err = goquery.NewDocumentFromReader(bytes.NewReader(context.text))
	if err != nil {
		panic(err.Error())
	}
	return context.dom
}

// GetBodyStr returns plain string crawled.
func (context *Context) initText() {
	var err error

	// Using the surf kernel download, try to automatically transcoding
	if context.Request.DownloaderID == request.SurfID {
		var contentType, pageEncode string
		// Preferentially read the encoding type from the response header
		contentType = context.Response.Header.Get("Content-Type")
		if _, params, err := mime.ParseMediaType(contentType); err == nil {
			if cs, ok := params["charset"]; ok {
				pageEncode = strings.ToLower(strings.TrimSpace(cs))
			}
		}
		// The response header is not read from the request header when the encoding type is not specified
		if len(pageEncode) == 0 {
			contentType = context.Request.Header.Get("Content-Type")
			if _, params, err := mime.ParseMediaType(contentType); err == nil {
				if cs, ok := params["charset"]; ok {
					pageEncode = strings.ToLower(strings.TrimSpace(cs))
				}
			}
		}

		switch pageEncode {
		// Do not do transcoding
		case "utf8", "utf-8", "unicode-1-1-utf-8":
		default:
			// Specify the encoding type, but not utf8, the automatic transcoding to utf8
			// get converter to utf-8
			// Charset auto determine. Use golang.org/x/net/html/charset. Get response body and change it to utf-8
			var destReader io.Reader

			if len(pageEncode) == 0 {
				destReader, err = charset.NewReader(context.Response.Body, "")
			} else {
				destReader, err = charset.NewReaderLabel(pageEncode, context.Response.Body)
			}

			if err == nil {
				context.text, err = ioutil.ReadAll(destReader)
				if err == nil {
					context.Response.Body.Close()
					return
				}
				logs.Log.Warning("[convert][%v]: %v (ignore transcoding)", context.GetURL(), err)
			} else {
				logs.Log.Warning("[convert][%v]: %v (ignore transcoding)", context.GetURL(), err)
			}
		}
	}

	// Do not do transcoding
	context.text, err = ioutil.ReadAll(context.Response.Body)
	context.Response.Body.Close()
	if err != nil {
		// panic(err.Error())
		return
	}

}

/**
 * Encoding type reference
 * Not case sensitive
 * var nameMap = map[string]htmlEncoding{
	"unicode-1-1-utf-8":   utf8,
	"utf-8":               utf8,
	"utf8":                utf8,
	"866":                 ibm866,
	"cp866":               ibm866,
	"csibm866":            ibm866,
	"ibm866":              ibm866,
	"csisolatin2":         iso8859_2,
	"iso-8859-2":          iso8859_2,
	"iso-ir-101":          iso8859_2,
	"iso8859-2":           iso8859_2,
	"iso88592":            iso8859_2,
	"iso_8859-2":          iso8859_2,
	"iso_8859-2:1987":     iso8859_2,
	"l2":                  iso8859_2,
	"latin2":              iso8859_2,
	"csisolatin3":         iso8859_3,
	"iso-8859-3":          iso8859_3,
	"iso-ir-109":          iso8859_3,
	"iso8859-3":           iso8859_3,
	"iso88593":            iso8859_3,
	"iso_8859-3":          iso8859_3,
	"iso_8859-3:1988":     iso8859_3,
	"l3":                  iso8859_3,
	"latin3":              iso8859_3,
	"csisolatin4":         iso8859_4,
	"iso-8859-4":          iso8859_4,
	"iso-ir-110":          iso8859_4,
	"iso8859-4":           iso8859_4,
	"iso88594":            iso8859_4,
	"iso_8859-4":          iso8859_4,
	"iso_8859-4:1988":     iso8859_4,
	"l4":                  iso8859_4,
	"latin4":              iso8859_4,
	"csisolatincyrillic":  iso8859_5,
	"cyrillic":            iso8859_5,
	"iso-8859-5":          iso8859_5,
	"iso-ir-144":          iso8859_5,
	"iso8859-5":           iso8859_5,
	"iso88595":            iso8859_5,
	"iso_8859-5":          iso8859_5,
	"iso_8859-5:1988":     iso8859_5,
	"arabic":              iso8859_6,
	"asmo-708":            iso8859_6,
	"csiso88596e":         iso8859_6,
	"csiso88596i":         iso8859_6,
	"csisolatinarabic":    iso8859_6,
	"ecma-114":            iso8859_6,
	"iso-8859-6":          iso8859_6,
	"iso-8859-6-e":        iso8859_6,
	"iso-8859-6-i":        iso8859_6,
	"iso-ir-127":          iso8859_6,
	"iso8859-6":           iso8859_6,
	"iso88596":            iso8859_6,
	"iso_8859-6":          iso8859_6,
	"iso_8859-6:1987":     iso8859_6,
	"csisolatingreek":     iso8859_7,
	"ecma-118":            iso8859_7,
	"elot_928":            iso8859_7,
	"greek":               iso8859_7,
	"greek8":              iso8859_7,
	"iso-8859-7":          iso8859_7,
	"iso-ir-126":          iso8859_7,
	"iso8859-7":           iso8859_7,
	"iso88597":            iso8859_7,
	"iso_8859-7":          iso8859_7,
	"iso_8859-7:1987":     iso8859_7,
	"sun_eu_greek":        iso8859_7,
	"csiso88598e":         iso8859_8,
	"csisolatinhebrew":    iso8859_8,
	"hebrew":              iso8859_8,
	"iso-8859-8":          iso8859_8,
	"iso-8859-8-e":        iso8859_8,
	"iso-ir-138":          iso8859_8,
	"iso8859-8":           iso8859_8,
	"iso88598":            iso8859_8,
	"iso_8859-8":          iso8859_8,
	"iso_8859-8:1988":     iso8859_8,
	"visual":              iso8859_8,
	"csiso88598i":         iso8859_8I,
	"iso-8859-8-i":        iso8859_8I,
	"logical":             iso8859_8I,
	"csisolatin6":         iso8859_10,
	"iso-8859-10":         iso8859_10,
	"iso-ir-157":          iso8859_10,
	"iso8859-10":          iso8859_10,
	"iso885910":           iso8859_10,
	"l6":                  iso8859_10,
	"latin6":              iso8859_10,
	"iso-8859-13":         iso8859_13,
	"iso8859-13":          iso8859_13,
	"iso885913":           iso8859_13,
	"iso-8859-14":         iso8859_14,
	"iso8859-14":          iso8859_14,
	"iso885914":           iso8859_14,
	"csisolatin9":         iso8859_15,
	"iso-8859-15":         iso8859_15,
	"iso8859-15":          iso8859_15,
	"iso885915":           iso8859_15,
	"iso_8859-15":         iso8859_15,
	"l9":                  iso8859_15,
	"iso-8859-16":         iso8859_16,
	"cskoi8r":             koi8r,
	"koi":                 koi8r,
	"koi8":                koi8r,
	"koi8-r":              koi8r,
	"koi8_r":              koi8r,
	"koi8-ru":             koi8u,
	"koi8-u":              koi8u,
	"csmacintosh":         macintosh,
	"mac":                 macintosh,
	"macintosh":           macintosh,
	"x-mac-roman":         macintosh,
	"dos-874":             windows874,
	"iso-8859-11":         windows874,
	"iso8859-11":          windows874,
	"iso885911":           windows874,
	"tis-620":             windows874,
	"windows-874":         windows874,
	"cp1250":              windows1250,
	"windows-1250":        windows1250,
	"x-cp1250":            windows1250,
	"cp1251":              windows1251,
	"windows-1251":        windows1251,
	"x-cp1251":            windows1251,
	"ansi_x3.4-1968":      windows1252,
	"ascii":               windows1252,
	"cp1252":              windows1252,
	"cp819":               windows1252,
	"csisolatin1":         windows1252,
	"ibm819":              windows1252,
	"iso-8859-1":          windows1252,
	"iso-ir-100":          windows1252,
	"iso8859-1":           windows1252,
	"iso88591":            windows1252,
	"iso_8859-1":          windows1252,
	"iso_8859-1:1987":     windows1252,
	"l1":                  windows1252,
	"latin1":              windows1252,
	"us-ascii":            windows1252,
	"windows-1252":        windows1252,
	"x-cp1252":            windows1252,
	"cp1253":              windows1253,
	"windows-1253":        windows1253,
	"x-cp1253":            windows1253,
	"cp1254":              windows1254,
	"csisolatin5":         windows1254,
	"iso-8859-9":          windows1254,
	"iso-ir-148":          windows1254,
	"iso8859-9":           windows1254,
	"iso88599":            windows1254,
	"iso_8859-9":          windows1254,
	"iso_8859-9:1989":     windows1254,
	"l5":                  windows1254,
	"latin5":              windows1254,
	"windows-1254":        windows1254,
	"x-cp1254":            windows1254,
	"cp1255":              windows1255,
	"windows-1255":        windows1255,
	"x-cp1255":            windows1255,
	"cp1256":              windows1256,
	"windows-1256":        windows1256,
	"x-cp1256":            windows1256,
	"cp1257":              windows1257,
	"windows-1257":        windows1257,
	"x-cp1257":            windows1257,
	"cp1258":              windows1258,
	"windows-1258":        windows1258,
	"x-cp1258":            windows1258,
	"x-mac-cyrillic":      macintoshCyrillic,
	"x-mac-ukrainian":     macintoshCyrillic,
	"chinese":             gbk,
	"csgb2312":            gbk,
	"csiso58gb231280":     gbk,
	"gb2312":              gbk,
	"gb_2312":             gbk,
	"gb_2312-80":          gbk,
	"gbk":                 gbk,
	"iso-ir-58":           gbk,
	"x-gbk":               gbk,
	"gb18030":             gb18030,
	"big5":                big5,
	"big5-hkscs":          big5,
	"cn-big5":             big5,
	"csbig5":              big5,
	"x-x-big5":            big5,
	"cseucpkdfmtjapanese": eucjp,
	"euc-jp":              eucjp,
	"x-euc-jp":            eucjp,
	"csiso2022jp":         iso2022jp,
	"iso-2022-jp":         iso2022jp,
	"csshiftjis":          shiftJIS,
	"ms932":               shiftJIS,
	"ms_kanji":            shiftJIS,
	"shift-jis":           shiftJIS,
	"shift_jis":           shiftJIS,
	"sjis":                shiftJIS,
	"windows-31j":         shiftJIS,
	"x-sjis":              shiftJIS,
	"cseuckr":             euckr,
	"csksc56011987":       euckr,
	"euc-kr":              euckr,
	"iso-ir-149":          euckr,
	"korean":              euckr,
	"ks_c_5601-1987":      euckr,
	"ks_c_5601-1989":      euckr,
	"ksc5601":             euckr,
	"ksc_5601":            euckr,
	"windows-949":         euckr,
	"csiso2022kr":         replacement,
	"hz-gb-2312":          replacement,
	"iso-2022-cn":         replacement,
	"iso-2022-cn-ext":     replacement,
	"iso-2022-kr":         replacement,
	"utf-16be":            utf16be,
	"utf-16":              utf16le,
	"utf-16le":            utf16le,
	"x-user-defined":      xUserDefined,
}*/
