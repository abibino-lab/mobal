package common

import (
	"net/url"
	"strings"

	"gitlab.com/abibino-lab/mobal/common/goquery"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	. "gitlab.com/abibino-lab/mobal/app/spider"
)

// Form is the default form element.
type Form struct {
	ctx       *Context
	rule      string
	selection *goquery.Selection
	method    string
	action    string
	fields    url.Values
	buttons   url.Values
}

// NewForm creates and returns a *Form type.
func NewForm(ctx *Context, rule string, u string, form *goquery.Selection, schemeAndHost ...string) *Form {
	fields, buttons := serializeForm(form)
	if len(schemeAndHost) == 0 {
		aurl, _ := url.Parse(u)
		schemeAndHost = append(schemeAndHost, aurl.Scheme+"://"+aurl.Host)
	}
	method, action := formAttributes(u, form, schemeAndHost...)
	if action == "" {
		return nil
	}
	if method == "" {
		method = "GET"
	}
	return &Form{
		ctx:       ctx,
		rule:      rule,
		selection: form,
		method:    method,
		action:    action,
		fields:    fields,
		buttons:   buttons,
	}
}

// Method returns the form method, eg "GET" or "POST" or "POST-M".
func (form *Form) Method() string {
	return form.method
}

// Action returns the form action URL.
// The URL will always be absolute.
func (form *Form) Action() string {
	return form.action
}

// Input sets the value of a form field.
func (form *Form) Input(name, value string) *Form {
	if _, ok := form.fields[name]; ok {
		form.fields.Set(name, value)
	}
	return form
}

// Input sets the value of a form field.
func (form *Form) Inputs(kv map[string]string) *Form {
	for k, v := range kv {
		if _, ok := form.fields[k]; ok {
			form.fields.Set(k, v)
		}
	}
	return form
}

// Submit submits the form.
// Clicks the first button in the form, or submits the form without using
// any button when the form does not contain any buttons.
func (form *Form) Submit() bool {
	if len(form.buttons) > 0 {
		for name := range form.buttons {
			return form.Click(name)
		}
	}
	return form.send("", "")
}

// Click submits the form by clicking the button with the given name.
func (form *Form) Click(button string) bool {
	if _, ok := form.buttons[button]; !ok {
		return false
	}
	return form.send(button, form.buttons[button][0])
}

// Dom returns the inner *goquery.Selection.
func (form *Form) Dom() *goquery.Selection {
	return form.selection
}

// send submits the form.
func (form *Form) send(buttonName, buttonValue string) bool {

	values := make(url.Values, len(form.fields)+1)
	for name, vals := range form.fields {
		values[name] = vals
	}
	if buttonName != "" {
		values.Set(buttonName, buttonValue)
	}
	valsStr := values.Encode()
	if form.Method() == "GET" {
		form.ctx.AddQueue(&request.Request{
			Rule:   form.rule,
			URL:    form.Action() + "?" + valsStr,
			Method: form.Method(),
		})
		return true
	} else {
		enctype, _ := form.selection.Attr("enctype")
		if enctype == "multipart/form-data" {
			form.ctx.AddQueue(&request.Request{
				Rule:     form.rule,
				URL:      form.Action(),
				PostData: valsStr,
				Method:   "POST-M",
			})
			return true
		}
		form.ctx.AddQueue(&request.Request{
			Rule:     form.rule,
			URL:      form.Action(),
			PostData: valsStr,
			Method:   form.Method(),
		})
		return true
	}

	return false
}

// Serialize converts the form fields into a url.Values type.
// Returns two url.Value types. The first is the form field values, and the
// second is the form button values.
func serializeForm(sel *goquery.Selection) (url.Values, url.Values) {
	input := sel.Find("input,button,textarea")
	if input.Length() == 0 {
		return url.Values{}, url.Values{}
	}

	fields := make(url.Values)
	buttons := make(url.Values)
	input.Each(func(_ int, s *goquery.Selection) {
		name, ok := s.Attr("name")
		if ok {
			typ, ok := s.Attr("type")
			if ok || s.Is("textarea") {
				if typ == "submit" {
					val, ok := s.Attr("value")
					if ok {
						buttons.Add(name, val)
					} else {
						buttons.Add(name, "")
					}
				} else {
					val, ok := s.Attr("value")
					if !ok {
						val = ""
					}
					fields.Add(name, val)
				}
			}
		}
	})

	return fields, buttons
}

func formAttributes(u string, form *goquery.Selection, schemeAndHost ...string) (string, string) {
	method, ok := form.Attr("method")
	if !ok {
		method = "GET"
	}
	action, ok := form.Attr("action")
	if !ok {
		action = u
	}
	if action, ok = MakeURL(action, schemeAndHost...); !ok {
		return "", ""
	}

	return strings.ToUpper(method), action
}
