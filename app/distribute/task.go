package distribute

// Task : for distributed distribution tasks
type Task struct {
	ID             int
	Spiders        []map[string]string // spider rule name field with keyin field, specify format map [string] string {"name": "baidu", "keyIn": "henry"}
	ThreadNum      int                 // global maximum concurrency
	Pausetime      int64               // pause duration reference / ms (random: Pausetime / 2 ~ Pausetime * 2)
	OutType        string              // output method
	DockerCap      int                 // Subdivide container capacity
	DockerQueueCap int                 // Segment output pool capacity, not less than 2
	SuccessInherit bool                // inherit history success record
	FailureInherit bool                // inherit history failure record
	Limit          int64               // set the upper limit, 0 is not limited, if the rule is set to the initial value of Limit is a custom limit, otherwise the default limit request number
	ProxyMinute    int64               // The number of minutes of proxy IP replacement
	// Optional
	KeyIns string // Custom input, late cut into multiple tasks KeyIn custom configuration
}
