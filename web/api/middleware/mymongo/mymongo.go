package mymongo

import (
	"time"

	"gitlab.com/abibino-lab/mobal/config"

	"gopkg.in/mgo.v2"
)

// DataStore containing a pointer to a mgo session
type DataStore struct {
	Session *mgo.Session
}

// ConnectToTagserver is a helper method that connections to pubgears' tagserver
// database
func (ds *DataStore) ConnectToTagserver() (err error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{config.MGO_HOST},
		Timeout:  60 * time.Second,
		Database: "mobal",
	}
	sess, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		return
	}
	sess.SetMode(mgo.Monotonic, true)
	ds.Session = sess
	return
}

// Close is a helper method that ensures the session is properly terminated
func (ds *DataStore) Close() {
	ds.Session.Close()
}
