package main

import (
	_ "gitlab.com/abibino-lab/mobal-lib" // Also you can also add your own rule base
	"gitlab.com/abibino-lab/mobal/exec"
	//_ "gitlab.com/abibino-lab/mobal-lib" // This is a publicly maintained spider rule base
)

func main() {
	// Set the runtime default interface and start running
	// Before running the software, you can set the -a_ui parameters for the "web", "gui" or "cmd", specify the operation of the operation interface
	// where "gui" only supports Windows systemsx``
	exec.DefaultRun("web")
}
