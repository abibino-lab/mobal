package controllers

import (
	"net/http"

	"gitlab.com/abibino-lab/mobal/web/api/models"

	"github.com/labstack/echo"
)

// Logout is a function for ....
func Logout(c echo.Context) (err error) {
	return nil
}

//Register is a function for ....
func Register(c echo.Context) (err error) {

	u := new(models.UserRegister)
	if err = c.Bind(&u); err != nil {
		return
	}

	if err = c.Validate(u); err != nil {
		return
	}

	if _, err = u.Save(); err != nil {
		return
	}

	return c.JSON(http.StatusCreated, map[string]string{"message": "Successfully"})
}
