package spider

import (
	"math"
	"sync"
	"time"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/app/scheduler"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

const (
	KeyIn      = util.UseKeyIn // If Spider.KeyIn is used, set the initial value to USE_KeyIn in the rule
	Limit      = math.MaxInt64 // If you want to customize the control limit in the rule, the Limit initial value must be Limit
	ForcedStop = "- Take the initiative to terminate Spider -"
)

type (
	// spider rules
	Spider struct {
		// The following fields are defined by the user
		Name            string                                                       // User interface displays the name (should be guaranteed uniqueness)
		Description     string                                                       // The user interface displays the description
		Pausetime       int64                                                        // random pause interval (50% ~ 200%), if the rules are directly defined, it is not covered by interface
		Limit           int64                                                        // default limit request number, 0 is not limited; if the rule is defined as Limit, then use the rules of the custom limit program
		KeyIn           string                                                       // Customize the input configuration information, set the initial value to KeyIn in the rule before use
		EnableCookie    bool                                                         // all requests are using cookie records
		NotDefaultField bool                                                         // whether to disable the output of the default field currentLink / parentLink / downloadTime
		Namespace       func(spider *Spider) string                                  // namespace, used to output files, named paths
		SubNamespace    func(spider *Spider, dataCell map[string]interface{}) string // Secondary naming, used to output files, named paths, can depend on specific data content
		RuleTree        *RuleTree                                                    // define a specific collection rule tree

		// The following fields are automatically assigned to the system
		id        int               // automatically assigns an index in SpiderQueue
		subName   string            // secondary name converted by KeyIn
		reqMatrix *scheduler.Matrix // request matrix
		timer     *Timer            // timers
		status    int               // execution status
		lock      sync.RWMutex
		once      sync.Once
	}
	// Collect the rule tree
	RuleTree struct {
		Root  func(*Context)   // root node (execute entry)
		Trunk map[string]*Rule // node hash table (execution acquisition process)
	}
	// Collect the rule node
	Rule struct {
		ItemFields []string                                           // result field list (optional, write guaranteed field order)
		ParseFunc  func(*Context)                                     // Content parsing function
		AidFunc    func(*Context, map[string]interface{}) interface{} // General helper function
	}
)

// add yourself to the spider menu
func (spider Spider) Register() *Spider {
	spider.status = status.STOPPED
	return Species.Add(&spider)
}

// Specify the list of field names for the result of the rule
func (spider *Spider) GetItemFields(rule *Rule) []string {
	return rule.ItemFields
}

// returns the value of the result field name
// does not exist when the empty string is returned
func (spider *Spider) GetItemField(rule *Rule, index int) (field string) {
	if index > len(rule.ItemFields)-1 || index < 0 {
		return ""
	}
	return rule.ItemFields[index]
}

// returns the index of the result field name
// does not exist when the index is -1
func (spider *Spider) GetItemFieldIndex(rule *Rule, field string) (index int) {
	for idx, v := range rule.ItemFields {
		if v == field {
			return idx
		}
	}
	return -1
}

// dynamically appends the result field name to the specified rule and returns the index position
// already exists to return to the original index position
func (spider *Spider) UpsertItemField(rule *Rule, field string) (index int) {
	for i, v := range rule.ItemFields {
		if v == field {
			return i
		}
	}
	rule.ItemFields = append(rule.ItemFields, field)
	return len(rule.ItemFields) - 1
}

// Get the spider name
func (spider *Spider) GetName() string {
	return spider.Name
}

// Get the spider secondary name
func (spider *Spider) GetSubName() string {
	spider.once.Do(func() {
		spider.subName = spider.GetKeyIn()
		if len([]rune(spider.subName)) > 8 {
			spider.subName = util.MakeHash(spider.subName)
		}
	})
	return spider.subName
}

// security returns the specified rule
func (spider *Spider) GetRule(ruleName string) (*Rule, bool) {
	rule, found := spider.RuleTree.Trunk[ruleName]
	return rule, found
}

// returns the specified rule
func (spider *Spider) MustGetRule(ruleName string) *Rule {
	return spider.RuleTree.Trunk[ruleName]
}

// return to the rule tree
func (spider *Spider) GetRules() map[string]*Rule {
	return spider.RuleTree.Trunk
}

// Get the spider description
func (spider *Spider) GetDescription() string {
	return spider.Description
}

// Get spider ID
func (spider *Spider) GetID() int {
	return spider.id
}

// set spider ID
func (spider *Spider) SetID(id int) {
	spider.id = id
}

// Get custom configuration information
func (spider *Spider) GetKeyIn() string {
	return spider.KeyIn
}

// Set up custom configuration information
func (spider *Spider) SetKeyIn(keyword string) {
	spider.KeyIn = keyword
}

// Get the acquisition limit
// <0 means that the number of requests is limited
//> 0 indicates that the custom limit scheme is used in the rule
func (spider *Spider) GetLimit() int64 {
	return spider.Limit
}

// set the acquisition limit
// <0 means that the number of requests is limited
//> 0 indicates that the custom limit scheme is used in the rule
func (spider *Spider) SetLimit(max int64) {
	spider.Limit = max
}

// control whether all requests use cookies
func (spider *Spider) GetEnableCookie() bool {
	return spider.EnableCookie
}

// Custom pause time pause [0] ~ (pause [0] + pause [1]), higher priority than external pass
// overwrite existing values ​​if and only if runtime [0] is true
func (spider *Spider) SetPausetime(pause int64, runtime ...bool) {
	if spider.Pausetime == 0 || len(runtime) > 0 && runtime[0] {
		spider.Pausetime = pause
	}
}

// set the timer
// @id is uniquely identified by the timer
// @ Bell == nil when the countdown, then @ tol for sleep long
// @bell! = Nil for the alarm, this time @ tol used to specify the time to wake up (from now encountered from the first bell to the bell)
func (spider *Spider) SetTimer(id string, tol time.Duration, bell *Bell) bool {
	if spider.timer == nil {
		spider.timer = newTimer()
	}
	return spider.timer.set(id, tol, bell)
}

// start the timer and return to the timer if it can continue to use
func (spider *Spider) RunTimer(id string) bool {
	if spider.timer == nil {
		return false
	}
	return spider.timer.sleep(id)
}

// return a copy of your own
func (spider *Spider) Copy() *Spider {
	ghost := &Spider{}
	ghost.Name = spider.Name
	ghost.subName = spider.subName

	ghost.RuleTree = &RuleTree{
		Root:  spider.RuleTree.Root,
		Trunk: make(map[string]*Rule, len(spider.RuleTree.Trunk)),
	}
	for k, v := range spider.RuleTree.Trunk {
		ghost.RuleTree.Trunk[k] = new(Rule)

		ghost.RuleTree.Trunk[k].ItemFields = make([]string, len(v.ItemFields))
		copy(ghost.RuleTree.Trunk[k].ItemFields, v.ItemFields)

		ghost.RuleTree.Trunk[k].ParseFunc = v.ParseFunc
		ghost.RuleTree.Trunk[k].AidFunc = v.AidFunc
	}

	ghost.Description = spider.Description
	ghost.Pausetime = spider.Pausetime
	ghost.EnableCookie = spider.EnableCookie
	ghost.Limit = spider.Limit
	ghost.KeyIn = spider.KeyIn

	ghost.NotDefaultField = spider.NotDefaultField
	ghost.Namespace = spider.Namespace
	ghost.SubNamespace = spider.SubNamespace

	ghost.timer = spider.timer
	ghost.status = spider.status

	return ghost
}

func (spider *Spider) ReqmatrixInit() *Spider {
	if spider.Limit < 0 {
		spider.reqMatrix = scheduler.AddMatrix(spider.GetName(), spider.GetSubName(), spider.Limit)
		spider.SetLimit(0)
	} else {
		spider.reqMatrix = scheduler.AddMatrix(spider.GetName(), spider.GetSubName(), math.MinInt64)
	}
	return spider
}

// Returns whether the request was added as a new failure to the end of the queue
func (spider *Spider) DoHistory(req *request.Request, ok bool) bool {
	return spider.reqMatrix.DoHistory(req, ok)
}

func (spider *Spider) RequestPush(req *request.Request) {
	spider.reqMatrix.Push(req)
}

func (spider *Spider) RequestPull() *request.Request {
	return spider.reqMatrix.Pull()
}

func (spider *Spider) RequestUse() {
	spider.reqMatrix.Use()
}

func (spider *Spider) RequestFree() {
	spider.reqMatrix.Free()
}

func (spider *Spider) RequestLen() int {
	return spider.reqMatrix.Len()
}

func (spider *Spider) TryFlushSuccess() {
	spider.reqMatrix.TryFlushSuccess()
}

func (spider *Spider) TryFlushFailure() {
	spider.reqMatrix.TryFlushFailure()
}

// start the spider
func (spider *Spider) Start() {
	defer func() {
		if p := recover(); p != nil {
			logs.Log.Error("Panic  [root]: %v", p)
		}
		spider.lock.Lock()
		spider.status = status.RUN
		spider.lock.Unlock()
	}()
	spider.RuleTree.Root(GetContext(spider, nil))
}

// Active crash crawler run the connection
func (spider *Spider) Stop() {
	spider.lock.Lock()
	defer spider.lock.Unlock()
	if spider.status == status.STOP {
		return
	}
	spider.status = status.STOP
	// Cancel all timers
	if spider.timer != nil {
		spider.timer.drop()
		spider.timer = nil
	}
}

func (spider *Spider) CanStop() bool {
	spider.lock.RLock()
	defer spider.lock.RUnlock()
	return spider.status != status.STOPPED && spider.reqMatrix.CanStop()
}

func (spider *Spider) IsStopping() bool {
	spider.lock.RLock()
	defer spider.lock.RUnlock()
	return spider.status == status.STOP
}

// If the task has been terminated, the crash crawler association
func (spider *Spider) tryPanic() {
	if spider.IsStopping() {
		panic(ForcedStop)
	}
}

// Exit the job before finishing the job
func (spider *Spider) Defer() {
	// Cancel all timers
	if spider.timer != nil {
		spider.timer.drop()
		spider.timer = nil
	}
	// Wait for the request to be processed
	spider.reqMatrix.Wait()
	// update the failed record
	spider.reqMatrix.TryFlushFailure()
}

// whether to output the default added field URL / ParentURL / downloadTime
func (spider *Spider) OutDefaultField() bool {
	return !spider.NotDefaultField
}
