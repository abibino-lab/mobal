package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type (
	// Target is a struct for..
	Target struct {
		ID        bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
		Domain    string        `bson:"domain,omitempty" json:"domain,omitempty"`
		Name      string        `bson:"name,omitempty" json:"name,omitempty"`
		Logo      string        `bson:"logo,omitempty" json:"logo,omitempty"`
		Type      string        `bson:"type" json:"type"` // ecommerce | marketplace | news
		Status    int           `bson:"status" json:"status"`
		CreatedAt time.Time     `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
		UpdatedAt time.Time     `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
		UpdatedBy bson.ObjectId `bson:"updatedBy,omitempty" json:"updatedBy,omitempty"`
		CreatedBy bson.ObjectId `bson:"createdBy,omitempty" json:"createdBy,omitempty"`
	}
)
