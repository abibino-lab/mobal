package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// in the specified set of conditions for inquiries
type Find struct {
	Database   string                 // database
	Collection string                 // collection
	Query      map[string]interface{} // Check for phrases
	Sort       []string               // sort, usage such as Sort ("firstname", "-lastname"), preceded by firstname, followed by lastname
	Skip       int                    // skip the previous n documents
	Limit      int                    // return up to n documents
	Select     interface{}            // query only, return to the specified field, such as {"name": 1}
	// Result struct {
	// docs [] interface {}
	// total int
	//}
}

func (find *Find) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	resultPtr2 := resultPtr.(*map[string]interface{})
	*resultPtr2 = map[string]interface{}{}

	err = Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(find.Database).C(find.Collection)

		if id, ok := find.Query["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be a string type!")
			} else {
				find.Query["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		q := c.Find(find.Query)

		(*resultPtr2)["Total"], _ = q.Count()

		if len(find.Sort) > 0 {
			q.Sort(find.Sort...)
		}

		if find.Skip > 0 {
			q.Skip(find.Skip)
		}

		if find.Limit > 0 {
			q.Limit(find.Limit)
		}

		if find.Select != nil {
			q.Select(find.Select)
		}
		r := []interface{}{}
		err = q.All(&r)

		(*resultPtr2)["Docs"] = r

		return err
	})
	return
}
