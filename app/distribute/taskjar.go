package distribute

// TaskJar : task repository
type TaskJar struct {
	Tasks chan *Task
}

// NewTaskJar :
func NewTaskJar() *TaskJar {
	return &TaskJar{
		Tasks: make(chan *Task, 1024),
	}
}

// Push : The server adds a task to the repository
func (taskJar *TaskJar) Push(task *Task) {
	id := len(taskJar.Tasks)
	task.ID = id
	taskJar.Tasks <- task
}

// Pull : The client gets a task from the local repository
func (taskJar *TaskJar) Pull() *Task {
	return <-taskJar.Tasks
}

// Len : Total number of warehouse tasks
func (taskJar *TaskJar) Len() int {
	return len(taskJar.Tasks)
}

// Send : The master node sends a task from the repository
func (taskJar *TaskJar) Send(clientNum int) Task {
	return *<-taskJar.Tasks
}

// Receive : receive a task from the node to the repository
func (taskJar *TaskJar) Receive(task *Task) {
	taskJar.Tasks <- task
}
