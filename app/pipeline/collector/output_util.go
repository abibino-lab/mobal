package collector

import (
	"gitlab.com/abibino-lab/mobal/logs"
)

// The main namespace relative to the database name, does not depend on the specific data content, optional
func (collector *Collector) namespace() string {
	if collector.Spider.Namespace == nil {
		if collector.Spider.GetSubName() == "" {
			return collector.Spider.GetName()
		}
		return collector.Spider.GetName() + "__" + collector.Spider.GetSubName()
	}
	return collector.Spider.Namespace(collector.Spider)
}

// Secondary namespace relative to table names, depending on the specific data content, optional
func (collector *Collector) subNamespace(dataCell map[string]interface{}) string {
	if collector.Spider.SubNamespace == nil {
		return dataCell["ruleName"].(string)
	}
	defer func() {
		if p := recover(); p != nil {
			logs.Log.Error("subNamespace: %v", p)
		}
	}()
	return collector.Spider.SubNamespace(collector.Spider, dataCell)
}

// Underline to connect primary and secondary namespace
func joinNamespaces(namespace, subNamespace string) string {
	if namespace == "" {
		return subNamespace
	} else if subNamespace != "" {
		return namespace + "__" + subNamespace
	}
	return namespace
}
