package models

import (
	"gitlab.com/abibino-lab/mobal/web/api/middleware/mymongo"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// New is a function for ...
func (i *Item) New() {
	i.ID = bson.NewObjectId()
	i.CreatedAt = i.ID.Time()
	i.UpdatedAt = i.ID.Time()
}

// GetShortDescription is a function for ...
func (i *Item) GetShortDescription() string {
	return i.Description
}

// TargetName is a function for ...
func (i *Item) TargetName() string {
	return i.Link
}

// SetAttributes is a function for ...
func (i *Item) SetAttributes(attr interface{}) {
	i.Attributes = attr
}

// Insert is a function for insert document to collection.
func (i *Item) Insert() (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()
	// Collection Item
	c := ds.Session.DB("mobal").C("items")

	// Index
	index := mgo.Index{
		Key:        []string{"link"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	// DB indexing
	err = c.EnsureIndex(index)

	if err != nil {
		code = ErrDatabase
	} else {

		// Insert document to collection
		err = c.Insert(i)

		if err != nil {
			if mgo.IsDup(err) {
				code = ErrDupRows
			}
			code = ErrDatabase
		}
	}

	return

}
