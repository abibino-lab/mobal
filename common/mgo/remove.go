package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// delete data
type Remove struct {
	Database   string                 // database
	Collection string                 // collection
	Selector   map[string]interface{} // document selector
}

func (remove *Remove) Exec(_ interface{}) error {
	return Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(remove.Database).C(remove.Collection)

		if id, ok := remove.Selector["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be a string type!")
			} else {
				remove.Selector["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		return c.Remove(remove.Selector)
	})
}
