package collector

import (
	"fmt"

	mgov2 "gopkg.in/mgo.v2"

	"gitlab.com/abibino-lab/mobal/common/mgo"
	"gitlab.com/abibino-lab/mobal/common/pool"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

/************************ MongoDB Output ***************************/

func init() {
	DataOutput["mgo"] = func(collector *Collector) error {
		// Connect to the database
		if mgo.Error() != nil {
			return fmt.Errorf("MongoDB database connection failed: %v", mgo.Error())
		}
		return mgo.Call(func(src pool.Src) error {
			var (
				db          = src.(*mgo.MgoSrc).DB(config.DB_NAME)
				namespace   = util.FileNameReplace(collector.namespace())
				collections = make(map[string]*mgov2.Collection)
				dataMap     = make(map[string][]interface{})
				err         error
			)

			for _, datacell := range collector.dataDocker {
				subNamespace := util.FileNameReplace(collector.subNamespace(datacell))
				cName := joinNamespaces(namespace, subNamespace)

				if _, ok := collections[subNamespace]; !ok {
					collections[subNamespace] = db.C(cName)
				}
				for k, v := range datacell["data"].(map[string]interface{}) {
					datacell[k] = v
				}
				delete(datacell, "data")
				delete(datacell, "ruleName")
				if !collector.Spider.OutDefaultField() {
					delete(datacell, "currentLink")
					delete(datacell, "parentLink")
					delete(datacell, "downloadTime")
				}
				dataMap[subNamespace] = append(dataMap[subNamespace], datacell)
			}

			for collection, docs := range dataMap {
				c := collections[collection]
				count := len(docs)
				loop := count / mgo.MaxLen

				for i := 0; i < loop; i++ {
					err = c.Insert(docs[i*mgo.MaxLen : (i+1)*mgo.MaxLen]...)
					if err != nil {
						logs.Log.Error("%v", err)
					}
				}

				if count%mgo.MaxLen == 0 {
					continue
				}

				err = c.Insert(docs[loop*mgo.MaxLen:]...)
				if err != nil {
					logs.Log.Error("%v", err)
				}
			}

			return nil
		})
	}
}
