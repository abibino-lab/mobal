package scheduler

import (
	"sort"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/abibino-lab/mobal/app/aid/history"
	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

// a Spider instance's request matrix
type Matrix struct {
	maxPage         int64                       // Maximum number of pages collected, expressed as a negative number
	resCount        int32                       // Resource usage count
	spiderName      string                      // Belongs to Spider
	reqs            map[int][]*request.Request  // [priority] queue with a priority of 0
	priorities      []int                       // Priority order, from low to high
	history         history.Historier           // History record
	tempHistory     map[string]bool             // Temporary record [reqUnique (url + method)] true
	failures        map[string]*request.Request // History and this failed request
	tempHistoryLock sync.RWMutex
	failureLock     sync.Mutex
	sync.Mutex
}

func newMatrix(spiderName, spiderSubName string, maxPage int64) *Matrix {
	matrix := &Matrix{
		spiderName:  spiderName,
		maxPage:     maxPage,
		reqs:        make(map[int][]*request.Request),
		priorities:  []int{},
		history:     history.New(spiderName, spiderSubName),
		tempHistory: make(map[string]bool),
		failures:    make(map[string]*request.Request),
	}
	if cache.Task.Mode != status.SERVER {
		matrix.history.ReadSuccess(cache.Task.OutType, cache.Task.SuccessInherit)
		matrix.history.ReadFailure(cache.Task.OutType, cache.Task.FailureInherit)
		matrix.setFailures(matrix.history.PullFailure())
	}
	return matrix
}

// Add the request to the queue, concurrent security
func (matrix *Matrix) Push(req *request.Request) {
	// Prohibit concurrent, reduce the amount of requested inventory
	matrix.Lock()
	defer matrix.Unlock()

	if sdl.checkStatus(status.STOP) {
		return
	}

	// Reach the request limit and stop the rule from running
	if matrix.maxPage >= 0 {
		return
	}

	// Wait when the state is suspended, reduce the amount of requested inventory
	waited := false
	for sdl.checkStatus(status.PAUSE) {
		waited = true
		time.Sleep(time.Second)
	}
	if waited && sdl.checkStatus(status.STOP) {
		return
	}

	// When resources are used too much, wait and decrease the amount of requests
	waited = false
	for atomic.LoadInt32(&matrix.resCount) > sdl.avgRes() {
		waited = true
		time.Sleep(100 * time.Millisecond)
	}
	if waited && sdl.checkStatus(status.STOP) {
		return
	}

	// Can not be re-downloaded req
	if !req.IsReloadable() {
		// When a successful record has occurred
		if matrix.hasHistory(req.Unique()) {
			return
		}
		// Add to temporary record
		matrix.insertTempHistory(req.Unique())
	}

	var priority = req.GetPriority()

	// Initialize the spy under the priority queue
	if _, found := matrix.reqs[priority]; !found {
		matrix.priorities = append(matrix.priorities, priority)
		sort.Ints(matrix.priorities) // Sort from small to large
		matrix.reqs[priority] = []*request.Request{}
	}

	// Add the request to the queue
	matrix.reqs[priority] = append(matrix.reqs[priority], req)

	// Roughly limit the number of requests to join the queue, concurrent circumstances should be more than maxPage
	atomic.AddInt64(&matrix.maxPage, 1)
}

// From the queue to take out the request, there is no return to nil, concurrent security
func (matrix *Matrix) Pull() (req *request.Request) {
	matrix.Lock()
	defer matrix.Unlock()
	if !sdl.checkStatus(status.RUN) {
		return
	}
	// Fetch the request from the highest priority to the priority
	for i := len(matrix.reqs) - 1; i >= 0; i-- {
		idx := matrix.priorities[i]
		if len(matrix.reqs[idx]) > 0 {
			req = matrix.reqs[idx][0]
			matrix.reqs[idx] = matrix.reqs[idx][1:]
			if sdl.useProxy {
				req.SetProxy(sdl.proxy.GetOne(req.GetURL()))
			} else {
				req.SetProxy("")
			}
			return
		}
	}
	return
}

func (matrix *Matrix) Use() {
	defer func() {
		recover()
	}()
	sdl.count <- true
	atomic.AddInt32(&matrix.resCount, 1)
}

func (matrix *Matrix) Free() {
	<-sdl.count
	atomic.AddInt32(&matrix.resCount, -1)
}

// Returns whether the request was added as a new failure to the end of the queue
func (matrix *Matrix) DoHistory(req *request.Request, ok bool) bool {
	if !req.IsReloadable() {
		matrix.tempHistoryLock.Lock()
		delete(matrix.tempHistory, req.Unique())
		matrix.tempHistoryLock.Unlock()

		if ok {
			matrix.history.UpsertSuccess(req.Unique())
			return false
		}
	}

	if ok {
		return false
	}

	matrix.failureLock.Lock()
	defer matrix.failureLock.Unlock()
	if _, ok := matrix.failures[req.Unique()]; !ok {
		// When the first failure occurs, re-execute at the end of the task queue
		matrix.failures[req.Unique()] = req
		logs.Log.Informational("+ Failure request: [%v]", req.GetURL())
		return true
	}
	// After failing twice, add a history failure record
	matrix.history.UpsertFailure(req)
	return false
}

func (matrix *Matrix) CanStop() bool {
	if sdl.checkStatus(status.STOP) {
		return true
	}
	if matrix.maxPage >= 0 {
		return true
	}
	if atomic.LoadInt32(&matrix.resCount) != 0 {
		return false
	}
	if matrix.Len() > 0 {
		return false
	}

	matrix.failureLock.Lock()
	defer matrix.failureLock.Unlock()
	if len(matrix.failures) > 0 {
		// Download the failed request in the history
		var goon bool
		for reqUnique, req := range matrix.failures {
			if req == nil {
				continue
			}
			matrix.failures[reqUnique] = nil
			goon = true
			logs.Log.Informational("- Failure request: [%v]", req.GetURL())
			matrix.Push(req)
		}
		if goon {
			return false
		}
	}
	return true
}

// Non-server mode to save the history of success records
func (matrix *Matrix) TryFlushSuccess() {
	if cache.Task.Mode != status.SERVER && cache.Task.SuccessInherit {
		matrix.history.FlushSuccess(cache.Task.OutType)
	}
}

// Non-server mode to save the history of failure records
func (matrix *Matrix) TryFlushFailure() {
	if cache.Task.Mode != status.SERVER && cache.Task.FailureInherit {
		matrix.history.FlushFailure(cache.Task.OutType)
	}
}

// Wait for the request to be processed
func (matrix *Matrix) Wait() {
	if sdl.checkStatus(status.STOP) {
		// println ("Wait $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $$$$$$ ")
		// Take the initiative to terminate the task, do not wait for the operation of the natural end of the task
		return
	}
	for atomic.LoadInt32(&matrix.resCount) != 0 {
		time.Sleep(500 * time.Millisecond)
	}
}

func (matrix *Matrix) Len() int {
	matrix.Lock()
	defer matrix.Unlock()
	var l int
	for _, reqs := range matrix.reqs {
		l += len(reqs)
	}
	return l
}

func (matrix *Matrix) hasHistory(reqUnique string) bool {
	if matrix.history.HasSuccess(reqUnique) {
		return true
	}
	matrix.tempHistoryLock.RLock()
	has := matrix.tempHistory[reqUnique]
	matrix.tempHistoryLock.RUnlock()
	return has
}

func (matrix *Matrix) insertTempHistory(reqUnique string) {
	matrix.tempHistoryLock.Lock()
	matrix.tempHistory[reqUnique] = true
	matrix.tempHistoryLock.Unlock()
}

func (matrix *Matrix) setFailures(reqs map[string]*request.Request) {
	matrix.failureLock.Lock()
	defer matrix.failureLock.Unlock()
	for key, req := range reqs {
		matrix.failures[key] = req
		logs.Log.Informational("+ Failure request: [%v]", req.GetURL())
	}
}

/// / take the initiative to terminate the task, the finishing work
// func (matrix * Matrix) windup () {
// matrix.Lock ()

// matrix.reqs = make (map [int] [] * request.Request)
// matrix.priorities = [] int {}
// matrix.tempHistory = make (map [string] bool)

// matrix.failures = make (map [string] * request.Request)

// matrix.Unlock ()
//}
