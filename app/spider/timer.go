package spider

import (
	"sync"
	"time"

	"gitlab.com/abibino-lab/mobal/logs"
)

type Timer struct {
	setting map[string]*Clock
	closed  bool
	sync.RWMutex
}

func newTimer() *Timer {
	return &Timer{
		setting: make(map[string]*Clock),
	}
}

// sleep wait and return whether the timer can continue to use
func (timer *Timer) sleep(id string) bool {
	timer.RLock()
	if timer.closed {
		timer.RUnlock()
		return false
	}

	c, ok := timer.setting[id]
	timer.RUnlock()
	if !ok {
		return false
	}

	c.sleep()

	timer.RLock()
	defer timer.RUnlock()
	if timer.closed {
		return false
	}
	_, ok = timer.setting[id]

	return ok
}

// @ Bell == nil when the countdown, then @ tol for sleep long
// @bell! = Nil for the alarm, this time @ tol used to specify the time to wake up (from now encountered from the first bell to the bell)
func (timer *Timer) set(id string, tol time.Duration, bell *Bell) bool {
	timer.Lock()
	defer timer.Unlock()
	if timer.closed {
		logs.Log.Critical("Set timer <%s> failed, timed system is off", id)
		return false
	}
	c, ok := newClock(id, tol, bell)
	if !ok {
		logs.Log.Critical("Set timer <%s> failed, parameter is incorrect", id)
		return ok
	}
	timer.setting[id] = c
	logs.Log.Critical("Set timer <%s> success", id)
	return ok
}

func (timer *Timer) drop() {
	timer.Lock()
	defer timer.Unlock()
	timer.closed = true
	for _, c := range timer.setting {
		c.wake()
	}
	timer.setting = make(map[string]*Clock)
}

type (
	Clock struct {
		id string
		// mode (alarm or countdown)
		typ int
		// Countdown sleep time long
		// or specify the alarm to wake up from time to time from now to the first to the bell
		tol time.Duration
		// wake up the clock
		bell  *Bell
		timer *time.Timer
	}
	Bell struct {
		Hour int
		Min  int
		Sec  int
	}
)

const (
	// alarm clock
	A = iota
	// countdown
	T
)

// @ Bell == nil when the countdown, then @ tol for sleep long
// @bell! = nil for the alarm, this time @ tol used to specify the time to wake up (from now encountered from the first bell to the bell)
func newClock(id string, tol time.Duration, bell *Bell) (*Clock, bool) {
	if tol <= 0 {
		return nil, false
	}
	if bell == nil {
		return &Clock{
			id:    id,
			typ:   T,
			tol:   tol,
			timer: newT(),
		}, true
	}
	if !(bell.Hour >= 0 && bell.Hour < 24 && bell.Min >= 0 && bell.Min < 60 && bell.Sec >= 0 && bell.Sec < 60) {
		return nil, false
	}
	return &Clock{
		id:    id,
		typ:   A,
		tol:   tol,
		bell:  bell,
		timer: newT(),
	}, true
}

func (clock *Clock) sleep() {
	d := clock.duration()
	clock.timer.Reset(d)
	t0 := time.Now()
	logs.Log.Critical("Timer <%s> sleep %v, plan %v wake up", clock.id, d, t0.Add(d).Format("2006-01-02 15:04:05"))
	<-clock.timer.C
	t1 := time.Now()
	logs.Log.Critical("Timer <%s> wake up at %v, actual sleep %v", clock.id, t1.Format("2006-01-02 15:04:05"), t1.Sub(t0))
}

func (clock *Clock) wake() {
	clock.timer.Reset(0)
}

func (clock *Clock) duration() time.Duration {
	switch clock.typ {
	case A:
		t := time.Now()
		year, month, day := t.Date()
		bell := time.Date(year, month, day, clock.bell.Hour, clock.bell.Min, clock.bell.Sec, 0, time.Local)
		if bell.Before(t) {
			bell = bell.Add(time.Hour * 24 * clock.tol)
		} else {
			bell = bell.Add(time.Hour * 24 * (clock.tol - 1))
		}
		return bell.Sub(t)
	case T:
		return clock.tol
	}
	return 0
}

func newT() *time.Timer {
	t := time.NewTimer(0)
	<-t.C
	return t
}
