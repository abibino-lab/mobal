package exec

import (
	"os"
	"os/exec"
	"os/signal"

	"gitlab.com/abibino-lab/mobal/config"

	"gitlab.com/abibino-lab/mobal/cmd" // cmd version
	"gitlab.com/abibino-lab/mobal/web" // web version
)

func run(which string) {
	exec.Command("/bin/sh", "-c", "title", config.FULL_NAME).Start()

	// Choose to run the interface
	switch which {
	case "cmd":
		cmd.Run()

	case "web":
		fallthrough
	default:
		ctrl := make(chan os.Signal, 1)
		signal.Notify(ctrl, os.Interrupt, os.Kill)
		go web.Run()
		<-ctrl
	}
}
