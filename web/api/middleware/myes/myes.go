package myes

import (
	"fmt"

	"gitlab.com/abibino-lab/mobal/config"
	elastic "gopkg.in/olivere/elastic.v5"
)

// DataIndex containing a pointer to a mgo session
type DataIndex struct {
	ESClient *elastic.Client
}

// ConnectToES is a func for ...
func (i *DataIndex) ConnectToES() (err error) {
	i.ESClient, err = elastic.NewClient(elastic.SetSniff(false), elastic.SetURL(fmt.Sprintf("http://%v:%v", config.ES_HOST, config.ES_PORT)))
	return
}

// Close is a helper method that ensures the session is properly terminated
func (i *DataIndex) Close(index string) {
	i.ESClient.CloseIndex(index)
}
