package scheduler

import (
	"sync"
	"time"

	"gitlab.com/abibino-lab/mobal/app/aid/proxy"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

// scheduler
type scheduler struct {
	status       int          // Operating status
	count        chan bool    // Total concurrent quantity count
	useProxy     bool         // Mark whether to use proxy IP
	proxy        *proxy.Proxy // Global proxy IP
	matrices     []*Matrix    // Spider instance request matrix list
	sync.RWMutex              // Global read and write locks
}

// define global scheduling
var sdl = &scheduler{
	status: status.RUN,
	count:  make(chan bool, cache.Task.ThreadNum),
	proxy:  proxy.New(),
}

func Init() {
	for sdl.proxy == nil {
		time.Sleep(100 * time.Millisecond)
	}
	sdl.matrices = []*Matrix{}
	sdl.count = make(chan bool, cache.Task.ThreadNum)

	if cache.Task.ProxyMinute > 0 {
		if sdl.proxy.Count() > 0 {
			sdl.useProxy = true
			sdl.proxy.UpdateTicker(cache.Task.ProxyMinute)
			logs.Log.Informational("Use proxy IP, proxy IP replacement frequency is %v minutes", cache.Task.ProxyMinute)
		} else {
			sdl.useProxy = false
			logs.Log.Informational("The online proxy IP list is empty and can not use proxy IP")
		}
	} else {
		sdl.useProxy = false
		logs.Log.Informational("Do not use proxy IP")
	}

	sdl.status = status.RUN
}

// register the resource queue
func AddMatrix(spiderName, spiderSubName string, maxPage int64) *Matrix {
	matrix := newMatrix(spiderName, spiderSubName, maxPage)
	sdl.RLock()
	defer sdl.RUnlock()
	sdl.matrices = append(sdl.matrices, matrix)
	return matrix
}

// pause \ resume all crawling tasks
func PauseRecover() {
	sdl.Lock()
	defer sdl.Unlock()
	switch sdl.status {
	case status.PAUSE:
		sdl.status = status.RUN
	case status.RUN:
		sdl.status = status.PAUSE
	}
}

// Terminate the task
func Stop() {
	// println ("scheduler ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ^^^^^^ ")
	sdl.Lock()
	defer sdl.Unlock()
	sdl.status = status.STOP
	// emptied
	defer func() {
		recover()
	}()
	// for _, matrix: = range sdl.matrices {
	// matrix.windup ()
	//}
	close(sdl.count)
	sdl.matrices = []*Matrix{}
	// println ("scheduler $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $$$$$$ ")
}

// The average amount of resources each spider instance is assigned to
func (scheduler *scheduler) avgRes() int32 {
	avg := int32(cap(sdl.count) / len(sdl.matrices))
	if avg == 0 {
		avg = 1
	}
	return avg
}

func (scheduler *scheduler) checkStatus(s int) bool {
	scheduler.RLock()
	b := scheduler.status == s
	scheduler.RUnlock()
	return b
}
