// data collection
package pipeline

import (
	"gitlab.com/abibino-lab/mobal/app/pipeline/collector"
	"gitlab.com/abibino-lab/mobal/app/pipeline/collector/data"
	"gitlab.com/abibino-lab/mobal/app/spider"
)

// data collection / output pipeline
type Pipeline interface {
	Start()                          // Startup
	Stop()                           // Stop
	CollectData(data.DataCell) error // Collect the data unit
	CollectFile(data.FileCell) error // Collect files
}

func New(sp *spider.Spider) Pipeline {
	return collector.NewCollector(sp)
}
