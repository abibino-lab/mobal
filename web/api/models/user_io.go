package models

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	"gopkg.in/mgo.v2/bson"
)

// UserRegister definition.
type UserRegister struct {
	Email           string `form:"email" json:"email" validate:"required,email"`
	UserName        string `form:"userName" json:"userName" validate:"required,min=4,max=30"`
	Password        string `form:"password" json:"password" validate:"required,min=6,max=30"`
	ConfirmPassword string `form:"confirmPassword" json:"confirmPassword" validate:"required,min=6,max=30,eqfield=Password"`
}

// UserAuth is a models ...
type (
	UserAuth struct {
		AccountID string `json:"accountId" form:"accountId" xml:"accountId" validate:"required,email"`
		Password  string `json:"password" form:"password" xml:"password" validate:"required,min=6"`
	}
)

// GlobalInformation definition.
type GlobalInformation struct {
	ID        string `form:"id"        validate:"required;Match(/^\\w{24}$/)"`
	Username  string `form:"username"  validate:"required;MinSize(6);MaxSize(30)"`
	Mobile    string `form:"mobile"    validate:"required;Match(/^\\d{12}/)"`
	BirthDate string `form:"birthdate" validate:"required;Match(/^\\d{4}-\\d{2}-\\d{2}$/)"`
	Gender    int    `form:"gender"    validate:"required;Numeric;Range(0,2)"`
}

// LoginInfo definition.
type LoginInfo struct {
	Code     int   `json:"code"`
	UserInfo *User `json:"user"`
}

// Logout defintion.
type Logout struct {
	ID string `form:"id" json:"id" validate:"required;Match(/^\\w{24}$/)"`
}

// ChangePassword definition.
type ChangePassword struct {
	ID          bson.ObjectId `form:"id" json:"id" validate:"required;Match(/^\\w{24}$/)"`
	OldPassword string        `form:"oldPassword" json:"oldPassword" validate:"Required"`
	NewPassword string        `form:"newPassword" json:"newPassword" validate:"Required"`
}

// Uploads definition.
type Uploads struct {
	ID string `form:"id"  json:"id" validate:"required;Match(/^\\w{24}$/)"`
}

type (
	// UserBase is a struct for ...
	UserBase struct {
		Link         string      `bson:"link,omitempty" json:"link,omitempty"`
		Name         string      `bson:"name,omitempty" json:"name,omitempty"`
		Contacts     interface{} `bson:"contacts,omitempty" json:"contacts,omitempty"`
		PhotoProfile string      `bson:"photoProfile,omitempty" json:"photoProfile,omitempty"`
		Address      Address     `bson:"address,omitempty" json:"address,omitempty"`
	}
	// User is a struct for ...
	User struct {
		ID           bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
		Role         string        `bson:"role,omitempty" json:"role,omitempty"`
		UserName     string        `bson:"userName,omitempty" json:"userName,omitempty"`
		Email        string        `bson:"email,omitempty" json:"email,omitempty"`
		MobilePhone  string        `bson:"mobilePhone,omitempty" json:"mobilePhone,omitempty"`
		Password     string        `bson:"password,omitempty" json:"password,omitempty"`
		Salt         string        `bson:"salt,omitempty" json:"salt,omitempty"`
		LoginHistory interface{}   `bson:"loginHistory,omitempty" json:"loginHistory,omitempty"`
		History      interface{}   `bson:"history,omitempty" json:"history,omitempty"`
		Status       int           `bson:"status" json:"status"`
		CreatedAt    time.Time     `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
		UpdatedAt    time.Time     `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
		UpdatedBy    bson.ObjectId `bson:"updatedBy,omitempty" json:"updatedBy,omitempty"`
		CreatedBy    bson.ObjectId `bson:"createdBy,omitempty" json:"createdBy,omitempty"`
		Profile      Profile       `bson:"profile,omitempty" json:"profile,omitempty"`
	}
	// Profile is a struct for ...
	Profile struct {
		FirstName    string      `bson:"firstName,omitempty" json:"firstName,omitempty"`
		LastName     string      `bson:"lastName,omitempty" json:"lastName,omitempty"`
		Contacts     interface{} `bson:"contacts,omitempty" json:"contacts,omitempty"`
		PhotoProfile string      `bson:"photoProfile,omitempty" json:"photoProfile,omitempty"`
	}
	// JwtCustomClaims are custom claims extending default ones.
	JwtCustomClaims struct {
		/*	Name  string `json:"name" xml:"name"`
			Email string `json:"email" xml:"email"`
			Role  string `json:"role" xml:"role"`
		*/
		Identity string `json:"identity" xml:"identity"`
		jwt.StandardClaims
	}
)
