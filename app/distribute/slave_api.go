package distribute

import (
	"encoding/json"

	"gitlab.com/abibino-lab/teleport"

	"gitlab.com/abibino-lab/mobal/logs"
)

// SlaveAPI : Create a slave node API
func SlaveAPI(n Distributer) teleport.API {
	return teleport.API{
		// Receive the task from the server and join the task library
		"task": &slaveTaskHandle{n},
	}
}

// The slave node automatically receives the operation of the master node task
type slaveTaskHandle struct {
	Distributer
}

// Process :
func (slaveTaskHandle *slaveTaskHandle) Process(receive *teleport.NetData) *teleport.NetData {
	t := &Task{}
	err := json.Unmarshal([]byte(receive.Body.(string)), t)
	if err != nil {
		logs.Log.Error("JSON decoding failed %v", receive.Body)
		return nil
	}
	slaveTaskHandle.Receive(t)
	return nil
}
