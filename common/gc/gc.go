package gc

import (
	"runtime"
	"runtime/debug"
	"sync"
	"time"
)

const (
	GC_SIZE = 50 << 20 // Default 50MB
)

var (
	gcOnce sync.Once
)

// Manually release some of the memory in the heap
func ManualGC() {
	go gcOnce.Do(func() {
		tick := time.Tick(2 * time.Minute)
		for {
			<-tick
			var mem runtime.MemStats
			runtime.ReadMemStats(&mem)
			if mem.HeapReleased >= GC_SIZE {
				debug.FreeOSMemory()
				// Runtime.GC()
			}
		}
	})
}
