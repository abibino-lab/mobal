package history

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/common/mgo"
	"gitlab.com/abibino-lab/mobal/common/mysql"
	"gitlab.com/abibino-lab/mobal/common/pool"
	"gitlab.com/abibino-lab/mobal/config"
)

// Failure is a struct failed record
type Failure struct {
	tabName     string
	fileName    string
	list        map[string]*request.Request //key:url
	inheritable bool
	sync.RWMutex
}

// PullFailure is a funtion for pull a failed record
func (failure *Failure) PullFailure() map[string]*request.Request {
	list := failure.list
	failure.list = make(map[string]*request.Request)
	return list
}

// UpsertFailure update or join a failed record,
// contrast is already there, there is no record,
// The return value indicates whether there is an insert operation.
func (failure *Failure) UpsertFailure(req *request.Request) bool {
	failure.RWMutex.Lock()
	defer failure.RWMutex.Unlock()
	if failure.list[req.Unique()] != nil {
		return false
	}
	failure.list[req.Unique()] = req
	return true
}

// DeleteFailure is a function for delete the failed record
func (failure *Failure) DeleteFailure(req *request.Request) {
	failure.RWMutex.Lock()
	delete(failure.list, req.Unique())
	failure.RWMutex.Unlock()
}

// flush is a function for clear the history failed record and update
func (failure *Failure) flush(provider string) (fLen int, err error) {
	failure.RWMutex.Lock()
	defer failure.RWMutex.Unlock()
	fLen = len(failure.list)

	switch provider {
	case "mgo":
		if mgo.Error() != nil {
			err = fmt.Errorf("Fail  [Add failed record][mgo]: %v Item [ERROR]  %v\n", fLen, mgo.Error())
			return
		}
		mgo.Call(func(src pool.Src) error {
			c := src.(*mgo.MgoSrc).DB(config.DB_NAME).C(failure.tabName)
			// Delete the failed log file
			c.DropCollection()
			if fLen == 0 {
				return nil
			}

			var docs = []interface{}{}
			for key, req := range failure.list {
				docs = append(docs, map[string]interface{}{"_id": key, "failure": req.Serialize()})
			}
			c.Insert(docs...)
			return nil
		})

	case "mysql":
		_, err := mysql.DB()
		if err != nil {
			return fLen, fmt.Errorf("Fail  [Add failed record][mysql]: %v Item [PING]  %v\n", fLen, err)
		}
		table, ok := getWriteMysqlTable(failure.tabName)
		if !ok {
			table = mysql.New()
			table.SetTableName(failure.tabName).CustomPrimaryKey("id VARCHAR(255) NOT NULL PRIMARY KEY").AddColumn("failure MEDIUMTEXT")
			setWriteMysqlTable(failure.tabName, table)
			// Create a failed record table
			err = table.Create()
			if err != nil {
				return fLen, fmt.Errorf("Fail  [Add failed record][mysql]: %v Item [CREATE]  %v\n", fLen, err)
			}
		} else {
			// Empty the failed record table
			err = table.Truncate()
			if err != nil {
				return fLen, fmt.Errorf("Fail  [Add a failed record][mysql]: %v Item [TRUNCATE]  %v\n", fLen, err)
			}
		}

		// Add a failed record
		for key, req := range failure.list {
			table.AutoInsert([]string{key, req.Serialize()})
			err = table.FlushInsert()
			if err != nil {
				fLen--
			}
		}

	default:
		// Delete the failed log file
		os.Remove(failure.fileName)
		if fLen == 0 {
			return
		}

		f, _ := os.OpenFile(failure.fileName, os.O_CREATE|os.O_WRONLY, 0777)

		docs := make(map[string]string, len(failure.list))
		for key, req := range failure.list {
			docs[key] = req.Serialize()
		}
		b, _ := json.Marshal(docs)
		b = bytes.Replace(b, []byte("\u0026"), []byte("&"), -1)
		f.Write(b)
		f.Close()
	}
	return
}
