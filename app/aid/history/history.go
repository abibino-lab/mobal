package history

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/common/mgo"
	"gitlab.com/abibino-lab/mobal/common/mysql"
	"gitlab.com/abibino-lab/mobal/common/pool"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

type (
	// Historier is interface for history
	Historier interface {
		ReadSuccess(provider string, inherit bool) // Read the success record
		UpsertSuccess(string) bool                 // Update or join success records
		HasSuccess(string) bool                    // Check if there is a successful record
		DeleteSuccess(string)                      // Delete the success record
		FlushSuccess(provider string)              // I/O output is successfully logged, but unclear cache

		ReadFailure(provider string, inherit bool) // Remove the failed record
		PullFailure() map[string]*request.Request  // Pull the failed record and empty it
		UpsertFailure(*request.Request) bool       // Update or join a failed record
		DeleteFailure(*request.Request)            // Delete the failed record
		FlushFailure(provider string)              // I/O output failed to record, but unclear cache

		Empty() // Clear the cache, but not output
	}

	// History is interface for history
	History struct {
		*Success
		*Failure
		provider string
		sync.RWMutex
	}
)

const (
	// SuccessSuffix is a constanta for ...
	SuccessSuffix = config.HISTORY_TAG + "__y"
	// FailureSuffix is a constanta for ...
	FailureSuffix = config.HISTORY_TAG + "__n"
	// SuccessFile is a constanta for ...
	SuccessFile = config.HISTORY_DIR + "/" + SuccessSuffix
	// FailureFile is a constanta for ...
	FailureFile = config.HISTORY_DIR + "/" + FailureSuffix
)

// New is function for create new historier
func New(name string, subName string) Historier {
	successTabName := SuccessSuffix + "__" + name
	successFileName := SuccessFile + "__" + name
	failureTabName := FailureSuffix + "__" + name
	failureFileName := FailureFile + "__" + name
	if subName != "" {
		successTabName += "__" + subName
		successFileName += "__" + subName
		failureTabName += "__" + subName
		failureFileName += "__" + subName
	}
	return &History{
		Success: &Success{
			tabName:  util.FileNameReplace(successTabName),
			fileName: successFileName,
			new:      make(map[string]bool),
			old:      make(map[string]bool),
		},
		Failure: &Failure{
			tabName:  util.FileNameReplace(failureTabName),
			fileName: failureFileName,
			list:     make(map[string]*request.Request),
		},
	}
}

// ReadSuccess is function for read the success record
func (history *History) ReadSuccess(provider string, inherit bool) {
	history.RWMutex.Lock()
	history.provider = provider
	history.RWMutex.Unlock()

	if !inherit {
		// Do not inherit history
		history.Success.old = make(map[string]bool)
		history.Success.new = make(map[string]bool)
		history.Success.inheritable = false
		return

	} else if history.Success.inheritable {
		// This time with the last inheritance history
		return
	} else {
		// Did not inherit history last time, but this time inherited
		history.Success.old = make(map[string]bool)
		history.Success.new = make(map[string]bool)
		history.Success.inheritable = true
	}

	switch provider {
	case "mgo":
		var docs = map[string]interface{}{}
		err := mgo.Mgo(&docs, "find", map[string]interface{}{
			"Database":   config.DB_NAME,
			"Collection": history.Success.tabName,
		})
		if err != nil {
			logs.Log.Error("Fail  [Read the success record][mgo]: %v", err)
			return
		}
		for _, v := range docs["Docs"].([]interface{}) {
			history.Success.old[v.(bson.M)["_id"].(string)] = true
		}

	case "mysql":
		_, err := mysql.DB()
		if err != nil {
			logs.Log.Error("Fail  [Read the success record][mysql]: %v", err)
			return
		}
		table, ok := getReadMysqlTable(history.Success.tabName)
		if !ok {
			table = mysql.New().SetTableName(history.Success.tabName)
			setReadMysqlTable(history.Success.tabName, table)
		}
		rows, err := table.SelectAll()
		if err != nil {
			return
		}

		for rows.Next() {
			var id string
			err = rows.Scan(&id)
			history.Success.old[id] = true
		}

	default:
		f, err := os.Open(history.Success.fileName)
		if err != nil {
			return
		}
		defer f.Close()
		b, _ := ioutil.ReadAll(f)
		if len(b) == 0 {
			return
		}
		b[0] = '{'
		json.Unmarshal(append(b, '}'), &history.Success.old)
	}
	logs.Log.Informational("[Read the success record]: %v Item", len(history.Success.old))
}

// ReadFailure is a function for remove the failed record
func (history *History) ReadFailure(provider string, inherit bool) {
	history.RWMutex.Lock()
	history.provider = provider
	history.RWMutex.Unlock()

	if !inherit {
		// Do not inherit history when
		history.Failure.list = make(map[string]*request.Request)
		history.Failure.inheritable = false
		return

	} else if history.Failure.inheritable {
		// This time with the last inheritance history
		return

	} else {
		// Did not inherit history last time, but this time inherited
		history.Failure.list = make(map[string]*request.Request)
		history.Failure.inheritable = true
	}
	var fLen int
	switch provider {
	case "mgo":
		if mgo.Error() != nil {
			logs.Log.Error("Fail  [Remove the failed record][mgo]: %v", mgo.Error())
			return
		}

		var docs = []interface{}{}
		mgo.Call(func(src pool.Src) error {
			c := src.(*mgo.MgoSrc).DB(config.DB_NAME).C(history.Failure.tabName)
			return c.Find(nil).All(&docs)
		})

		fLen = len(docs)

		for _, v := range docs {
			key := v.(bson.M)["_id"].(string)
			failure := v.(bson.M)["failure"].(string)
			req, err := request.Deserialize(failure)
			if err != nil {
				continue
			}
			history.Failure.list[key] = req
		}

	case "mysql":
		_, err := mysql.DB()
		if err != nil {
			logs.Log.Error("Fail  [Remove the failed record][mysql]: %v", err)
			return
		}
		table, ok := getReadMysqlTable(history.Failure.tabName)
		if !ok {
			table = mysql.New().SetTableName(history.Failure.tabName)
			setReadMysqlTable(history.Failure.tabName, table)
		}
		rows, err := table.SelectAll()
		if err != nil {
			return
		}

		for rows.Next() {
			var key, failure string
			err = rows.Scan(&key, &failure)
			req, err := request.Deserialize(failure)
			if err != nil {
				continue
			}
			history.Failure.list[key] = req
			fLen++
		}

	default:
		f, err := os.Open(history.Failure.fileName)
		if err != nil {
			return
		}
		b, _ := ioutil.ReadAll(f)
		f.Close()

		if len(b) == 0 {
			return
		}

		docs := map[string]string{}
		json.Unmarshal(b, &docs)

		fLen = len(docs)

		for key, s := range docs {
			req, err := request.Deserialize(s)
			if err != nil {
				continue
			}
			history.Failure.list[key] = req
		}
	}

	logs.Log.Informational("[Remove the failed record]: %v Item", fLen)
}

// Empty is a funtion for clear cache, but not output
func (history *History) Empty() {
	history.RWMutex.Lock()
	history.Success.new = make(map[string]bool)
	history.Success.old = make(map[string]bool)
	history.Failure.list = make(map[string]*request.Request)
	history.RWMutex.Unlock()
}

// FlushSuccess is a function for flush. I/O output is successfully recorded, but unclear cache
func (history *History) FlushSuccess(provider string) {
	history.RWMutex.Lock()
	history.provider = provider
	history.RWMutex.Unlock()
	sucLen, err := history.Success.flush(provider)
	if sucLen <= 0 {
		return
	}
	// logs.Log.Informational(" * ")
	if err != nil {
		logs.Log.Error("%v", err)
	} else {
		logs.Log.Informational("[Add a successful record]: %v Item", sucLen)
	}
}

// FlushFailure is a function for flush. I/O output failed record, but unclear cache
func (history *History) FlushFailure(provider string) {
	history.RWMutex.Lock()
	history.provider = provider
	history.RWMutex.Unlock()
	failLen, err := history.Failure.flush(provider)
	if failLen <= 0 {
		return
	}
	// logs.Log.Informational(" * ")
	if err != nil {
		logs.Log.Error("%v", err)
	} else {
		logs.Log.Informational("[Add a failed record]: %v Item", failLen)
	}
}

var (
	readMysqlTable     = map[string]*mysql.MyTable{}
	readMysqlTableLock sync.RWMutex
)

func getReadMysqlTable(name string) (*mysql.MyTable, bool) {
	readMysqlTableLock.RLock()
	tab, ok := readMysqlTable[name]
	readMysqlTableLock.RUnlock()
	if ok {
		return tab.Clone(), true
	}
	return nil, false
}

func setReadMysqlTable(name string, tab *mysql.MyTable) {
	readMysqlTableLock.Lock()
	readMysqlTable[name] = tab
	readMysqlTableLock.Unlock()
}

var (
	writeMysqlTable     = map[string]*mysql.MyTable{}
	writeMysqlTableLock sync.RWMutex
)

func getWriteMysqlTable(name string) (*mysql.MyTable, bool) {
	writeMysqlTableLock.RLock()
	tab, ok := writeMysqlTable[name]
	writeMysqlTableLock.RUnlock()
	if ok {
		return tab.Clone(), true
	}
	return nil, false
}

func setWriteMysqlTable(name string, tab *mysql.MyTable) {
	writeMysqlTableLock.Lock()
	writeMysqlTable[name] = tab
	writeMysqlTableLock.Unlock()
}
