package config

import (
	"strings"

	"gitlab.com/abibino-lab/mobal/logs/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

const (
	VERSION   string = "v0.0.1"                                     // software information.
	AUTHOR    string = "abibino-lab"                                // software information.
	NAME      string = "Mobal ghost spider data collection"         // software information.
	FULL_NAME string = NAME + "_" + VERSION + "（by " + AUTHOR + ")" // software information.
	TAG       string = "mobal"                                      // software information.
	ICON_PNG  string = "iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAb3klEQVR42uVdB3gU1dp+Zzeb3gupkEIgkBASAoQuICBNkK6gKCo2BEUQ9F4hBruIoiB4ERAFAfWnCYhcmqKEEgiEhDSSQAqQRvqmbpn/fDMbkkDKbnYTgvd9nn12ZnfmzO73nq+eM2c4/EMQ/gdvBDeEQI1QcAjhgUD2sTXHwYbn4cy2q9l2IdvO44Dr7D2OlyBSocZfH3fn8u/X7+but+D0QTjPS9RJGCrhMZMJd4qTCRw6mAAu7OVkDJhI2UsCmLN3FWOkSg2Uq4BiBXC7GsiqBDIroFTyOKPmsV1ihJ3hXbiStvwPDyQBguCvYjqnRpizKfz9LIGuFkCFWhRqTpUo4Eom7Gom+Ar2LmP/1JiRYS0DbI0AZ0aSuxngyIhKKwcS5EBqOeNHjU1MKz75yJ/Laov/8sAREJbEj2CCX+NhDv8BdqJQE0vZiwmwTKV7exZMO7owAntaA2asrcgiILaEcafG+8ycrQoP4Kpb8/88MASEX+DNeSt8aiXFvJFOkJgyYUUUABkVhruGtznQ304k5Q/mFa6VIYGRMCPcn7vSWv/rgSAgPI7vBCkOM1PTPZQJ6DQTfEpZ612vO9OIwQ6iafozH5XMjM15z4/7uTWu1e4JWB7HB0iZ8AfZw8PKSBCIYNNbG+S4RzgBluz9YA4gV2BJeHdulaGv064JIOHLjHB6uCOs5UrgbGHbXp+EM9Ae6Mwc/MFsoLDa8CS0WwLCk3hHFqtHMeF3oogmtk2Dw/robQNQpLWfaUKpArPe687tNFTb7ZIASqp4V5wMssZACfuFl4rv9y8C+tmJ+cWhXFQqOfRm+UK8IdptnwQk8YucjfF5V9br/mY2n7/fPwiioB52FBO5M4W4yKkwwBAharsjIDyZ95CokTDMAZYnmfCV7UH6GlDoO85Z9EU3q7CIRUar9W2zXRFAGS6fiP8G2mAkc3i4UXm/f9G98GTZc3cr4EgeCngJvPUtXbQ6AWGJ/HCOx6PsSg+xi7mzDk2FMdaXoGb7mez9LHudpmPZd7NdTdHHy0xQ83YLMkXJLA/JqNRfC1qNgLCr/HhOjXBbGfowoQo1F3NNcYwuSpaF4vl81tMLFeJnTszJUax/ipmeSnUbS1UHuLLfSf6Jmci0Fd04b33aMjgBghlJwqemUrwZyFTVngn+JjMleVUshFOKFckauy6TiIUxOoaIoSolmR1VO7L7jQmNhcc4Vyh0or7h3bgL+rRlUITF8+vtTPBKLxY7p5eL6Xx7cqSGAmmAknWma+X46P3u3DstbcegBDB7/yxL3b8LtBYTJ+rx/1RYM831ZSRcLOSPv+8vGdnSdgxGQHgcb6mSIDXYBh2oNNwW9Zr7CRIcFez+zucrjMo4x/A+XHlL2zEI/p3IP+ckw2bavt2qFfT2g2Cm6VnMt+VUY5tMhbktScwMRsA78fxvbqYYd7Mdxu6tBYrsyBckyYVoLpUJc8mH/txeXdowGAH/ilOnWcs4z5J/sN1vCB4sZ/EwFf0dDQ6VKhAhVeOZDwK5VG3ONwgBCw4lm1h4di42lnIm1e04fm8tUAjtzkhwYBqRy0xSZhWKWCj99KfduQPNnWsQAt44X7LE3dZqZT6L49X/wJBTW1gaiaUK6oOpZVBX8Vi80p/7sqlz9CaAEq+yGEVUzIHtwWVMDf/hwY8AS1t7hIya2OB3VGMhs0RkULlCocKLn/bgNjbWVosJoEHyClOM4DkstpFh6L+73G+xtB2SbuXjuyKHJo8hB00miTSBGYZZn/k3PKasEwFCb4/DZI7D82x3hJkUxjaaeTbPdrrfYmlbLNViOIZkYycTKgJyphm9PgngUu4+RmsClsTwQ3kJ1phK0NNWJqoYmXsaoChS3Jt4jXICBtnysDCuf4k/49OxI9cENk4uzV5zAjvkobs6WtS1LGzI4LQ6v6WoKpcjI+4yxnua4/H+AbA2M75XHnHatUUk0DhCTiWuWFqib7g3Vy9Qb5YA6vWlcfhYyuFNplYSqmjSBCgKN6tUTY9WsbwAM1x4eFjUv8zrP52EpNtASGWyZv9AH1tGhFN9It86eBEq75BWEX5GfDRUOWmYMzgAE4Ibt6tvakkAgUhQ0LRIJd75PJD7qO53TRLwIrPzFmb4xYzDeCoV01Q/Kh3rUlwj9sk8+VrUfnajoBTL/kqDQ5dArdogIhd3rt0nLdpV5gATc0tDyFxAwa1MXIuOxFN9fOr1+vxKHvlFRejqYlfv+MU6EECg2pFciXKJCn4rg7gbzRIQHscbl6hx1MwIDxGDDZkZXfCMuxpBtpI7+5v+ikWsdTettIDQl2nCTPfa/QVHkiFz19/zlxUV4PrlSATZy/D6I6HwsLcSP1fwOFXIYefx01gzqY9ASB7rwk7mRsL3i3QkgARN0yir1Ni4ugf3YrMEvBHDf2sqxQsWRqLw9a3Rkya84qlCR5r3x1BSUY0Fh5O01gLC025KBNuLAvjmVAKSbbu3+PeoFApB8NYVt7FwdCj6+Lje+S6yENiXxZKqnGw85VyFYf6eKKtW4ecsKZ7zFI9Z1ILJijTDg+dRKVHDc1UQl9soAQuv8K+yv/k1yapUZbjkiiKCRd4qWBqLJGyJiEeMjb9O54f5idtRaTn4Ue7cot+RlRyPgtQ4zB8RjIm9arUoRQ7szQZu1bjJhAisnj5I2NzLCPkrH1jdQ/zqDT1mi7Io8j2mBe8K23d/+UYc78urEcecrXG5yvBTQkKZKZnlIW6TLwi7XAZrHSKaaY6VGOxiKmhQWKqx1ucRSvKykXIhAo/16IgXhgXX2nmWsv43jxNmRtcgLy0FYSF28HNzYKZHhQ+viZ3mSw0BC/UggAefviZQ4tUgAa/F8EeYrRpFXru1qgove1Sju5345/99NAFlLtqbEqq5LNV02tdjtTuHwsqU86eEQGDZY4Pu2PlyZudPFnA4eVu8t6AGZJ6cb1zA8okDhP2112onA38VqNu1GwMvRcDaAC6+HgELYvkpzFTvpu3WrOmQIF7zEbf/LzoNf0u9dDr/wy4KWJnK8FozQiBB3oi/BD4vHWGPDa5n52OKgT3MrBQo7j2vMDkW68b6CRqSWKTC+kzpne/WaAho7trNQa1SLl3XS/ZZPQLmx/Cx7IMebVFPW+6jQAcLGS6wxOqHMledzn3BsQQ9Xa1Zh2n8mLz0FNxOuITnBvtj1oCAO5/fqGA9LKvx6e2kLX3Kk/Hi8F7C/ruJ9UlaqyFggb4EVFftXd/HdModAuZF84M5Cf5uBVk3iMFWVXjCy0QU6Dm5TjH9I8a5mOjXAfMbEEIps/NpLLoZ7mWPRWNC79h5Civ3ZHM4V9R02wUXT2LHM0OF7eM5auzNldT7/msNAfP1JUCpuPpNiLHfHQJevcz/wt6mt4awGwILu/GexvQvOp2Haksnrc8dxGVjZqAL09jaz6jnpjPBextX443RfdHNrbaGcSgH+COvvp1vCKW3s7HAixdMlZyFnSuuSu855+ue4nvda7cU64I4Mb+fc503tShBMTM9uoUVemKZZwVcbczw4cUC3DKy1/q8/qoMBLnYYkOetWDns1NYVpSThtdH9sJwf887x11lYeW2zIbtfIOIO4l1M8XeT+c1dD/COg0BrxqAgPVBnJVAwEuXFKMkEqMjrSPmxvG0XTH6d7QRCLipAwGPSjIgte2AzZduoJjF81OCOmFm3fJBNbA1U6zHa4uclHh8NayjECGllSiwMq3hDH29hoB5BiDgmyDOXiDghXOVn0pNTJa2kpwbxRBmSmb1dMEHUdoTQD1+sbscq/6Mh5spjxeGBtUrHxzK5XDitm6/g9rsVhCDRY/0Fva/SGmcvG+CxPdXLuv"
)
const (
	WORK_ROOT      string = TAG + "_pkg"                  // software information.
	CONFIG         string = WORK_ROOT + "/config.ini"     // software information.
	CACHE_DIR      string = WORK_ROOT + "/cache"          // software information.
	LOG            string = WORK_ROOT + "/logs/mobal.log" // software information.
	LOG_ASYNC      bool   = true                          // software information.
	PHANTOMJS_TEMP string = CACHE_DIR                     // software information.
	HISTORY_TAG    string = "history"                     // software information.
	HISTORY_DIR    string = WORK_ROOT + "/" + HISTORY_TAG // software informa
	SPIDER_EXT     string = ".mobal.html"                 // software information.

)

// software information.

var (
	CRAWLS_CAP               int    = setting.DefaultInt("crawlcap", crawlcap) // software information.
	PHANTOMJS                string = setting.String("phantomjs")              // software information.
	PROXY                    string = setting.String("proxylib")               // software information.
	SPIDER_DIR               string = setting.String("spiderdir")              // software information.
	FILE_DIR                 string = setting.String("fileoutdir")             // software information.
	TEXT_DIR                 string = setting.String("textoutdir")             // software information.
	DB_NAME                  string = setting.String("dbname")
	RUNMODE                  string = setting.DefaultString("runmode", "prod")                   // software information.
	MGO_CONN_STR             string = setting.String("mgo::connstring")                          // software information.
	MGO_CONN_CAP             int    = setting.DefaultInt("mgo::conncap", mgoconncap)             // software informati
	MGO_CONN_GC_SECOND       int64  = setting.DefaultInt64("mgo::conngcsecond", mgoconngcsecond) // software information.
	MGO_HOST                 string = setting.String("mgo::host")
	MGO_PORT                 string = setting.String("mgo::port")
	ES_HOST                  string = setting.String("es::host")
	ES_PORT                  string = setting.String("es::port")                                                   //
	MYSQL_CONN_STR           string = setting.String("mysql::connstring")                                          // software information.
	MYSQL_CONN_CAP           int    = setting.DefaultInt("mysql::conncap", mysqlconncap)                           // software information.
	MYSQL_MAX_ALLOWED_PACKET int    = setting.DefaultInt("mysql::maxallowedpacket", mysqlmaxallowedpacketmb) << 20 // software information.
	KAFKA_BORKERS            string = setting.DefaultString("kafka::brokers", kafkabrokers)                        // software information.
	LOG_CAP                  int64  = setting.DefaultInt64("log::cap", logcap)                                     // software information.
	LogLevel                 int    = logLevel(setting.String("log::level"))                                       // software information.
	LOG_CONSOLE_LEVEL        int    = logLevel(setting.String("log::consolelevel"))                                // software information.
	LOG_FEEDBACK_LEVEL       int    = logLevel(setting.String("log::feedbacklevel"))                               // software information.
	LOG_LINEINFO             bool   = setting.DefaultBool("log::lineinfo", loglineinfo)                            // software information.
	LOG_SAVE                 bool   = setting.DefaultBool("log::save", logsave)                                    // software information.
	PRIVATEKEY_PATH          string = setting.DefaultString("api::privatekeypath", privatekeypath)
	PUBLICKEY_PATH           string = setting.DefaultString("api::publickeypath", publickeypath)
)

func init() {
	cache.Task = &cache.AppConf{
		Mode:           setting.DefaultInt("run::mode", mode),                 // software information.
		Port:           setting.DefaultInt("run::port", port),                 // software information.
		Master:         setting.String("run::master"),                         // software information.
		ThreadNum:      setting.DefaultInt("run::thread", thread),             // software information.
		Pausetime:      setting.DefaultInt64("run::pause", pause),             // software information.
		OutType:        setting.String("run::outtype"),                        // software information.
		DockerCap:      setting.DefaultInt("run::dockercap", dockercap),       // software information.
		Limit:          setting.DefaultInt64("run::limit", limit),             // software information.
		ProxyMinute:    setting.DefaultInt64("run::proxyminute", proxyminute), // software information.
		SuccessInherit: setting.DefaultBool("run::success", success),          // software information.
		FailureInherit: setting.DefaultBool("run::failure", failure),          // software information.
	}
}

func logLevel(l string) int {
	switch strings.ToLower(l) {
	case "app":
		return logs.LevelApp
	case "emergency":
		return logs.LevelEmergency
	case "alert":
		return logs.LevelAlert
	case "critical":
		return logs.LevelCritical
	case "error":
		return logs.LevelError
	case "warning":
		return logs.LevelWarning
	case "notice":
		return logs.LevelNotice
	case "informational":
		return logs.LevelInformational
	case "info":
		return logs.LevelInformational
	case "debug":
		return logs.LevelDebug
	}
	return -10
}
