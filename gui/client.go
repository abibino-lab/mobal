package gui

import (
	. "github.com/lxn/walk/declarative"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/config"
)

func clientWindow() {
	mw.Close()
	if err := (MainWindow{
		AssignTo: &mw,
		DataBinder: DataBinder{
			AssignTo:       &db,
			DataSource:     Input,
			ErrorPresenter: ErrorPresenterRef{&ep},
		},
		Title:    config.FULL_NAME + "                                                          [Operation Mode -> Client]",
		MinSize:  Size{1100, 600},
		Layout:   VBox{MarginsZero: true},
		Children: []Widget{
		// Composite{
		// 	Layout:  HBox{},
		// 	MaxSize: Size{1100, 150},
		// 	Children: []Widget{
		// 		PushButton{
		// 			MaxSize:  Size{1000, 150},
		// 			Text:     "Disconnect the server",
		// 			AssignTo: &runStopBtn,
		// 		},
		// 	},
		// },
		},
	}.Create()); err != nil {
		panic(err)
	}

	setWindow()

	// Initialize the application
	Init()

	// Perform the task
	go app.LogicApp.Run()

	// Run the form program
	mw.Run()
}

// Click the start event
// func clientStart() {

// 	if runStopBtn.Text() == "Reconnect the server" {
// 		runStopBtn.SetEnabled(false)
// 		runStopBtn.SetText("Connecting to server ...")
// 		clientStop()
// 		return
// 	}

// 	runStopBtn.SetText("Disconnect the server")

// }
