package collector

import (
	"fmt"
	"os"

	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/common/xlsx"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

/************************ Excel Output ***************************/
func init() {
	DataOutput["excel"] = func(collector *Collector) (err error) {
		defer func() {
			if p := recover(); p != nil {
				err = fmt.Errorf("%v", p)
			}
		}()

		var (
			file   *xlsx.File
			row    *xlsx.Row
			cell   *xlsx.Cell
			sheets = make(map[string]*xlsx.Sheet)
		)

		// Create a file
		file = xlsx.NewFile()

		// Add a sort data sheet
		for _, datacell := range collector.dataDocker {
			var subNamespace = util.FileNameReplace(collector.subNamespace(datacell))
			if _, ok := sheets[subNamespace]; !ok {
				// Add a worksheet
				sheet, err := file.AddSheet(subNamespace)
				if err != nil {
					logs.Log.Error("%v", err)
					continue
				}
				sheets[subNamespace] = sheet
				// Write the header
				row = sheets[subNamespace].AddRow()
				for _, title := range collector.MustGetRule(datacell["ruleName"].(string)).ItemFields {
					row.AddCell().Value = title
				}
				if collector.Spider.OutDefaultField() {
					row.AddCell().Value = "currentLink"
					row.AddCell().Value = "parentLink"
					row.AddCell().Value = "downloadTime"
				}
			}

			row = sheets[subNamespace].AddRow()
			for _, title := range collector.MustGetRule(datacell["ruleName"].(string)).ItemFields {
				cell = row.AddCell()
				vd := datacell["data"].(map[string]interface{})
				if v, ok := vd[title].(string); ok || vd[title] == nil {
					cell.Value = v
				} else {
					cell.Value = util.JsonString(vd[title])
				}
			}
			if collector.Spider.OutDefaultField() {
				row.AddCell().Value = datacell["currentLink"].(string)
				row.AddCell().Value = datacell["parentLink"].(string)
				row.AddCell().Value = datacell["downloadTime"].(string)
			}
		}
		folder := config.TEXT_DIR + "/" + cache.StartTime.Format("2006-01-02 150405")
		filename := fmt.Sprintf("%v/%v__%v-%v.xlsx", folder, util.FileNameReplace(collector.namespace()), collector.sum[0], collector.sum[1])

		// Create / open the directory
		f2, err := os.Stat(folder)
		if err != nil || !f2.IsDir() {
			if err := os.MkdirAll(folder, 0777); err != nil {
				logs.Log.Error("Error: %v", err)
			}
		}

		// Save document
		err = file.Save(filename)
		return
	}
}
