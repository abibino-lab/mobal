// Package app interface for graphical user interface.
// The basic business execution order is: New () -> [SetLog (io.Writer) ->] Init () -> SpiderPrepare () -> Run ()
package app

import (
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/abibino-lab/teleport"
	"gitlab.com/abibino-lab/mobal/app/crawler"
	"gitlab.com/abibino-lab/mobal/app/distribute"
	"gitlab.com/abibino-lab/mobal/app/pipeline"
	"gitlab.com/abibino-lab/mobal/app/pipeline/collector"
	"gitlab.com/abibino-lab/mobal/app/scheduler"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

type (
	// App inteface ...
	App interface {
		SetLog(io.Writer) App                                         // set the global log real-time display terminal
		LogGoOn() App                                                 // Continue log printing
		LogRest() App                                                 // pause log printing
		Init(mode int, port int, master string, w ...io.Writer) App   // Must be initialized before using App, except for SetLog ()
		ReInit(mode int, port int, master string, w ...io.Writer) App // Switch the run mode and reset the log print target
		GetAppConf(k ...string) interface{}                           // Get global parameters
		SetAppConf(k string, v interface{}) App                       // Set the global parameters (client mode does not call the method)
		SpiderPrepare(original []*spider.Spider) App                  // must be called after the global run parameter is run () (not called in client mode)
		Run()                                                         // Blocked operation until task completes (must be invoked after all configuration items should be configured)
		Stop()                                                        // Offline mode to terminate the task halfway (external blocking operation until the current task is terminated)
		IsRunning() bool                                              // Check if the task is running
		IsPause() bool                                                // Check if the task is in a paused state
		IsStopped() bool                                              // Check if the task has been terminated
		PauseRecover()                                                // Offline mode pause \ resume task
		Status() int                                                  // returns the current state
		GetSpiderLib() []*spider.Spider                               // Get all spider species
		GetSpiderByName(string) *spider.Spider                        // get a spider by name
		GetSpiderQueue() crawler.SpiderQueue                          // Get the spider queue interface instance
		GetOutputLib() []string                                       // Get all output mode
		GetTaskJar() *distribute.TaskJar                              // return to the task library
		distribute.Distributer                                        // Implement a distributed interface
	}
	// Logic struct ...
	Logic struct {
		*cache.AppConf                      // global configuration
		*spider.SpiderSpecies               // all spider species
		crawler.SpiderQueue                 // the current task of the spider queue
		*distribute.TaskJar                 // The repository that delivers the task between the server and the client
		crawler.Pool                        // crawling the pool
		teleport.Teleport                   // socket long connection duplex communication interface, json data transmission
		sum                   [2]uint64     // Execute count
		takeTime              time.Duration // Execute timing
		status                int           // Operating status
		finish                chan bool
		finishOnce            sync.Once
		canSocketLog          bool
		sync.RWMutex
	}
)

/*
 * Task runtime common configuration
type AppConf struct {
	Mode           int    // node role
	Port           int    // The master node port
	Master         string // server (master node) address, no port
	ThreadNum      int    // global maximum concurrency
	Pausetime      int64  // pause duration reference / ms (random: Pausetime / 2 ~ Pausetime * 2)
	OutType        string // output method
	DockerCap      int    // Subdivide container capacity
	DockerQueueCap int    // Segment output pool capacity, not less than 2
	SuccessInherit bool   // inherit history success record
	FailureInherit bool   // inherit history failure record
	Limit          int64  // set the upper limit, 0 is not limited, if the rule is set to the initial value of Limit is a custom limit, otherwise the default limit request number
	ProxyMinute    int64  // The number of minutes of proxy IP replacement
	// Optional
	KeyIns string // Custom input, late cut into multiple tasks KeyIn custom configuration
}
*/

// LogicApp is variable ...
// The only global core interface instance
var LogicApp = New()

// New is a function ...
func New() App {
	return newLogic()
}

func newLogic() *Logic {
	return &Logic{
		AppConf:       cache.Task,
		SpiderSpecies: spider.Species,
		status:        status.STOPPED,
		Teleport:      teleport.New(),
		TaskJar:       distribute.NewTaskJar(),
		SpiderQueue:   crawler.NewSpiderQueue(),
		Pool:          crawler.NewCrawlerPool(),
	}
}

// SetLog is a function ...
// set the global log real-time display terminal
func (logic *Logic) SetLog(w io.Writer) App {
	logs.Log.SetOutput(w)
	return logic
}

// LogRest is a function ...
// pause log printing
func (logic *Logic) LogRest() App {
	logs.Log.Rest()
	return logic
}

// LogGoOn is a function ...
// Continue log printing
func (logic *Logic) LogGoOn() App {
	logs.Log.GoOn()
	return logic
}

// GetAppConf is a function ...
// Get global parameters
func (logic *Logic) GetAppConf(k ...string) interface{} {
	defer func() {
		if err := recover(); err != nil {
			logs.Log.Error(fmt.Sprintf("%v", err))
		}
	}()
	if len(k) == 0 {
		return logic.AppConf
	}
	key := strings.Title(k[0])
	acv := reflect.ValueOf(logic.AppConf).Elem()
	return acv.FieldByName(key).Interface()
}

// SetAppConf is a function ...
// set global parameters
func (logic *Logic) SetAppConf(k string, v interface{}) App {
	defer func() {
		if err := recover(); err != nil {
			logs.Log.Error(fmt.Sprintf("%v", err))
		}
	}()
	if k == "Limit" && v.(int64) <= 0 {
		v = int64(spider.Limit)
	} else if k == "DockerCap" && v.(int) < 1 {
		v = int(1)
	}
	acv := reflect.ValueOf(logic.AppConf).Elem()
	key := strings.Title(k)
	if acv.FieldByName(key).CanSet() {
		acv.FieldByName(key).Set(reflect.ValueOf(v))
	}

	return logic
}

// Init is a function ...
// use Init before you must use Init to initialize (except SetLog ())
func (logic *Logic) Init(mode int, port int, master string, w ...io.Writer) App {
	logic.canSocketLog = false
	if len(w) > 0 {
		logic.SetLog(w[0])
	}
	logic.LogGoOn()

	logic.AppConf.Mode, logic.AppConf.Port, logic.AppConf.Master = mode, port, master
	logic.Teleport = teleport.New()
	logic.TaskJar = distribute.NewTaskJar()
	logic.SpiderQueue = crawler.NewSpiderQueue()
	logic.Pool = crawler.NewCrawlerPool()

	switch logic.AppConf.Mode {
	case status.SERVER:
		logs.Log.EnableStealOne(false)
		if logic.checkPort() {
			logs.Log.Informational("The current operating mode is: [Server] mode - ")
			logic.Teleport.SetAPI(distribute.MasterAPI(logic)).Server(":" + strconv.Itoa(logic.AppConf.Port))
		}

	case status.CLIENT:
		if logic.checkAll() {
			logs.Log.Informational("The current operating mode is: [Client] mode -")
			logic.Teleport.SetAPI(distribute.SlaveAPI(logic)).Client(logic.AppConf.Master, ":"+strconv.Itoa(logic.AppConf.Port))
			// open log between nodes
			logic.canSocketLog = true
			logs.Log.EnableStealOne(true)
			go logic.socketLog()
		}
	case status.OFFLINE:
		logs.Log.EnableStealOne(false)
		logs.Log.Informational("The current operating mode is: [Stand-alone] mode - ")
		return logic
	default:
		logs.Log.Warning("Please specify the correct operating mode!")
		return logic
	}
	return logic
}

// ReInit is a function ...
// Use when switching the operating mode
func (logic *Logic) ReInit(mode int, port int, master string, w ...io.Writer) App {
	if !logic.IsStopped() {
		logic.Stop()
	}
	logic.LogRest()
	if logic.Teleport != nil {
		logic.Teleport.Close()
	}
	// Wait for the end
	if mode == status.UNSET {
		logic = newLogic()
		logic.AppConf.Mode = status.UNSET
		return logic
	}
	// Reopen
	logic = newLogic().Init(mode, port, master, w...).(*Logic)
	return logic
}

// SpiderPrepare must be executed immediately after setting the global run parameters, Run ()
// original is the original spider of the spider package that has not been assigned an assignment
// A spider that has been explicitly assigned will no longer reassign KeyIn
// client mode does not call this method
func (logic *Logic) SpiderPrepare(original []*spider.Spider) App {
	logic.SpiderQueue.Reset()
	// traverse the task
	for _, sp := range original {
		spcopy := sp.Copy()
		spcopy.SetPausetime(logic.AppConf.Pausetime)
		if spcopy.GetLimit() == spider.Limit {
			spcopy.SetLimit(logic.AppConf.Limit)
		} else {
			spcopy.SetLimit(-1 * logic.AppConf.Limit)
		}
		logic.SpiderQueue.Add(spcopy)
	}
	// traverse the custom configuration
	logic.SpiderQueue.AddKeyIns(logic.AppConf.KeyIns)
	return logic
}

// GetOutputLib is a function ...
// Get all output mode
func (logic *Logic) GetOutputLib() []string {
	return collector.DataOutputLib
}

// GetSpiderLib is a function ...
// Get all spider species
func (logic *Logic) GetSpiderLib() []*spider.Spider {
	return logic.SpiderSpecies.Get()
}

// GetSpiderByName is a function ...
// get a spider by name
func (logic *Logic) GetSpiderByName(name string) *spider.Spider {
	return logic.SpiderSpecies.GetByName(name)
}

// GetMode is a function ...
// return to the current run mode
func (logic *Logic) GetMode() int {
	return logic.AppConf.Mode
}

// GetTaskJar is a function ...
// return to the task library
func (logic *Logic) GetTaskJar() *distribute.TaskJar {
	return logic.TaskJar
}

// CountNodes is a function ...
// The number of nodes returned in the server client mode
func (logic *Logic) CountNodes() int {
	return logic.Teleport.CountNodes()
}

// GetSpiderQueue is a function ...
// Get the spider queue interface instance
func (logic *Logic) GetSpiderQueue() crawler.SpiderQueue {
	return logic.SpiderQueue
}

// Run is a function for run the task
func (logic *Logic) Run() {
	// Make sure to open the report
	logic.LogGoOn()
	if logic.AppConf.Mode != status.CLIENT && logic.SpiderQueue.Len() == 0 {
		logs.Log.Warning("Task list can not be empty!")
		logic.LogRest()
		return
	}
	logic.finish = make(chan bool)
	logic.finishOnce = sync.Once{}
	// reset the count
	logic.sum[0], logic.sum[1] = 0, 0
	// reset the timer
	logic.takeTime = 0
	// set the state
	logic.setStatus(status.RUN)
	defer logic.setStatus(status.STOPPED)
	// task execution
	switch logic.AppConf.Mode {
	case status.OFFLINE:
		logic.offline()
	case status.SERVER:
		logic.server()
	case status.CLIENT:
		logic.client()
	default:
		return
	}
	<-logic.finish
}

// PauseRecover is a function ...
// Offline mode pause \ resume task
func (logic *Logic) PauseRecover() {
	switch logic.Status() {
	case status.PAUSE:
		logic.setStatus(status.RUN)
	case status.RUN:
		logic.setStatus(status.PAUSE)
	}

	scheduler.PauseRecover()
}

// stop is a function ...
// Offline mode to terminate the task halfway
func (logic *Logic) Stop() {
	if logic.status == status.STOPPED {
		return
	}
	if logic.status != status.STOP {
		// can not reverse the order of the stop
		logic.setStatus(status.STOP)
		// println ("scheduler.Stop ()")
		scheduler.Stop()
		// println ("logic.Pool.Stop ()")
		logic.Pool.Stop()
	}
	// println ("wait logic.IsStopped ()")
	for !logic.IsStopped() {
		time.Sleep(time.Second)
	}
}

// IsRunning is a function ...
// Check if the task is running
func (logic *Logic) IsRunning() bool {
	return logic.status == status.RUN
}

// IsPause is a function ...
// Check if the task is in a paused state
func (logic *Logic) IsPause() bool {
	return logic.status == status.PAUSE
}

// IsStopped is a function ...
// Check if the task has been terminated
func (logic *Logic) IsStopped() bool {
	return logic.status == status.STOPPED
}

// status is a function ...
// return to the current running state
func (logic *Logic) Status() int {
	logic.RWMutex.RLock()
	defer logic.RWMutex.RUnlock()
	return logic.status
}

// setStatus is a function ...
// return to the current running state
func (logic *Logic) setStatus(status int) {
	logic.RWMutex.Lock()
	defer logic.RWMutex.Unlock()
	logic.status = status
}

// ******************************************** Private Method ************************************************* \\
// Offline mode running
func (logic *Logic) offline() {
	logic.exec()
}

// server mode to run, must be in SpiderPrepare () after the implementation of the call can be successfully added tasks
// The generated task is the same as the current global configuration
func (logic *Logic) server() {
	// mark the end
	defer func() {
		logic.finishOnce.Do(func() { close(logic.finish) })
	}()

	// facilitate adding tasks to the library
	tasksNum, spidersNum := logic.addNewTask()

	if tasksNum == 0 {
		return
	}

	// Print the report
	logs.Log.Informational("+-----------------------------------------------------------------------------------------------------------------------------------+")
	logs.Log.Informational(" The success of adding %v task, contains a total of %v collection rules -", tasksNum, spidersNum)
	logs.Log.Informational("+-----------------------------------------------------------------------------------------------------------------------------------+")
}

// In server mode, generate a task and add it to the library
func (logic *Logic) addNewTask() (tasksNum, spidersNum int) {
	length := logic.SpiderQueue.Len()
	t := distribute.Task{}
	// read from the configuration field
	logic.setTask(&t)

	for i, sp := range logic.SpiderQueue.GetAll() {

		t.Spiders = append(t.Spiders, map[string]string{"name": sp.GetName(), "keyIn": sp.GetKeyIn()})
		spidersNum++

		// Every ten spiders as a task
		if i > 0 && i%10 == 0 && length > 10 {
			// deposit
			one := t
			logic.TaskJar.Push(&one)
			// logs.Log.App ("* [Add Task] Details:% # v", * t)

			tasksNum++

			// empty spider
			t.Spiders = []map[string]string{}
		}
	}

	if len(t.Spiders) != 0 {
		// deposit
		one := t
		logic.TaskJar.Push(&one)
		tasksNum++
	}
	return
}

// client mode running
func (logic *Logic) client() {
	// mark the end
	defer func() {
		logic.finishOnce.Do(func() { close(logic.finish) })
	}()

	for {
		// Get a task from the task library
		t := logic.downTask()

		if logic.Status() == status.STOP || logic.Status() == status.STOPPED {
			return
		}

		// ready to run
		logic.taskToRun(t)

		// reset the count
		logic.sum[0], logic.sum[1] = 0, 0
		// reset the timer
		logic.takeTime = 0

		// perform the task
		logic.exec()
	}
}

// get the task in client mode
func (logic *Logic) downTask() *distribute.Task {
ReStartLabel:
	if logic.Status() == status.STOP || logic.Status() == status.STOPPED {
		return nil
	}
	if logic.CountNodes() == 0 && logic.TaskJar.Len() == 0 {
		time.Sleep(time.Second)
		goto ReStartLabel
	}

	if logic.TaskJar.Len() == 0 {
		logic.Request(nil, "task", "")
		for logic.TaskJar.Len() == 0 {
			if logic.CountNodes() == 0 {
				goto ReStartLabel
			}
			time.Sleep(time.Second)
		}
	}
	return logic.TaskJar.Pull()
}

// client mode from the task to prepare the operating conditions
func (logic *Logic) taskToRun(t *distribute.Task) {
	// Clear the history task
	logic.SpiderQueue.Reset()

	// change the global configuration
	logic.setAppConf(t)

	// initialize the spider queue
	for _, n := range t.Spiders {
		sp := logic.GetSpiderByName(n["name"])
		if sp == nil {
			continue
		}
		spcopy := sp.Copy()
		spcopy.SetPausetime(t.Pausetime)
		if spcopy.GetLimit() > 0 {
			spcopy.SetLimit(t.Limit)
		} else {
			spcopy.SetLimit(-1 * t.Limit)
		}
		if v, ok := n["keyIn"]; ok {
			spcopy.SetKeyIn(v)
		}
		logic.SpiderQueue.Add(spcopy)
	}
}

// start the task
func (logic *Logic) exec() {
	count := logic.SpiderQueue.Len()
	cache.ResetPageCount()
	// Refresh the status of the output method
	pipeline.RefreshOutput()
	// initialize the resource queue
	scheduler.Init()

	// set the crawler queue
	crawlerCap := logic.Pool.Reset(count)

	logs.Log.Informational("Total number of tasks (number of tasks [* custom configuration number]) %v", count)
	logs.Log.Informational("Acquisition engine pool capacity %v", crawlerCap)
	logs.Log.Informational("Concurrency up to %v", logic.AppConf.ThreadNum)
	logs.Log.Informational("Random stop interval %v - %v milliseconds", logic.AppConf.Pausetime/2, logic.AppConf.Pausetime*2)
	logs.Log.App("Start crawling, please be patient")
	logs.Log.Informational("+-----------------------------------------------------------------------------------------------------------------------------------+")

	// start the timer
	cache.StartTime = time.Now()

	// Choose a reasonable concurrency according to the pattern
	if logic.AppConf.Mode == status.OFFLINE {
		// control execution status
		go logic.goRun(count)
	} else {
		// To ensure the synchronization of receiving server tasks
		logic.goRun(count)
	}
}

// task execution
func (logic *Logic) goRun(count int) {
	// perform the task
	var i int
	for i = 0; i < count && logic.Status() != status.STOP; i++ {
	pause:
		if logic.IsPause() {
			time.Sleep(time.Second)
			goto pause
		}
		// Remove the free spider from the crawl queue and execute it concurrently
		c := logic.Pool.Use()
		if c != nil {
			go func(i int, c crawler.Crawler) {
				// Execute and return the result message
				c.Init(logic.SpiderQueue.GetByIndex(i)).Run()
				// After the end of the task to recover the spider
				logic.RWMutex.RLock()
				if logic.status != status.STOP {
					logic.Pool.Free(c)
				}
				logic.RWMutex.RUnlock()
			}(i, c)
		}
	}
	// monitor the end of the task
	for ii := 0; ii < i; ii++ {
		s := <-cache.ReportChan
		if (s.DataNum == 0) && (s.FileNum == 0) {
			logs.Log.App("[Subtotal: %s | KeyIn: %s] No collection results, with %v!", s.SpiderName, s.KeyIn, s.Time)
			continue
		}
		logs.Log.Informational(" * ")
		switch {
		case s.DataNum > 0 && s.FileNum == 0:
			logs.Log.App("[Subtotal: %s | KeyIn: %s] Collect data %v, %v!",
				s.SpiderName, s.KeyIn, s.DataNum, s.Time)
		case s.DataNum == 0 && s.FileNum > 0:
			logs.Log.App("[Subtotal: %s | KeyIn: %s] Total download file %v, with %v!",
				s.SpiderName, s.KeyIn, s.FileNum, s.Time)
		default:
			logs.Log.App("[Subtotal: %s | KeyIn: %s] Collect data %v + download file %v, with %v!",
				s.SpiderName, s.KeyIn, s.DataNum, s.FileNum, s.Time)
		}

		logic.sum[0] += s.DataNum
		logic.sum[1] += s.FileNum
	}

	// Always time consuming
	logic.takeTime = time.Since(cache.StartTime)
	var prefix = func() string {
		if logic.Status() == status.STOP {
			return "Task canceled halfway:"
		}
		return "This time"
	}()
	// print summary report
	logs.Log.Informational(" * ")
	logs.Log.Informational("+-----------------------------------------------------------------------------------------------------------------------------------+")
	logs.Log.Informational(" * ")
	switch {
	case logic.sum[0] > 0 && logic.sum[1] == 0:
		logs.Log.App("- %s total collection [data %v], real climb [successful %v URL + failure %v URL = total %v URL], time consuming [%v] - ",
			prefix, logic.sum[0], cache.GetPageCount(1), cache.GetPageCount(-1), cache.GetPageCount(0), logic.takeTime)
	case logic.sum[0] == 0 && logic.sum[1] > 0:
		logs.Log.App("- %s total collection [file %v], real climb [successful %v URL + failure %v URL = total %v URL], time [%v] - ",
			prefix, logic.sum[1], cache.GetPageCount(1), cache.GetPageCount(-1), cache.GetPageCount(0), logic.takeTime)
	case logic.sum[0] == 0 && logic.sum[1] == 0:
		logs.Log.App("- %s no collection results, real climb [%v URL + failure %v URL = total %v URL], time consuming [%v] - ",
			prefix, cache.GetPageCount(1), cache.GetPageCount(-1), cache.GetPageCount(0), logic.takeTime)
	default:
		logs.Log.App("- %s total collection [data %v + file %v], real climb [successful %v URL + failure %v URL = total %v URL], time [%v]",
			prefix, logic.sum[0], logic.sum[1], cache.GetPageCount(1), cache.GetPageCount(-1), cache.GetPageCount(0), logic.takeTime)
	}
	logs.Log.Informational(" * ")
	logs.Log.Informational("+-----------------------------------------------------------------------------------------------------------------------------------+")

	// Standalone mode concurrent operation, the need to mark the end of the task
	if logic.AppConf.Mode == status.OFFLINE {
		logic.LogRest()
		logic.finishOnce.Do(func() { close(logic.finish) })
	}
}

// The client feeds feedback to the server
func (logic *Logic) socketLog() {
	for logic.canSocketLog {
		_, msg, ok := logs.Log.StealOne()
		if !ok {
			return
		}
		if logic.Teleport.CountNodes() == 0 {
			// After losing connection with the server, discard the log
			continue
		}
		logic.Teleport.Request(msg, "log", "")
	}
}

func (logic *Logic) checkPort() bool {
	if logic.AppConf.Port == 0 {
		logs.Log.Warning("Distributed port can not be empty!")
		return false
	}
	return true
}

func (logic *Logic) checkAll() bool {
	if logic.AppConf.Master == "" || !logic.checkPort() {
		logs.Log.Warning("The server address can not be empty!")
		return false
	}
	return true
}

// set the task runtime common configuration
func (logic *Logic) setAppConf(task *distribute.Task) {
	logic.AppConf.ThreadNum = task.ThreadNum
	logic.AppConf.Pausetime = task.Pausetime
	logic.AppConf.OutType = task.OutType
	logic.AppConf.DockerCap = task.DockerCap
	logic.AppConf.SuccessInherit = task.SuccessInherit
	logic.AppConf.FailureInherit = task.FailureInherit
	logic.AppConf.Limit = task.Limit
	logic.AppConf.ProxyMinute = task.ProxyMinute
	logic.AppConf.KeyIns = task.KeyIns
}
func (logic *Logic) setTask(task *distribute.Task) {
	task.ThreadNum = logic.AppConf.ThreadNum
	task.Pausetime = logic.AppConf.Pausetime
	task.OutType = logic.AppConf.OutType
	task.DockerCap = logic.AppConf.DockerCap
	task.SuccessInherit = logic.AppConf.SuccessInherit
	task.FailureInherit = logic.AppConf.FailureInherit
	task.Limit = logic.AppConf.Limit
	task.ProxyMinute = logic.AppConf.ProxyMinute
	task.KeyIns = logic.AppConf.KeyIns
}
