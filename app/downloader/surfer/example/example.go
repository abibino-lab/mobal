package main

import (
	"gitlab.com/abibino-lab/mobal/app/downloader/surfer"
	"io/ioutil"
	"log"
	"time"
)

func main() {
	var values = "username=123456@qq.com&password=123456&login_btn=login_btn&submit=login_btn"

	// use the surf kernel to download by default
	log.Println("********************************************* Surf kernel GET download test starts *********************************************")
	resp, err := surfer.Download(&surfer.DefaultRequest{
		URL: "http://www.baidu.com/ ",
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("baidu resp.Status: %s\nresp.Header: %#v\n", resp.Status, resp.Header)

	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	log.Printf("baidu resp.Body: %s\nerr: %v", b, err)

	// use the surf kernel to download by default
	log.Println("********************************************* Surf kernel POST download test starts *********************************************")
	resp, err = surfer.Download(&surfer.DefaultRequest{
		URL:      "http://accounts.lewaos.com/ ",
		Method:   "POST",
		PostData: values,
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("lewaos resp.Status: %s\nresp.Header: %#v\n", resp.Status, resp.Header)

	b, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	log.Printf("lewaos resp.Body: %s\nerr: %v", b, err)

	log.Println("********************************************* The phantomjs kernel GET download test begins *********************************************")

	// specify the use of phantomjs kernel to download
	resp, err = surfer.Download(&surfer.DefaultRequest{
		URL:          "http://www.baidu.com/ ",
		DownloaderID: 1,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("baidu resp.Status: %s\nresp.Header: %#v\n", resp.Status, resp.Header)

	b, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	log.Printf("baidu resp.Body: %s\nerr: %v", b, err)

	log.Println("********************************************* The phantomjs kernel POST download test starts *********************************************")

	// specify the use of phantomjs kernel to download
	resp, err = surfer.Download(&surfer.DefaultRequest{
		DownloaderID: 1,
		URL:          "http://accounts.lewaos.com/ ",
		Method:       "POST",
		PostData:     values,
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("lewaos resp.Status: %s\nresp.Header: %#v\n", resp.Status, resp.Header)

	b, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	log.Printf("lewaos resp.Body: %s\nerr: %v", b, err)

	surfer.DestroyJsFiles()

	time.Sleep(10e9)
}
