package kafka

import (
	"fmt"
	"strings"
	"sync"

	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"

	"github.com/Shopify/sarama"
)

var (
	err      error
	producer sarama.SyncProducer
	lock     sync.RWMutex
	once     sync.Once
)

type KafkaSender struct {
	topic string
}

func GetProducer() (sarama.SyncProducer, error) {
	return producer, err
}

// Refresh the producer
func Refresh() {
	once.Do(func() {
		conf := sarama.NewConfig()
		conf.Producer.RequiredAcks = sarama.WaitForAll // Wait for all backups to return to ack
		conf.Producer.Retry.Max = 10                   // Number of retries
		conf.Producer.Return.Successes = true
		brokerList := config.KAFKA_BORKERS
		producer, err = sarama.NewSyncProducer(strings.Split(brokerList, ","), conf)
		if err != nil {
			logs.Log.Error("Kafka:%v", err)
		}
	})
}

func New() *KafkaSender {
	return &KafkaSender{}
}

func (p *KafkaSender) SetTopic(topic string) {
	p.topic = topic
}

func (p *KafkaSender) Push(data map[string]interface{}) error {
	val := util.JsonString(data)
	_, _, err := producer.SendMessage(&sarama.ProducerMessage{
		Topic: p.topic,
		Value: sarama.StringEncoder(val),
	})
	if err != nil {
		fmt.Println(err)
	}
	return err
}
