package models

import (
	"gitlab.com/abibino-lab/mobal/web/api/middleware/mymongo"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// New is a function for ...
func (loc *Location) New() {
	loc.ID = bson.NewObjectId()
	loc.CreatedAt = loc.ID.Time()
	loc.UpdatedAt = loc.ID.Time()
}

// Insert a function for add new data category
func (loc *Location) Insert() (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()
	// Collection Item
	c := ds.Session.DB("mobal").C("locations")

	// Index
	index := mgo.Index{
		Key:        []string{"node", "parentNode", "name", "keyword", "type", "internationalName"},
		Unique:     false,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	err = c.EnsureIndex(index)

	if err != nil {
		code = ErrDatabase
	} else {
		err = c.Insert(loc)

		if err != nil {
			if mgo.IsDup(err) {
				code = ErrDupRows
			}
			code = ErrDatabase
		}
	}
	return
}
