package crawler

import (
	. "gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/logs"
)

type (
	// SpiderQueue is a interface for collect the rule queue in the engine
	SpiderQueue interface {
		Reset() // Reset the queue
		Add(*Spider)
		AddAll([]*Spider)
		AddKeyIns(string) // Add the KeyIn property to the queue member traversal, but the prerequisite must be that the queue member has not been added to the keyin
		GetByIndex(int) *Spider
		GetByName(string) *Spider
		GetAll() []*Spider
		Len() int // Return queue length
	}
	sq struct {
		list []*Spider
	}
)

// NewSpiderQueue is a function for create new spider queue
func NewSpiderQueue() SpiderQueue {
	return &sq{
		list: []*Spider{},
	}
}

func (sq *sq) Reset() {
	sq.list = []*Spider{}
}

func (sq *sq) Add(sp *Spider) {
	sp.SetID(sq.Len())
	sq.list = append(sq.list, sp)
}

func (sq *sq) AddAll(list []*Spider) {
	for _, v := range list {
		sq.Add(v)
	}
}

// add keyin, traverse the spider queue to get a new queue (the spider that has been explicitly assigned will no longer reassign KeyIn)
func (sq *sq) AddKeyIns(keyIns string) {
	keyInSlice := util.KeyInsParse(keyIns)
	if len(keyInSlice) == 0 {
		return
	}

	unit1 := []*Spider{} // can not be added to a custom configuration spider
	unit2 := []*Spider{} // can be added to the custom configuration of the spider
	for _, v := range sq.GetAll() {
		if v.GetKeyIn() == KeyIn {
			unit2 = append(unit2, v)
			continue
		}
		unit1 = append(unit1, v)
	}

	if len(unit2) == 0 {
		logs.Log.Warning("This batch of tasks do not need to fill in the custom configuration!")
		return
	}

	sq.Reset()

	for _, keyIn := range keyInSlice {
		for _, v := range unit2 {
			v.KeyIn = keyIn
			nv := v
			sq.Add(nv.Copy())
		}
	}
	if sq.Len() == 0 {
		sq.AddAll(append(unit1, unit2...))
	}

	sq.AddAll(unit1)
}

func (sq *sq) GetByIndex(idx int) *Spider {
	return sq.list[idx]
}

func (sq *sq) GetByName(n string) *Spider {
	for _, sp := range sq.list {
		if sp.GetName() == n {
			return sp
		}
	}
	return nil
}

func (sq *sq) GetAll() []*Spider {
	return sq.list
}

func (sq *sq) Len() int {
	return len(sq.list)
}
