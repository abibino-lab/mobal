package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// ItemFields is a global fields of item
var ItemFields = []string{
	"id",
	"link",
	"title",
	"meta",
	"attributes",
	"postedAt",
	"updatedAt",
	"source",
}

type (
	// Item is a struct for ...
	Item struct {
		ID          bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
		Link        string        `bson:"link,omitempty" json:"link,omitempty"`
		Name        string        `bson:"name,omitempty" json:"name,omitempty"`
		Description string        `bson:"description,omitempty" json:"description,omitempty"`
		Images      []string      `bson:"images,omitempty" json:"images,omitempty"`
		Attributes  interface{}   `bson:"attributes,omitempty" json:"attributes,omitempty"`
		Address     Address       `bson:"address,omitempty" json:"address,omitempty"`
		Categories  []string      `bson:"categories,omitempty" json:"categories,omitempty"`
		Keywords    string        `bson:"keywords,omitempty" json:"keywords,omitempty"`
		User        UserBase      `bson:"user,omitempty" json:"user,omitempty"`
		Status      int           `bson:"status" json:"status"`
		PostedAt    time.Time     `bson:"postedAt,omitempty" json:"postedAt,omitempty"`
		CreatedAt   time.Time     `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
		UpdatedAt   time.Time     `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
		UpdatedBy   bson.ObjectId `bson:"updatedBy,omitempty" json:"updatedBy,omitempty"`
		CreatedBy   bson.ObjectId `bson:"createdBy,omitempty" json:"createdBy,omitempty"`
	}

	// DefaultProductAttribute is a struct for ...
	DefaultProductAttribute struct {
		Brand string `bson:"brand,omitempty" json:"brand,omitempty"`
		Type  string `bson:"type,omitempty" json:"type,omitempty"`
	}
	// SaleAttribute is a struct for ...
	SaleAttribute struct {
		Price    string `bson:"price,omitempty" json:"price,omitempty"`
		Currency string `bson:"currency,omitempty" json:"currency,omitempty"`
	}

	// Address is a struct for ...
	Address struct {
		Country    string     `bson:"country,omitempty" json:"country,omitempty"`
		State      string     `bson:"state,omitempty" json:"state,omitempty"`
		City       string     `bson:"city,omitempty" json:"city,omitempty"`
		Coordinate Coordinate `bson:"coordinate,omitempty" json:"coordinate,omitempty"`
	}

	// Coordinate  is a struct for ...
	Coordinate struct {
		Latitude  string `bson:"latitude,omitempty" json:"latitude,omitempty"`
		Longitude string `bson:"longitude,omitempty" json:"longitude,omitempty"`
	}
	// Attributes a struct for ...
	Attributes struct {
		SaleAttribute map[string]interface{} `bson:"saleAttribute,omitempty" json:"saleAttribute,omitempty"`
		Spesification map[string]interface{} `bson:"spesification,omitempty" json:"spesification,omitempty"`
	}
	// MotorCycleSpesification a struct for ...
	MotorCycleSpesification struct {
		DefaultProductAttribute
		CreatedAt      time.Time          `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
		Fuel           string             `bson:"fuel,omitempty" json:"fuel,omitempty"`
		EngineCapacity AttributeKeyValInt `bson:"engineCapacity,omitempty" json:"engineCapacity,omitempty"`
		teleportion    string             `bson:"teleportion,omitempty" json:"teleportion,omitempty"`
	}
	// AttributeKeyValInt a struct for ...
	AttributeKeyValInt struct {
		Value        float32 `bson:"value,omitempty" json:"value,omitempty"`
		Denomination string  `bson:"denomination,omitempty" json:"denomination,omitempty"`
	}

	// Items is a struct for ...
	Items []map[string]interface{}
)

func (s Items) Len() int {
	return len(s)
}

func (s Items) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s Items) Less(i, j int) bool {
	s1 := s[i]["score_matching"].(float64)
	s2 := s[j]["score_matching"].(float64)
	return s1 > s2
}
