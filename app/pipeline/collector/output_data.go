package collector

import (
	"gitlab.com/abibino-lab/mobal/logs"
)

var (
	// DataOutput is global support for output mode
	DataOutput = make(map[string]func(collector *Collector) error)

	// DataOutputLib is list of global supported text data output methods
	DataOutputLib []string
)

// Text data output
func (collector *Collector) outputData() {
	defer func() {
		// Recall the cache block
		collector.resetDataDocker()
	}()

	// output
	dataLen := uint64(len(collector.dataDocker))
	if dataLen == 0 {
		return
	}

	defer func() {
		if p := recover(); p != nil {
			logs.Log.Informational(" * ")
			logs.Log.Emergency("Panic  [Data output:%v | KeyIn:%v | Batch:%v] Data %v item! [ERROR] %v",
				collector.Spider.GetName(), collector.Spider.GetKeyIn(), collector.dataBatch, dataLen, p)
		}
	}()

	// Output statistics
	collector.addDataSum(dataLen)

	// Execute the output
	err := DataOutput[collector.outType](collector)

	logs.Log.Informational(" * ")
	if err != nil {
		logs.Log.Error("Fail  [Data output:%v | KeyIn:%v | Batch:%v] Data %v item! [ERROR] %v",
			collector.Spider.GetName(), collector.Spider.GetKeyIn(), collector.dataBatch, dataLen, err)
	} else {
		logs.Log.App("[Data output:%v | KeyIn:%v | Batch:%v] Data %v item!",
			collector.Spider.GetName(), collector.Spider.GetKeyIn(), collector.dataBatch, dataLen)
		collector.Spider.TryFlushSuccess()
	}
}
