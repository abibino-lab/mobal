// Copyright 2015 abibino-lab Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package surfer s a Go language prepared by the high concurrent web downloader, support GET/POST/HEAD Method and method http/https Protocol, while supporting the fixed UserAgent automatically save the cookie with a random large number of UserAgent disabled cookie two modes, a high degree of simulation of the browser behavior, enabling analog login and other functions.
package surfer

import (
	"net/http"
	"sync"
	// "os"
	// "path"
	// "path/filepath"
)

var (
	surf        Surfer
	phantom     Surfer
	splash      Surfer
	onceSurf    sync.Once
	oncePhantom sync.Once
	onceSplash  sync.Once
	tempJsDir   = "./tmp"
	// phantomjsFile = filepath.Clean(path.Join(os.Getenv("GOPATH"), `/src/gitlab.com/abibino-lab/surfer/phantomjs/phantomjs"))
	phantomjsFile = `./phantomjs`
)

// Download is a function for download HTML from target url
func Download(req Request) (resp *http.Response, err error) {
	switch req.GetDownloaderID() {
	case SurfID:
		onceSurf.Do(func() { surf = New() })
		resp, err = surf.Download(req)
	case PhomtomJsID:
		oncePhantom.Do(func() { phantom = NewPhantom(phantomjsFile, tempJsDir) })
		resp, err = phantom.Download(req)
	case SplashID:
		onceSplash.Do(func() { splash = New() })
	}
	return
}

// DestroyJsFiles is a funtion for destroy phantomjs js temporary files
func DestroyJsFiles() {
	if pt, ok := phantom.(*Phantom); ok {
		pt.DestroyJsFiles()
	}
}

// DestroyLuaScriptFiles is a funtion for destroy phantomjs js temporary files
func DestroyLuaScriptFiles() {
	if pt, ok := splash.(*Splash); ok {
		pt.DestroyLuaScriptFiles()
	}
}

// Surfer is a function downloader represents a core of HTTP web browser for crawler.
type Surfer interface {
	// GET @param url string, header http.Header, cookies []*http.Cookie
	// HEAD @param url string, header http.Header, cookies []*http.Cookie
	// POST PostForm @param url, referer string, values url.Values, header http.Header, cookies []*http.Cookie
	// POST-M PostMultipart @param url, referer string, values url.Values, header http.Header, cookies []*http.Cookie
	Download(Request) (resp *http.Response, err error)
}
