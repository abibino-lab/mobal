package status

// Run mode
const (
	UNSET int = iota - 1
	OFFLINE
	SERVER
	CLIENT
)

// data header information
const (
	// task request header
	REQTASK = iota + 1
	// Task Response Header
	TASK
	// print the header
	LOG
)

// Operating status
const (
	STOPPED = iota - 1
	STOP
	RUN
	PAUSE
)
