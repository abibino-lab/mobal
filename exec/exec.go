package exec

import (
	"flag"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/cmd"
	"gitlab.com/abibino-lab/mobal/common/gc"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
	"gitlab.com/abibino-lab/mobal/web"
)

var (
	uiflag             *string
	modeflag           *int
	portflag           *int
	masterflag         *string
	keyinsflag         *string
	limitflag          *int64
	outputflag         *string
	threadflag         *int
	pauseflag          *int64
	proxyflag          *int64
	dockerflag         *int
	successInheritflag *bool
	failureInheritflag *bool
)

func init() {
	// Open the maximum number of cores to run
	runtime.GOMAXPROCS(runtime.NumCPU())
	// Turn on manual GC
	gc.ManualGC()
}

func DefaultRun(uiDefault string) {
	logs.Log.Informational("%v", config.FULL_NAME)
	flag.String("a *********************************************** Common *********************************************** -a", "", "")
	// interface
	uiflag = flag.String("_ui", uiDefault, "   <Select the user interface> [web] [gui] [cmd]")
	flagCommon()
	web.Flag()
	cmd.Flag()
	flag.String("z", "", "README:   Parameter setting reference [xxx] Prompt when the parameter contains multiple values \",\" interval \r\n")
	flag.Parse()
	writeFlag()
	run(*uiflag)
}

func flagCommon() {
	// Run mode
	modeflag = flag.Int(
		"a_mode",
		cache.Task.Mode,
		"   <Running mode: ["+strconv.Itoa(status.OFFLINE)+"] Stand-alone    ["+strconv.Itoa(status.SERVER)+"] Server    ["+strconv.Itoa(status.CLIENT)+"] Client>")

	// port number, non-stand-alone mode
	portflag = flag.Int(
		"a_port",
		cache.Task.Port,
		"   <Port number: only fill in the number can be, without a colon, stand-alone mode does not fill>")

	// master node ip, client mode fill
	masterflag = flag.String(
		"a_master",
		cache.Task.Master,
		"   <Server IP: No port, client mode used>")

	// Custom configuration
	keyinsflag = flag.String(
		"a_keyins",
		cache.Task.KeyIns,
		"   <Custom configuration: multi-task, please separate a layer of \"<>\">")

	// Collect the upper limit
	limitflag = flag.Int64(
		"a_limit",
		cache.Task.Limit,
		"   <Collection cap (default limit URL)> [> = 0]")

	// output method
	outputflag = flag.String(
		"a_outtype",
		cache.Task.OutType,
		func() string {
			var outputlib string
			for _, v := range app.LogicApp.GetOutputLib() {
				outputlib += "[" + v + "] "
			}
			return "   <Output method: > " + strings.TrimRight(outputlib, " ")
		}())

	// Concurrent access number
	threadflag = flag.Int(
		"a_thread",
		cache.Task.ThreadNum,
		"   <Concurrent Escort> [1~99999]")

	// Average pause time
	pauseflag = flag.Int64(
		"a_pause",
		cache.Task.Pausetime,
		"   <Average pause time/ms> [>=100]")

	// proxy IP replacement frequency
	proxyflag = flag.Int64(
		"a_proxyminute",
		cache.Task.ProxyMinute,
		"   <Proxy IP replacement frequency: /m, 0 is not used when the agent> [>=0]")

	// Batch output
	dockerflag = flag.Int(
		"a_dockercap",
		cache.Task.DockerCap,
		"   <Batch output> [1~5000000]")

	// inherit history success record
	successInheritflag = flag.Bool(
		"a_success",
		cache.Task.SuccessInherit,
		"   <Inherit and save success records> [true] [false]")

	// inherit history failure record
	failureInheritflag = flag.Bool(
		"a_failure",
		cache.Task.FailureInherit,
		"   <Inherit and save the failed record> [true] [false]")
}

func writeFlag() {
	cache.Task.Mode = *modeflag
	cache.Task.Port = *portflag
	cache.Task.Master = *masterflag
	cache.Task.KeyIns = *keyinsflag
	cache.Task.Limit = *limitflag
	cache.Task.OutType = *outputflag
	cache.Task.ThreadNum = *threadflag
	cache.Task.Pausetime = *pauseflag
	cache.Task.ProxyMinute = *proxyflag
	cache.Task.DockerCap = *dockerflag
	cache.Task.SuccessInherit = *successInheritflag
	cache.Task.FailureInherit = *failureInheritflag
}
