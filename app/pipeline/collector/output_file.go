package collector

import (
	"bytes"
	"io"
	"os"
	"path/filepath"
	"sync/atomic"

	"gitlab.com/abibino-lab/mobal/app/pipeline/collector/data"
	bytesSize "gitlab.com/abibino-lab/mobal/common/bytes"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	// "gitlab.com/abibino-lab/mobal/runtime/cache"
)

// File output
func (collector *Collector) outputFile(file data.FileCell) {
	// Reuse FileCell
	defer func() {
		data.PutFileCell(file)
		collector.wait.Done()
	}()

	// Path: File / "ruleName" / "time" / "name"
	p, n := filepath.Split(filepath.Clean(file["name"].(string)))
	// Dir: = Filepath.Join (config.FILE_DIR, util.FileNameReplace (collector.namespace ()) + "__" + cache.StartTime.Format ("January 02, 2006 04:45 05 seconds"), P)
	dir := filepath.Join(config.FILE_DIR, util.FileNameReplace(collector.namespace()), p)

	// File name
	fileName := filepath.Join(dir, util.FileNameReplace(n))

	// Create / open the directory
	d, err := os.Stat(dir)
	if err != nil || !d.IsDir() {
		if err := os.MkdirAll(dir, 0777); err != nil {
			logs.Log.Error(
				" *     Fail  [Download Document:%v | KeyIn:%v | Batch:%v]   %v [ERROR]  %v\n",
				collector.Spider.GetName(), collector.Spider.GetKeyIn(), atomic.LoadUint64(&collector.fileBatch), fileName, err,
			)
			return
		}
	}

	// File does not exist to create the file 0777 permissions, if there is written before the clear content
	f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		logs.Log.Error(
			" *     Fail  [Download Document:%v | KeyIn:%v | Batch:%v]   %v [ERROR]  %v\n",
			collector.Spider.GetName(), collector.Spider.GetKeyIn(), atomic.LoadUint64(&collector.fileBatch), fileName, err,
		)
		return
	}

	size, err := io.Copy(f, bytes.NewReader(file["Bytes"].([]byte)))
	f.Close()
	if err != nil {
		logs.Log.Error(
			" *     Fail  [Download Document:%v | KeyIn:%v | Batch:%v]   %v (%s) [ERROR]  %v\n",
			collector.Spider.GetName(), collector.Spider.GetKeyIn(), atomic.LoadUint64(&collector.fileBatch), fileName, bytesSize.Format(uint64(size)), err,
		)
		return
	}

	// Output statistics
	collector.addFileSum(1)

	// Print the report
	logs.Log.Informational(" * ")
	logs.Log.App(
		" *     [Download Document:%v | KeyIn:%v | Batch:%v]   %v (%s)\n",
		collector.Spider.GetName(), collector.Spider.GetKeyIn(), atomic.LoadUint64(&collector.fileBatch), fileName, bytesSize.Format(uint64(size)),
	)
	logs.Log.Informational(" * ")
}
