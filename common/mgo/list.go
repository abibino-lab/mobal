package mgo

import (
	"fmt"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// incoming database list | return database and its collection tree
type List struct {
	Dbs []string // list of database names
}

func (list *List) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	resultPtr2 := resultPtr.(*map[string][]string)
	*resultPtr2 = map[string][]string{}

	err = Call(func(src pool.Src) error {
		var (
			s   = src.(*MgoSrc)
			dbs []string
		)

		if dbs, err = s.DatabaseNames(); err != nil {
			return err
		}

		if len(list.Dbs) == 0 {
			for _, dbname := range dbs {
				(*resultPtr2)[dbname], _ = s.DB(dbname).CollectionNames()
			}
			return err
		}

		for _, dbname := range list.Dbs {
			(*resultPtr2)[dbname], _ = s.DB(dbname).CollectionNames()
		}
		return err
	})

	return
}
