package controllers

import (
	"sync"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
	ws "golang.org/x/net/websocket"
)

type SocketController struct {
	connPool     map[string]*ws.Conn
	wchanPool    map[string]*Wchan
	connRWMutex  sync.RWMutex
	wchanRWMutex sync.RWMutex
}

func (socketController *SocketController) GetConn(sessID string) *ws.Conn {
	socketController.connRWMutex.RLock()
	defer socketController.connRWMutex.RUnlock()
	return socketController.connPool[sessID]
}

func (socketController *SocketController) GetWchan(sessID string) *Wchan {
	socketController.wchanRWMutex.RLock()
	defer socketController.wchanRWMutex.RUnlock()
	return socketController.wchanPool[sessID]
}

func (socketController *SocketController) Add(sessID string, conn *ws.Conn) {
	socketController.connRWMutex.Lock()
	socketController.wchanRWMutex.Lock()
	defer socketController.connRWMutex.Unlock()
	defer socketController.wchanRWMutex.Unlock()

	socketController.connPool[sessID] = conn
	socketController.wchanPool[sessID] = newWchan()
}

func (socketController *SocketController) Remove(sessID string, conn *ws.Conn) {
	socketController.connRWMutex.Lock()
	socketController.wchanRWMutex.Lock()
	defer socketController.connRWMutex.Unlock()
	defer socketController.wchanRWMutex.Unlock()

	if socketController.connPool[sessID] == nil {
		return
	}
	wc := socketController.wchanPool[sessID]
	close(wc.wchan)
	conn.Close()
	delete(socketController.connPool, sessID)
	delete(socketController.wchanPool, sessID)
}

func (socketController *SocketController) Write(sessID string, void map[string]interface{}, to ...int) {
	socketController.wchanRWMutex.RLock()
	defer socketController.wchanRWMutex.RUnlock()

	// to 1, only to the current connection; to -1, all the connections except the current connection to send; to 0 or when empty, to all connections
	var t int
	if len(to) > 0 {
		t = to[0]
	}

	void["mode"] = app.LogicApp.GetAppConf("mode").(int)

	switch t {
	case 1:
		wc := socketController.wchanPool[sessID]
		if wc == nil {
			return
		}
		void["initiative"] = true
		wc.wchan <- void

	case 0, -1:
		l := len(socketController.wchanPool)
		for _sessID, wc := range socketController.wchanPool {
			if t == -1 && _sessID == sessID {
				continue
			}
			_void := make(map[string]interface{}, l)
			for k, v := range void {
				_void[k] = v
			}
			if _sessID == sessID {
				_void["initiative"] = true
			} else {
				_void["initiative"] = false
			}
			wc.wchan <- _void
		}
	}
}

type Wchan struct {
	wchan chan interface{}
}

func newWchan() *Wchan {
	return &Wchan{
		wchan: make(chan interface{}, 1024),
	}
}

var (
	wsAPI = map[string]func(string, map[string]interface{}){}
	Sc    = &SocketController{
		connPool:  make(map[string]*ws.Conn),
		wchanPool: make(map[string]*Wchan),
	}
)

var (
	spiderMenu []map[string]string
)

func AppInit() {
	app.LogicApp.SetLog(Lsc).SetAppConf("Mode", cache.Task.Mode)

	spiderMenu = func() (spmenu []map[string]string) {
		// Get the spider family
		for _, sp := range app.LogicApp.GetSpiderLib() {
			spmenu = append(spmenu, map[string]string{"name": sp.GetName(), "description": sp.GetDescription()})
		}
		return spmenu
	}()
}

// WSHandler is a func for ...
func WSHandler(c echo.Context) (err error) {
	sess, _ := GlobalSessions.SessionStart(nil, c.Request())
	sessID := sess.SessionID()
	logs.Log.Informational("Session ID (% v)!", sessID)

	ws.Handler(func(conn *ws.Conn) {
		defer conn.Close()

		defer func() {
			if p := recover(); p != nil {
				logs.Log.Error("%v", p)
			}
		}()

		if Sc.GetConn(sessID) == nil {
			Sc.Add(sessID, conn)
		}

		defer Sc.Remove(sessID, conn)

		go func() {
			for info := range Sc.GetWchan(sessID).wchan {
				if err = ws.JSON.Send(conn, info); err != nil {
					logs.Log.Debug("websocket send error disconnected (% v)!", err)
					break
				}
				time.Sleep(10 * time.Second)
			}
		}()

		for {
			var req map[string]interface{}

			if err = ws.JSON.Receive(conn, &req); err != nil {
				logs.Log.Debug("websocket receive error disconnected (% v)!", err)
				break
			}

			// log.Log.Debug ("Received from web:% v", req)
			wsAPI[util.Atoa(req["operate"])](sessID, req)
		}
	}).ServeHTTP(c.Response(), c.Request())

	return
}

func init() {

	// initialize the run
	wsAPI["refresh"] = func(sessID string, req map[string]interface{}) {
		// Write the send channel
		Sc.Write(sessID, tplData(app.LogicApp.GetAppConf("mode").(int)), 1)
	}

	// initialize the run
	wsAPI["init"] = func(sessID string, req map[string]interface{}) {
		var mode = util.Atoi(req["mode"])
		var port = util.Atoi(req["port"])
		var master = util.Atoa(req["ip"]) // server (master node) address, no port
		currMode := app.LogicApp.GetAppConf("mode").(int)
		if currMode == status.UNSET {
			app.LogicApp.Init(mode, port, master, Lsc) // Run mode is initialized and set the log output destination
		} else {
			app.LogicApp = app.LogicApp.ReInit(mode, port, master) // switch the run mode
		}

		if mode == status.CLIENT {
			go app.LogicApp.Run()
		}

		// Write the send channel
		Sc.Write(sessID, tplData(mode))
	}

	wsAPI["run"] = func(sessID string, req map[string]interface{}) {
		if app.LogicApp.GetAppConf("mode").(int) != status.CLIENT {
			setConf(req)
		}

		if app.LogicApp.GetAppConf("mode").(int) == status.OFFLINE {
			Sc.Write(sessID, map[string]interface{}{"operate": "run"})
		}

		go func() {
			app.LogicApp.Run()
			if app.LogicApp.GetAppConf("mode").(int) == status.OFFLINE {
				Sc.Write(sessID, map[string]interface{}{"operate": "stop"})
			}
		}()
	}

	// Termination of the current task, now only support stand-alone mode
	wsAPI["stop"] = func(sessID string, req map[string]interface{}) {
		if app.LogicApp.GetAppConf("mode").(int) != status.OFFLINE {
			Sc.Write(sessID, map[string]interface{}{"operate": "stop"})
			return
		} else {
			// println ("stopping ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
			app.LogicApp.Stop()
			// println ("stopping +++++++++++++++++++++++++++++++++++++++")
			Sc.Write(sessID, map[string]interface{}{"operate": "stop"})
			// println ("stopping $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
		}
	}

	// task pause and resume, currently only supports stand-alone mode
	wsAPI["pauseRecover"] = func(sessID string, req map[string]interface{}) {
		if app.LogicApp.GetAppConf("mode").(int) != status.OFFLINE {
			return
		}
		app.LogicApp.PauseRecover()
		Sc.Write(sessID, map[string]interface{}{"operate": "pauseRecover"})
	}

	// Exit the current mode
	wsAPI["exit"] = func(sessID string, req map[string]interface{}) {
		app.LogicApp = app.LogicApp.ReInit(status.UNSET, 0, "")
		Sc.Write(sessID, map[string]interface{}{"operate": "exit"})
	}
}

func tplData(mode int) map[string]interface{} {
	var info = map[string]interface{}{"operate": "init", "mode": mode}

	// Run the model title
	switch mode {
	case status.OFFLINE:
		info["title"] = config.FULL_NAME + "                                                          [Operation mode -> stand-alone]"
	case status.SERVER:
		info["title"] = config.FULL_NAME + "                                                          [Operation mode -> service side]"
	case status.CLIENT:
		info["title"] = config.FULL_NAME + "                                                          [Operation Mode -> Client]"
	}

	if mode == status.CLIENT {
		return info
	}

	// Spider family list
	info["spiders"] = map[string]interface{}{
		"menu": spiderMenu,
		"curr": func() interface{} {
			l := app.LogicApp.GetSpiderQueue().Len()
			if l == 0 {
				return 0
			}
			var curr = make(map[string]bool, l)
			for _, sp := range app.LogicApp.GetSpiderQueue().GetAll() {
				curr[sp.GetName()] = true
			}

			return curr
		}(),
	}

	// Output list
	info["OutType"] = map[string]interface{}{
		"menu": app.LogicApp.GetOutputLib(),
		"curr": app.LogicApp.GetAppConf("OutType"),
	}

	// Concurrent connection limit
	info["ThreadNum"] = map[string]int{
		"max":  999999,
		"min":  1,
		"curr": app.LogicApp.GetAppConf("ThreadNum").(int),
	}

	// pause interval / ms (random: Pausetime / 2 ~ Pausetime * 2)
	info["Pausetime"] = map[string][]int64{
		"menu": {0, 100, 300, 500, 1000, 3000, 5000, 10000, 15000, 20000, 30000, 60000},
		"curr": []int64{app.LogicApp.GetAppConf("Pausetime").(int64)},
	}

	// The number of minutes of proxy IP replacement
	info["ProxyMinute"] = map[string][]int64{
		"menu": {0, 1, 3, 5, 10, 15, 20, 30, 45, 60, 120, 180},
		"curr": []int64{app.LogicApp.GetAppConf("ProxyMinute").(int64)},
	}

	// the capacity of the batch output
	info["DockerCap"] = map[string]int{
		"min":  1,
		"max":  5000000,
		"curr": app.LogicApp.GetAppConf("DockerCap").(int),
	}

	// Collect the upper limit
	if app.LogicApp.GetAppConf("Limit").(int64) == spider.Limit {
		info["Limit"] = 0
	} else {
		info["Limit"] = app.LogicApp.GetAppConf("Limit")
	}

	// Custom configuration
	info["KeyIns"] = app.LogicApp.GetAppConf("KeyIns")

	// inherit history
	info["SuccessInherit"] = app.LogicApp.GetAppConf("SuccessInherit")
	info["FailureInherit"] = app.LogicApp.GetAppConf("FailureInherit")

	// Operating status
	info["status"] = app.LogicApp.Status()

	return info
}

// Configure the operating parameters
func setConf(req map[string]interface{}) {
	if tn := util.Atoi(req["ThreadNum"]); tn == 0 {
		app.LogicApp.SetAppConf("ThreadNum", 1)
	} else {
		app.LogicApp.SetAppConf("ThreadNum", tn)
	}

	app.LogicApp.
		SetAppConf("Pausetime", int64(util.Atoi(req["Pausetime"]))).
		SetAppConf("ProxyMinute", int64(util.Atoi(req["ProxyMinute"]))).
		SetAppConf("OutType", util.Atoa(req["OutType"])).
		SetAppConf("DockerCap", util.Atoi(req["DockerCap"])).
		SetAppConf("Limit", int64(util.Atoi(req["Limit"]))).
		SetAppConf("KeyIns", util.Atoa(req["KeyIns"])).
		SetAppConf("SuccessInherit", req["SuccessInherit"] == "true").
		SetAppConf("FailureInherit", req["FailureInherit"] == "true")

	setSpiderQueue(req)
}

func setSpiderQueue(req map[string]interface{}) {
	spNames, ok := req["spiders"].([]interface{})
	if !ok {
		return
	}
	spiders := []*spider.Spider{}
	for _, sp := range app.LogicApp.GetSpiderLib() {
		for _, spName := range spNames {
			if util.Atoa(spName) == sp.GetName() {
				spiders = append(spiders, sp.Copy())
			}
		}
	}
	app.LogicApp.SpiderPrepare(spiders)
}
