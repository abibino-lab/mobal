package models

import (
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"time"

	"golang.org/x/crypto/scrypt"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/abibino-lab/mobal/web/api/middleware"
	"gitlab.com/abibino-lab/mobal/web/api/middleware/mymongo"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// New alloc and initialize a user.
func (p *Profile) New(name string) {
	p.FirstName = name
}

// Save alloc and initialize a user.
func (r *UserRegister) Save() (code int, err error) {
	salt, err := generateSalt()
	if err != nil {
		code = ErrSystem
	} else {
		hash, err := generatePassHash(r.Password, salt)
		if err != nil {
			code = ErrSystem
		} else {
			u := User{}
			p := Profile{}
			p.New(r.UserName)

			u.New()
			u.Email = r.Email
			u.UserName = r.UserName
			u.Profile = p
			u.Password = hash
			u.Email = r.Email
			u.Salt = salt
			code, err = u.Insert()
		}
	}
	return
}

// New is a function for ...
func (u *User) New() {
	u.ID = bson.NewObjectId()
	u.CreatedAt = u.ID.Time()
	u.UpdatedAt = u.ID.Time()
}

// Insert is a function for insert document to collection.
func (u *User) Insert() (code int, err error) {
	ds := mymongo.DataStore{}
	if err = ds.ConnectToTagserver(); err != nil {
		code = ErrDatabase
		return
	}

	defer ds.Close()
	// Collection Item
	c := ds.Session.DB("mobal").C("users")

	uniqueIndex := []string{"email", "mobilePhone", "userName"} // Index
	for ui := range uniqueIndex {
		index := mgo.Index{
			Key:        []string{uniqueIndex[ui]},
			Unique:     true,
			DropDups:   false,
			Background: true,
			Sparse:     true,
		}

		if err = c.EnsureIndex(index); err != nil {
			code = ErrDatabase
			return
		}
	}

	if err = c.Insert(&u); err != nil {
		if mgo.IsDup(err) {
			code = ErrDupRows
		}
		code = ErrDatabase
	}

	return
}

const pwHashBytes = 64

func generateSalt() (salt string, err error) {
	buf := make([]byte, pwHashBytes)
	if _, err := io.ReadFull(rand.Reader, buf); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", buf), nil
}

func generatePassHash(password string, salt string) (hash string, err error) {
	h, err := scrypt.Key([]byte(password), []byte(salt), 16384, 8, 1, pwHashBytes)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h), nil
}

// FindByID query a document according to input id.
func (u *User) FindByID(id string) (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()

	c := ds.Session.DB("mobal").C("users")
	err = c.FindId(id).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = ErrNotFound
		} else {
			code = ErrDatabase
		}
	}
	return
}

// FindByEmail query a document according to input email.
func (u *User) FindByEmail(email string) (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()

	c := ds.Session.DB("mobal").C("users")
	err = c.Find(bson.M{"email": email}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = ErrNotFound
		} else {
			code = ErrDatabase
		}
	}
	return
}

// CheckPass compare input password.
func (u *User) CheckPass(pass string) (ok bool, err error) {
	hash, err := generatePassHash(pass, u.Salt)
	if err != nil {
		return false, err
	}

	return u.Password == hash, nil
}

// ClearPass clear password information.
func (u *User) ClearPass() {
	u.Password = ""
	u.Salt = ""
}

// ChangePass update password and salt information according to input id.
func (u *User) ChangePass(id, oldPass, newPass string) (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()

	c := ds.Session.DB("").C("users")

	err = c.FindId(id).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			code = ErrNotFound
		}
		code = ErrDatabase
	} else {
		// Check hash
		oldHash, err := generatePassHash(oldPass, u.Salt)
		if err != nil {
			code = ErrSystem
		} else {
			// Create new salt
			newSalt, err := generateSalt()
			if err != nil {
				code = ErrSystem
			} else {
				// Create new hash
				newHash, err := generatePassHash(newPass, newSalt)

				if err != nil {
					code = ErrSystem
				} else {
					// Update data with new pass
					err = c.Update(bson.M{"_id": id, "password": oldHash}, bson.M{"$set": bson.M{"password": newHash, "salt": newSalt}})

					if err != nil {
						if err == mgo.ErrNotFound {
							code = ErrNotFound
						}
						code = ErrDatabase
					}
				}
			}
		}
	}
	return
}

// AuthFind query a document according to input email.
func (u *User) AuthFind(id string) (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()

	c := ds.Session.DB("mobal").C("users")
	err = c.Find(bson.M{
		"$or": []bson.M{ // you can try this in []interface
			bson.M{"email": id},
			bson.M{"userName": id},
			bson.M{"mobilePhone": id},
		},
	}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = ErrNotFound
		} else {
			code = ErrDatabase
		}
	}
	return
}

// UserClaimsIdentity is a function...
func (ua *UserAuth) UserClaimsIdentity() (t string, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()

	// Collection Item
	c := ds.Session.DB("mobal").C("users")
	// Query One
	u := User{}
	err = c.Find(bson.M{
		"$or": []bson.M{ // you can try this in []interface
			bson.M{"email": ua.AccountID},
			bson.M{"userName": ua.AccountID},
			bson.M{"mobilePhone": ua.AccountID},
		},
		"$and": []bson.M{
			bson.M{"status": 1},
		},
	}).One(&u)

	if err != nil {
		return
	}

	switch u.Status {
	case 0:
		err = errors.New("Your account must be activate")
		return
	case 2:
		err = errors.New("Your account suspend")
		return
	default:
		err = nil
	}

	// Comparing the password with the hash
	ok, err := u.CheckPass(ua.Password)
	if !ok {
		return
	}

	identity := u.ID.Hex()

	// Set custom claims
	claims := &JwtCustomClaims{
		identity,
		jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	// Generate encoded token and send it as response.
	t, err = token.SignedString(middleware.RSA.SignKey)
	return
}
