package collector

import (
	"fmt"
	"sync"

	"gitlab.com/abibino-lab/mobal/common/mysql"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/logs"
)

/************************ MySQL Output ***************************/

func init() {
	var (
		mysqlTable     = map[string]*mysql.MyTable{}
		mysqlTableLock sync.RWMutex
	)

	var getMysqlTable = func(name string) (*mysql.MyTable, bool) {
		mysqlTableLock.RLock()
		defer mysqlTableLock.RUnlock()
		tab, ok := mysqlTable[name]
		if ok {
			return tab.Clone(), true
		}
		return nil, false
	}

	var setMysqlTable = func(name string, tab *mysql.MyTable) {
		mysqlTableLock.Lock()
		mysqlTable[name] = tab
		mysqlTableLock.Unlock()
	}

	DataOutput["mysql"] = func(collector *Collector) error {
		_, err := mysql.DB()
		if err != nil {
			return fmt.Errorf("MySQL database connection failed: %v", err)
		}
		var (
			mysqls    = make(map[string]*mysql.MyTable)
			namespace = util.FileNameReplace(collector.namespace())
		)
		for _, datacell := range collector.dataDocker {
			subNamespace := util.FileNameReplace(collector.subNamespace(datacell))
			tName := joinNamespaces(namespace, subNamespace)
			table, ok := mysqls[tName]
			if !ok {
				table, ok = getMysqlTable(tName)
				if ok {
					mysqls[tName] = table
				} else {
					table = mysql.New()
					table.SetTableName(tName)
					for _, title := range collector.MustGetRule(datacell[`RuleName`].(string)).ItemFields {
						table.AddColumn(title + ` MEDIUMTEXT`)
					}
					if collector.Spider.OutDefaultField() {
						table.AddColumn(`currentLink VARCHAR(255)`, `parentLink VARCHAR(255)`, `downloadTime VARCHAR(50)`)
					}
					if err := table.Create(); err != nil {
						logs.Log.Error("%v", err)
						continue
					} else {
						setMysqlTable(tName, table)
						mysqls[tName] = table
					}
				}
			}
			data := []string{}
			for _, title := range collector.MustGetRule(datacell["ruleName"].(string)).ItemFields {
				vd := datacell["data"].(map[string]interface{})
				if v, ok := vd[title].(string); ok || vd[title] == nil {
					data = append(data, v)
				} else {
					data = append(data, util.JsonString(vd[title]))
				}
			}
			if collector.Spider.OutDefaultField() {
				data = append(data, datacell["currentLink"].(string), datacell["parentLink"].(string), datacell["downloadTime"].(string))
			}
			table.AutoInsert(data)
		}
		for _, tab := range mysqls {
			util.CheckErr(tab.FlushInsert())
		}
		mysqls = nil
		return nil
	}
}
