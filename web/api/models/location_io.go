package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type (
	// Location is a struct for ...
	Location struct {
		ID                bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
		Node              int           `bson:"node" json:"node"`
		ParentNode        int           `bson:"parentNode" json:"parentNode"`
		Sort              int           `bson:"sort" json:"sort"`
		Type              string        `bson:"type" json:"type"` // Country | State | City | - | Village
		Name              string        `bson:"name" json:"name"`
		Keyword           string        `bson:"keyword" json:"keyword"`
		MetaTitle         string        `bson:"metaTitle" json:"metaTitle"`
		MetaDescription   string        `bson:"metaDescription" json:"metaDescription"`
		InternationalName string        `bson:"internationalName" json:"internationalName"`
		Attributes        interface{}   `bson:"attributes" json:"attributes"`
		Status            int           `bson:"status" json:"status"`
		CreatedAt         time.Time     `bson:"createdAt,omitempty" json:"createdAt,omitempty"`
		UpdatedAt         time.Time     `bson:"updatedAt,omitempty" json:"updatedAt,omitempty"`
		UpdatedBy         bson.ObjectId `bson:"updatedBy,omitempty" json:"updatedBy,omitempty"`
		CreatedBy         bson.ObjectId `bson:"createdBy,omitempty" json:"createdBy,omitempty"`
	}
)
