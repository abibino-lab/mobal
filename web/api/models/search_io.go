package models

type (
	// Search is a struct for ...
	Search struct {
		Query     string            `json:"q" form:"q" query:"q"`
		Filter    map[string]string `json:"filter" form:"filter" query:"filter"`          //attributes[...][...],
		Sort      string            `json:"sort" form:"sort" query:"sort"`                //sort[...][...],
		Fields    string            `json:"fields" form:"fields" query:"fields"`          //sort[...][...],
		Paging    map[string]int    `json:"paging" form:"paging" query:"paging"`          //offset=10&limit=5
		Algorithm map[string]string `json:"algorithm" form:"algorithm" query:"algorithm"` // tsukamoto
	}
)
