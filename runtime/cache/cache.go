package cache

import (
	"runtime"
	"sync/atomic"
	"time"
)

// ************************************** Task Runtime Common Configuration ***** *********************************** \\

// AppConf is a task runtime for public configuration
type AppConf struct {
	Mode           int    // node role
	Port           int    // The master node port
	Master         string // server (master node) address, no port
	ThreadNum      int    // global maximum concurrency
	Pausetime      int64  // pause duration reference / ms (random: Pausetime / 2 ~ Pausetime * 2)
	OutType        string // output method
	DockerCap      int    // Subdivide container capacity
	Limit          int64  // set the upper limit, 0 is not limited, if the rule is set to the initial value of Limit is a custom limit, otherwise the default limit request number
	ProxyMinute    int64  // The number of minutes of proxy IP replacement
	SuccessInherit bool   // inherit history success record
	FailureInherit bool   // inherit history failure record
	// Optional
	KeyIns string // Custom input, late cut into multiple tasks KeyIn custom configuration

}

// Task is an initial value for the default value
var Task = new(AppConf)

// **************************************** Mission Report ************************************* \\

type Report struct {
	SpiderName string
	KeyIn      string
	DataNum    uint64
	FileNum    uint64
	// DataSize uint64
	// FileSize uint64
	Time time.Duration
}

var (
	// Click the start button at the point in time
	StartTime time.Time
	// text data summary report
	ReportChan chan *Report
	// Total number of request pages [] uint {total number of failures}
	pageSum [2]uint64
)

// reset the page count
func ResetPageCount() {
	pageSum = [2]uint64{}
}

// 0 returns the total number of downloads, negative returns the number of failures, positive returns the number of successes
func GetPageCount(i int) uint64 {
	switch {
	case i > 0:
		// return success number
		return pageSum[0]
	case i < 0:
		// returns the number of failures
		return pageSum[1]
	case i == 0:
	}
	// return the total number
	return pageSum[0] + pageSum[1]
}

func PageSuccCount() {
	atomic.AddUint64(&pageSum[0], 1)
}

func PageFailCount() {
	atomic.AddUint64(&pageSum[1], 1)
}

// **************************************** init function execution order control *** **************************************** \\

var initOrder = make(map[int]bool)

// mark the current init () has finished executing
func ExecInit(order int) {
	initOrder[order] = true
}

// wait for specified init () to finish
// call in concurrent concurrency
func WaitInit(order int) {
	for !initOrder[order] {
		runtime.Gosched()
	}
}

//****************************************initialization******* ************************************ \\

func init() {
	// task report
	ReportChan = make(chan *Report)
}
