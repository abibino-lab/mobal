package proxy

import (
	"sync"
	"time"
)

// UsableProxy is a struct for handle proxy
type UsableProxy struct {
	curIndex  int // Current index proxy IP
	proxys    []string
	timeDelay []time.Duration
	isEcho    bool // Whether to print for ip information
	sync.Mutex
}

// Len is a function for implement the sorting interface
func (usable *UsableProxy) Len() int {
	return len(usable.proxys)
}

func (usable *UsableProxy) Less(i, j int) bool {
	return usable.timeDelay[i] < usable.timeDelay[j]
}

func (usable *UsableProxy) Swap(i, j int) {
	usable.proxys[i], usable.proxys[j] = usable.proxys[j], usable.proxys[i]
	usable.timeDelay[i], usable.timeDelay[j] = usable.timeDelay[j], usable.timeDelay[i]
}
