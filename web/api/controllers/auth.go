package controllers

import (
	"net/http"

	"gitlab.com/abibino-lab/mobal/web/api/models"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

// Accessible is a ...
func Accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessibleaaaa")
}

// Restricted is a ...
func Restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*models.JwtCustomClaims)

	return c.String(http.StatusOK, "Welcome "+claims.Identity+"!")
}

// Auth is a ...
func Auth(c echo.Context) (err error) {
	ua := new(models.UserAuth)
	if err = c.Bind(ua); err != nil {
		return err
	}

	t, err := ua.UserClaimsIdentity()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"access_token": t,
	})
}
