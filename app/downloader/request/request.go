package request

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"gitlab.com/abibino-lab/mobal/common/util"
)

// Request : called recommended object waiting for being crawled
type Request struct {
	Spider        string          // rule name, set automatically, prohibit people to fill out
	URL           string          // target URL must be set
	Rule          string          // The name of the rule node used to resolve the response must be set
	Method        string          // GET POST POST-M HEAD
	Header        http.Header     // request header information
	EnableCookie  bool            // whether to use cookies in the Spider's EnableCookie settings
	PostData      string          // POST values
	DialTimeout   time.Duration   // create a connection timeout dial tcp: i / o timeout
	ConnTimeout   time.Duration   // connection status timeout WSARecv tcp: i / o timeout
	TryTimes      int             // the maximum number of attempts to download
	RetryPause    time.Duration   // download failed after the next attempt to download the waiting time
	RedirectTimes int             // The maximum number of redirects, 0 is not limited, less than 0 to prohibit the redirection
	Temp          Temp            // Temporary data
	TempIsJSON    map[string]bool // Temp to JSON stored field marked as true, automatically set to prohibit people to fill out
	Priority      int             // Specify the scheduling priority, the default is 0 (the minimum priority is 0)
	Reloadable    bool            // is it necessary to repeat the link download
	// Surfer Downloader kernel ID
	// 0 for Surf high concurrent downloader, a variety of control functions
	// 1 for the PhantomJS downloader, features strong defense, slow, low concurrent
	DownloaderID int

	proxy  string // Automatically set the proxy when the user interface is set to use proxy IP
	unique string // ID
	lock   sync.RWMutex
}

const (
	// DefaultDialTimeout :
	DefaultDialTimeout = 2 * time.Minute // The default request server timed out
	// DefaultConnTimeout :
	DefaultConnTimeout = 2 * time.Minute // default download timeout
	// DefaultTryTimes :
	DefaultTryTimes = 3 // default maximum number of downloads
	// DefaultRetryPause :
	DefaultRetryPause = 2 * time.Second // default to re-download before pause
)

const (
	// SurfID :
	SurfID = 0 // default surf download kernel (Go native), this value can not be changed
	// PhantomJSID :
	PhantomJSID = 1 // spare phantomjs download kernel, generally not used (poor efficiency, head information support is not perfect)
)

// Prepare : Send the request before the job, set a series of default values
// Request.URL and Request.Rule must be set
// Request.Spider does not need to be set manually (set by the system automatically)
// Request.EnableCookie is set in the Spider field and invalidated in the rule request
// The following fields have default values, not set:
// Request.Method defaults to the GET method;
// Request.DialTimeout defaults to the constant DefaultDialTimeout, less than 0 does not limit the waiting time;
// Request.ConnTimeout defaults to the constant DefaultConnTimeout, less than 0 does not limit the download timeout;
// Request.TryTimes defaults to the constant DefaultTryTimes, less than 0 does not limit the number of failed overloads;
// Request.RedirectTimes by default does not limit the number of redirects, less than 0 to prohibit redirects;
// Request.RetryPause defaults to the constant DefaultRetryPause;
// Request.DownloaderID specified downloader ID, 0 for the default Surf high concurrent downloader, full functionality, 1 for the PhantomJS downloader, features strong break, slow, low concurrent.
func (request *Request) Prepare() error {
	// Make sure url is correct and equal to URL string in Response
	URL, err := url.Parse(request.URL)
	if err != nil {
		return err
	}

	request.URL = URL.String()

	if request.Method == "" {
		request.Method = "GET"
	} else {
		request.Method = strings.ToUpper(request.Method)
	}

	if request.Header == nil {
		request.Header = make(http.Header)
	}

	if request.DialTimeout < 0 {
		request.DialTimeout = 0
	} else if request.DialTimeout == 0 {
		request.DialTimeout = DefaultDialTimeout
	}

	if request.ConnTimeout < 0 {
		request.ConnTimeout = 0
	} else if request.ConnTimeout == 0 {
		request.ConnTimeout = DefaultConnTimeout
	}

	if request.TryTimes == 0 {
		request.TryTimes = DefaultTryTimes
	}

	if request.RetryPause <= 0 {
		request.RetryPause = DefaultRetryPause
	}

	if request.Priority < 0 {
		request.Priority = 0
	}

	if request.DownloaderID < SurfID || request.DownloaderID > PhantomJSID {
		request.DownloaderID = SurfID
	}

	if request.TempIsJSON == nil {
		request.TempIsJSON = make(map[string]bool)
	}

	if request.Temp == nil {
		request.Temp = make(Temp)
	}
	return nil
}

// Deserialize :
func Deserialize(s string) (*Request, error) {
	req := new(Request)
	return req, json.Unmarshal([]byte(s), req)
}

// Serialize :
func (request *Request) Serialize() string {
	for k, v := range request.Temp {
		request.Temp.set(k, v)
		request.TempIsJSON[k] = true
	}
	b, _ := json.Marshal(request)
	return strings.Replace(util.Bytes2String(b), `\u0026`, `&`, -1)
}

// Unique : Request unique identifier
func (request *Request) Unique() string {
	if request.unique == "" {
		block := md5.Sum([]byte(request.Spider + request.Rule + request.URL + request.Method))
		request.unique = hex.EncodeToString(block[:])
	}
	return request.unique
}

// Copy : Get a copy
func (request *Request) Copy() *Request {
	reqcopy := new(Request)
	b, _ := json.Marshal(request)
	json.Unmarshal(b, reqcopy)
	return reqcopy
}

// GetURL :
func (request *Request) GetURL() string {
	return request.URL
}

// GetMethod : get the name of the Http request (note that this does not refer to the Http GET method)
func (request *Request) GetMethod() string {
	return request.Method
}

// SetMethod : set the type of Http request method
func (request *Request) SetMethod(method string) *Request {
	request.Method = strings.ToUpper(method)
	return request
}

// SetURL :
func (request *Request) SetURL(url string) *Request {
	request.URL = url
	return request
}

// GetReferer :
func (request *Request) GetReferer() string {
	return request.Header.Get("Referer")
}

// SetReferer :
func (request *Request) SetReferer(referer string) *Request {
	request.Header.Set("Referer", referer)
	return request
}

// GetPostData :
func (request *Request) GetPostData() string {
	return request.PostData
}

// GetHeader :
func (request *Request) GetHeader() http.Header {
	return request.Header
}

// SetHeader :
func (request *Request) SetHeader(key, value string) *Request {
	request.Header.Set(key, value)
	return request
}

// AddHeader :
func (request *Request) AddHeader(key, value string) *Request {
	request.Header.Add(key, value)
	return request
}

// GetEnableCookie :
func (request *Request) GetEnableCookie() bool {
	return request.EnableCookie
}

// SetEnableCookie :
func (request *Request) SetEnableCookie(enableCookie bool) *Request {
	request.EnableCookie = enableCookie
	return request
}

// GetCookies :
func (request *Request) GetCookies() string {
	return request.Header.Get("Cookie")
}

// SetCookies :
func (request *Request) SetCookies(cookie string) *Request {
	request.Header.Set("Cookie", cookie)
	return request
}

// GetDialTimeout :
func (request *Request) GetDialTimeout() time.Duration {
	return request.DialTimeout
}

// GetConnTimeout :
func (request *Request) GetConnTimeout() time.Duration {
	return request.ConnTimeout
}

// GetTryTimes :
func (request *Request) GetTryTimes() int {
	return request.TryTimes
}

// GetRetryPause :
func (request *Request) GetRetryPause() time.Duration {
	return request.RetryPause
}

// GetProxy :
func (request *Request) GetProxy() string {
	return request.proxy
}

func (request *Request) SetProxy(proxy string) *Request {
	// SetProxy :
	request.proxy = proxy
	return request
}

// GetRedirectTimes :
func (request *Request) GetRedirectTimes() int {
	return request.RedirectTimes
}

// GetRuleName :
func (request *Request) GetRuleName() string {
	return request.Rule
}

// SetRuleName :
func (request *Request) SetRuleName(ruleName string) *Request {
	request.Rule = ruleName
	return request
}

// GetSpiderName :
func (request *Request) GetSpiderName() string {
	return request.Spider
}

// SetSpiderName :
func (request *Request) SetSpiderName(spiderName string) *Request {
	request.Spider = spiderName
	return request
}

// IsReloadable :
func (request *Request) IsReloadable() bool {
	return request.Reloadable
}

// SetReloadable :
func (request *Request) SetReloadable(can bool) *Request {
	request.Reloadable = can
	return request
}

// Get temporary cache data
// defaultValue can not be interface {} (nil)
// GetTemp :
func (request *Request) GetTemp(key string, defaultValue interface{}) interface{} {
	if defaultValue == nil {
		panic("*Request.GetTemp()的defaultValue不能为nil，错误位置:key=" + key)
	}
	request.lock.RLock()
	defer request.lock.RUnlock()

	if request.Temp[key] == nil {
		return defaultValue
	}

	if request.TempIsJSON[key] {
		return request.Temp.get(key, defaultValue)
	}

	return request.Temp[key]
}

// GetTemps :
func (request *Request) GetTemps() Temp {
	return request.Temp
}

// SetTemp :
func (request *Request) SetTemp(key string, value interface{}) *Request {
	request.lock.Lock()
	request.Temp[key] = value
	delete(request.TempIsJSON, key)
	request.lock.Unlock()
	return request
}

// SetTemps :
func (request *Request) SetTemps(temp map[string]interface{}) *Request {
	request.lock.Lock()
	request.Temp = temp
	request.TempIsJSON = make(map[string]bool)
	request.lock.Unlock()
	return request
}

// GetPriority :
// GetPriority :
func (request *Request) GetPriority() int {
	return request.Priority
}

// SetPriority :
// SetPriority :
func (request *Request) SetPriority(priority int) *Request {
	request.Priority = priority
	return request
}

// GetDownloaderID :
// GetDownloaderID :
func (request *Request) GetDownloaderID() int {
	return request.DownloaderID
}

// SetDownloaderID :
// SetDownloaderID :
func (request *Request) SetDownloaderID(id int) *Request {
	request.DownloaderID = id
	return request
}

// MarshalJSON :
// MarshalJSON :
func (request *Request) MarshalJSON() ([]byte, error) {
	for k, v := range request.Temp {
		if request.TempIsJSON[k] {
			continue
		}
		request.Temp.set(k, v)
		request.TempIsJSON[k] = true
	}

	b, err := json.Marshal(*request)

	return b, err
}
