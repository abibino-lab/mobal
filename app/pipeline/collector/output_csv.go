package collector

import (
	"encoding/csv"
	"fmt"
	"os"

	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

/************************ CSV Output ***************************/
func init() {
	DataOutput["csv"] = func(collector *Collector) (err error) {
		defer func() {
			if p := recover(); p != nil {
				err = fmt.Errorf("%v", p)
			}
		}()
		var (
			namespace = util.FileNameReplace(collector.namespace())
			sheets    = make(map[string]*csv.Writer)
		)
		for _, datacell := range collector.dataDocker {
			var subNamespace = util.FileNameReplace(collector.subNamespace(datacell))
			if _, ok := sheets[subNamespace]; !ok {
				folder := config.TEXT_DIR + "/" + cache.StartTime.Format("2006-01-02 150405") + "/" + joinNamespaces(namespace, subNamespace)
				filename := fmt.Sprintf("%v/%v-%v.csv", folder, collector.sum[0], collector.sum[1])

				// create / open the directory
				f, err := os.Stat(folder)
				if err != nil || !f.IsDir() {
					if err := os.MkdirAll(folder, 0777); err != nil {
						logs.Log.Error("Error: %v", err)
					}
				}

				// Create a file by data category
				file, err := os.Create(filename)

				if err != nil {
					logs.Log.Error("%v", err)
					continue
				}

				file.WriteString("\xEF\xBB\xBF") // write UTF-8 BOM

				sheets[subNamespace] = csv.NewWriter(file)
				th := collector.MustGetRule(datacell["ruleName"].(string)).ItemFields
				if collector.Spider.OutDefaultField() {
					th = append(th, "currentLink", "parentLink", "downloadTime")
				}
				sheets[subNamespace].Write(th)

				defer func(file *os.File) {
					// send the cached data stream
					sheets[subNamespace].Flush()
					// Close the file
					file.Close()
				}(file)
			}

			row := []string{}
			for _, title := range collector.MustGetRule(datacell["ruleName"].(string)).ItemFields {
				vd := datacell["data"].(map[string]interface{})
				if v, ok := vd[title].(string); ok || vd[title] == nil {
					row = append(row, v)
				} else {
					row = append(row, util.JsonString(vd[title]))
				}
			}
			if collector.Spider.OutDefaultField() {
				row = append(row, datacell["currentLink"].(string))
				row = append(row, datacell["parentLink"].(string))
				row = append(row, datacell["downloadTime"].(string))
			}
			sheets[subNamespace].Write(row)
		}
		return
	}
}
