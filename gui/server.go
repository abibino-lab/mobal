package gui

import (
	"strconv"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

var serverCount int

func serverWindow() {
	mw.Close()

	if err := (MainWindow{
		AssignTo: &mw,
		DataBinder: DataBinder{
			AssignTo:       &db,
			DataSource:     Input,
			ErrorPresenter: ErrorPresenterRef{&ep},
		},
		Title:   config.FULL_NAME + "                                                          [Operation mode -> server]",
		MinSize: Size{1100, 700},
		Layout:  VBox{MarginsZero: true},
		Children: []Widget{

			Composite{
				AssignTo: &setting,
				Layout:   Grid{Columns: 2},
				Children: []Widget{
					// Task list
					TableView{
						ColumnSpan:            1,
						MinSize:               Size{550, 450},
						AlternatingRowBGColor: walk.RGB(255, 255, 224),
						CheckBoxes:            true,
						ColumnsOrderable:      true,
						Columns: []TableViewColumn{
							{Title: "#", Width: 45},
							{Title: "Task", Width: 110 /*, Format: "%.2f", Alignment: AlignFar*/},
							{Title: "Description", Width: 370},
						},
						Model: spiderMenu,
					},

					VSplitter{
						ColumnSpan: 1,
						MinSize:    Size{550, 450},
						Children: []Widget{

							VSplitter{
								Children: []Widget{
									Label{
										Text: "Custom configuration (multi-tasking, please pack a layer of \"<>\")",
									},
									LineEdit{
										Text: Bind("KeyIns"),
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Collection limit (default limit URL):",
									},
									NumberEdit{
										Value:    Bind("Limit"),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Concurrent coroutines:(1~99999)",
									},
									NumberEdit{
										Value:    Bind("ThreadNum", Range{1, 99999}),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Batch output size: (1~5,000,000 Item)",
									},
									NumberEdit{
										Value:    Bind("DockerCap", Range{1, 5000000}),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Pause time reference:",
									},
									ComboBox{
										Value:         Bind("Pausetime", SelRequired{}),
										DisplayMember: "Key",
										BindingMember: "Int64",
										Model:         GuiOpt.Pausetime,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Proxy IP replacement frequency:",
									},
									ComboBox{
										Value:         Bind("ProxyMinute", SelRequired{}),
										DisplayMember: "Key",
										BindingMember: "Int64",
										Model:         GuiOpt.ProxyMinute,
									},
								},
							},

							RadioButtonGroupBox{
								ColumnSpan: 1,
								Title:      "*Output method",
								Layout:     HBox{},
								DataMember: "OutType",
								Buttons:    outputList,
							},
						},
					},
				},
			},

			Composite{
				Layout: HBox{},
				Children: []Widget{

					// Required error check
					LineErrorPresenter{
						AssignTo: &ep,
					},

					HSplitter{
						MaxSize: Size{220, 50},
						Children: []Widget{
							Label{
								Text: "Inherit and save success records",
							},
							CheckBox{
								Checked: Bind("SuccessInherit"),
							},
						},
					},

					HSplitter{
						MaxSize: Size{220, 50},
						Children: []Widget{
							Label{
								Text: "Inherit and save the failed record",
							},
							CheckBox{
								Checked: Bind("FailureInherit"),
							},
						},
					},

					PushButton{
						MinSize:   Size{90, 0},
						Text:      serverBtnTxt(),
						AssignTo:  &runStopBtn,
						OnClicked: serverStart,
					},
				},
			},
		},
	}.Create()); err != nil {
		panic(err)
	}

	setWindow()

	// Initialize the application
	Init()

	// Run the form program
	mw.Run()
}

// Click the start event
func serverStart() {
	if err := db.Submit(); err != nil {
		logs.Log.Error("%v", err)
		return
	}

	// Read the task
	Input.Spiders = spiderMenu.GetChecked()

	if len(Input.Spiders) == 0 {
		logs.Log.Warning("- Task list can not be empty")
		return
	}

	// Record configuration information
	SetTaskConf()

	runStopBtn.SetEnabled(false)
	runStopBtn.SetText("Distribute the task (···)")

	// Reset the spiders queue
	SpiderPrepare()

	// Generate distribution tasks
	app.LogicApp.Run()

	serverCount++

	runStopBtn.SetText(serverBtnTxt())
	runStopBtn.SetEnabled(true)
}

// Update button text
func serverBtnTxt() string {
	return "Distribute the task (" + strconv.Itoa(serverCount) + ")"
}
