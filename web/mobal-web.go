// Package web ==> [spider frame (golang)] Mobal (Phantom Spider) is a pure Go language prepared by the high-level, distributed, heavyweight reptile software, support stand-alone, server, client three operating modes, with Web, GUI (Mysql / mongodb / csv / excel, etc.), a large number of Demo sharing; at the same time she also supports horizontal and vertical two crawling mode, support for simulated login (mysql / mongodb / csv / excel, etc.), a large number of Demo shared; And task suspension, cancellation and a series of advanced features;
// (official QQ group: Go large data 42731170, welcome to join our discussion).
// Web interface version.
package web

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"

	"github.com/facebookgo/grace/gracehttp"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"gitlab.com/abibino-lab/mobal/web/api/middleware"
	// fm "gitlab.com/abibino-lab/mobal/web/front/middleware"
	"gitlab.com/abibino-lab/mobal/web/routes"
	validator "gopkg.in/go-playground/validator.v9"
)

var (
	ip   *string
	port *int
	addr string
)

// Flag is a function for get external parameters
func Flag() {
	flag.String("b ******************************************** Only for web ******************************************** -b", "", "")
	// web server IP and port number
	ip = flag.String("b_ip", "0.0.0.0", "   <Web Server IP>")
	port = flag.Int("b_port", 9090, "   <Web Server Port>")
}

// Run is a funtion for execute the entry
func Run() {

	/*============
	APP INITIALIZE
	============*/
	// appInit()

	/*===============
	PRE-BINDING ROUTE
	===============*/
	e := echo.New()

	/*===========
	DEBUG SETTING
	===========*/
	e.Debug = true
	e.Pre(mw.RemoveTrailingSlash())

	/*============
	SET MIDDLEWARE
	============*/
	/*===========
	SET TEMPLATES
	===========*/
	// t := &fm.Template{}
	// t.Init()

	// e.Renderer = t
	e.Validator = &middleware.CustomValidator{Validator: validator.New()}
	e.Use(mw.Logger())                     // Logger middleware
	e.Use(mw.Recover())                    // Recover middleware
	e.Use(mw.CORSWithConfig(mw.CORSConfig{ // CORS
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE, echo.OPTIONS},
		AllowHeaders: []string{"Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
	}))

	e.Use(mw.CSRFWithConfig(mw.CSRFConfig{
		TokenLookup: "header:X-XSRF-TOKEN",
	}))

	e.Use(mw.Secure())                     //Secure middleware
	e.Use(mw.GzipWithConfig(mw.GzipConfig{ //Gzip middleware
		Level: 5,
	}))

	/*================
	END SET MIDDLEWARE
	================*/

	/*===============
	ROUTES INITIALIZE
	===============*/
	routes.Init(e)

	/*=================
	SPECIALS CONDITIONS
	=================*/
	var cmd *exec.Cmd // Automatically open the web browser
	switch runtime.GOOS {
	case "windows":
		cmd = exec.Command("cmd", "/c", "start", "http://localhost:"+strconv.Itoa(*port))
	case "darwin":
		cmd = exec.Command("open", "http://localhost:"+strconv.Itoa(*port))
	}

	if cmd != nil {
		go func() {
			log.Printf("[P][mobal] Open the default browser after two seconds...")
			time.Sleep(time.Second * 2)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Run()
		}()
	}

	/*================
	SET SERVER ADDRESS
	================*/
	addr = *ip + ":" + strconv.Itoa(*port)
	e.Server.Addr = addr

	log.Printf("[P][mobal] Server running on %v", e.Server.Addr)
	/*===========
	START SERVICE
	===========*/
	e.Logger.Fatal(gracehttp.Serve(e.Server))
}
