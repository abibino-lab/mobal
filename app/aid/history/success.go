package history

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/abibino-lab/mobal/common/mgo"
	"gitlab.com/abibino-lab/mobal/common/mysql"
	"gitlab.com/abibino-lab/mobal/config"
)

// Success is a struct success history
type Success struct {
	tabName     string
	fileName    string
	new         map[string]bool // [Request.Unique()]true
	old         map[string]bool // [Request.Unique()]true
	inheritable bool
	sync.RWMutex
}

// UpsertSuccess is a function for update or join a successful record,
// Contrast is already there, there is no record,
// The return value indicates whether there is an insert operation.
func (success *Success) UpsertSuccess(reqUnique string) bool {
	success.RWMutex.Lock()
	defer success.RWMutex.Unlock()

	if success.old[reqUnique] {
		return false
	}
	if success.new[reqUnique] {
		return false
	}
	success.new[reqUnique] = true
	return true
}

// HasSuccess is a function for check a successful record,
func (success *Success) HasSuccess(reqUnique string) bool {
	success.RWMutex.Lock()
	has := success.old[reqUnique] || success.new[reqUnique]
	success.RWMutex.Unlock()
	return has
}

// DeleteSuccess is a function for delete the success record
func (success *Success) DeleteSuccess(reqUnique string) {
	success.RWMutex.Lock()
	delete(success.new, reqUnique)
	success.RWMutex.Unlock()
}

// flush is a function for flush process
func (success *Success) flush(provider string) (sLen int, err error) {
	success.RWMutex.Lock()
	defer success.RWMutex.Unlock()

	sLen = len(success.new)
	if sLen == 0 {
		return
	}

	switch provider {
	case "mgo":
		if mgo.Error() != nil {
			err = fmt.Errorf("Fail  [Add a successful record][mgo]: %v Item [ERROR]  %v\n", sLen, mgo.Error())
			return
		}
		var docs = make([]map[string]interface{}, sLen)
		var i int
		for key := range success.new {
			docs[i] = map[string]interface{}{"_id": key}
			success.old[key] = true
			i++
		}
		err := mgo.Mgo(nil, "insert", map[string]interface{}{
			"Database":   config.DB_NAME,
			"Collection": success.tabName,
			"Docs":       docs,
		})
		if err != nil {
			err = fmt.Errorf("Fail  [Add a successful record][mgo]: %v Item [ERROR]  %v\n", sLen, err)
		}

	case "mysql":
		_, err := mysql.DB()
		if err != nil {
			return sLen, fmt.Errorf("Fail  [Add a successful record][mysql]: %v Item [ERROR]  %v\n", sLen, err)
		}
		table, ok := getWriteMysqlTable(success.tabName)
		if !ok {
			table = mysql.New()
			table.SetTableName(success.tabName).CustomPrimaryKey("id VARCHAR(255) NOT NULL PRIMARY KEY")
			err = table.Create()
			if err != nil {
				return sLen, fmt.Errorf("Fail  [Add a successful record][mysql]: %v Item [ERROR]  %v\n", sLen, err)
			}
			setWriteMysqlTable(success.tabName, table)
		}
		for key := range success.new {
			table.AutoInsert([]string{key})
			success.old[key] = true
		}
		err = table.FlushInsert()
		if err != nil {
			return sLen, fmt.Errorf("Fail  [Add a successful record][mysql]: %v Item [ERROR]  %v\n", sLen, err)
		}

	default:
		f, _ := os.OpenFile(success.fileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)

		b, _ := json.Marshal(success.new)
		b[0] = ','
		f.Write(b[:len(b)-1])
		f.Close()

		for key := range success.new {
			success.old[key] = true
		}
	}
	success.new = make(map[string]bool)
	return
}
