package distribute

// Distributer : Distributed interface
type Distributer interface {
	// The master node sends a task from the repository
	Send(clientNum int) Task
	// Receive a task from the node to the repository
	Receive(task *Task)
	// Returns the number of nodes connected to it
	CountNodes() int
}
