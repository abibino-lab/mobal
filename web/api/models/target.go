package models

import (
	"gitlab.com/abibino-lab/mobal/web/api/middleware/mymongo"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// New is a function for ...
func (t *Target) New() {
	t.ID = bson.NewObjectId()
	t.CreatedAt = t.ID.Time()
	t.UpdatedAt = t.ID.Time()
}

// Insert is a function for insert document to collection.
func (t *Target) Insert() (code int, err error) {
	ds := mymongo.DataStore{}
	ds.ConnectToTagserver()
	defer ds.Close()
	// Collection Item
	c := ds.Session.DB("mobal").C("targets")

	// Index
	index := mgo.Index{
		Key:        []string{"domain"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	err = c.EnsureIndex(index)

	if err != nil {
		code = ErrDatabase
	} else {
		err = c.Insert(t)

		if err != nil {
			if mgo.IsDup(err) {
				code = ErrDupRows
			}
			code = ErrDatabase
		}
	}

	return

}
