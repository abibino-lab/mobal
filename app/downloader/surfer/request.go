// Copyright 2015 abibino-lab Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package surfer

import (
	"net/http"
	"strings"
	"sync"
	"time"
)

type (
	Request interface {
		// url
		GetURL() string
		// GET POST POST-M HEAD
		GetMethod() string
		// POST values
		GetPostData() string
		// http header
		GetHeader() http.Header
		// enable http cookies
		GetEnableCookie() bool
		// dial tcp: i/o timeout
		GetDialTimeout() time.Duration
		// WSARecv tcp: i/o timeout
		GetConnTimeout() time.Duration
		// the max times of download
		GetTryTimes() int
		// the pause time of retry
		GetRetryPause() time.Duration
		// the download ProxyHost
		GetProxy() string
		// max redirect times
		GetRedirectTimes() int
		// select Surf ro PhomtomJS
		GetDownloaderID() int
	}

	// The default implementation of the Request
	DefaultRequest struct {
		// url (required)
		URL string
		// GET POST POST-M HEAD (The default is GET)
		Method string
		// http header
		Header http.Header
		// Whether to use cookies, set in Spider's EnableCookie
		EnableCookie bool
		// POST values
		PostData string
		// dial tcp: i/o timeout
		DialTimeout time.Duration
		// WSARecv tcp: i/o timeout
		ConnTimeout time.Duration
		// the max times of download
		TryTimes int
		// how long pause when retry
		RetryPause time.Duration
		// max redirect times
		// when RedirectTimes equal 0, redirect times is ∞
		// when RedirectTimes less than 0, redirect times is 0
		RedirectTimes int
		// the download ProxyHost
		Proxy string

		// Tentukan Downloader ID
		// 0 Surf Download concurrency tinggi, berbagai fungsi kontrol penuh
		// 1 PhantomJS downloader, fitur yang kuat anti-pecah, lambat, concurrency rendah
		DownloaderID int

		// Make sure only call once
		once sync.Once
	}
)

const (
	SurfID             = 0               // Surf Downloader Identifier
	PhomtomJsID        = 1               // PhomtomJs downloader identifier
	SplashID           = 2               // Splash downloader identifier
	DefaultMethod      = "GET"           // Default request method
	DefaultDialTimeout = 2 * time.Minute // The default request server timed out
	DefaultConnTimeout = 2 * time.Minute // Default download timeout
	DefaultTryTimes    = 3               // Default maximum number of downloads
	DefaultRetryPause  = 2 * time.Second // Default to re-download before pause
)

func (defaultRequest *DefaultRequest) prepare() {
	if defaultRequest.Method == "" {
		defaultRequest.Method = DefaultMethod
	}
	defaultRequest.Method = strings.ToUpper(defaultRequest.Method)

	if defaultRequest.Header == nil {
		defaultRequest.Header = make(http.Header)
	}

	if defaultRequest.DialTimeout < 0 {
		defaultRequest.DialTimeout = 0
	} else if defaultRequest.DialTimeout == 0 {
		defaultRequest.DialTimeout = DefaultDialTimeout
	}

	if defaultRequest.ConnTimeout < 0 {
		defaultRequest.ConnTimeout = 0
	} else if defaultRequest.ConnTimeout == 0 {
		defaultRequest.ConnTimeout = DefaultConnTimeout
	}

	if defaultRequest.TryTimes == 0 {
		defaultRequest.TryTimes = DefaultTryTimes
	}

	if defaultRequest.RetryPause <= 0 {
		defaultRequest.RetryPause = DefaultRetryPause
	}

	if defaultRequest.DownloaderID != PhomtomJsID {
		defaultRequest.DownloaderID = SurfID
	}
}

// GetURL is a func ...
func (defaultRequest *DefaultRequest) GetURL() string {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.URL
}

// GetMethod GET POST POST-M HEAD
func (defaultRequest *DefaultRequest) GetMethod() string {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.Method
}

// GetPostData POST values
func (defaultRequest *DefaultRequest) GetPostData() string {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.PostData
}

// GetHeader http header
func (defaultRequest *DefaultRequest) GetHeader() http.Header {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.Header
}

// GetEnableCookie enable http cookies
func (defaultRequest *DefaultRequest) GetEnableCookie() bool {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.EnableCookie
}

// GetDialTimeout dial tcp: i/o timeout
func (defaultRequest *DefaultRequest) GetDialTimeout() time.Duration {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.DialTimeout
}

// GetConnTimeout WSARecv tcp: i/o timeout
func (defaultRequest *DefaultRequest) GetConnTimeout() time.Duration {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.ConnTimeout
}

// GetTryTimes is the max times of download
func (defaultRequest *DefaultRequest) GetTryTimes() int {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.TryTimes
}

// GetRetryPause is the pause time of retry
func (defaultRequest *DefaultRequest) GetRetryPause() time.Duration {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.RetryPause
}

// GetProxy is the download ProxyHost
func (defaultRequest *DefaultRequest) GetProxy() string {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.Proxy
}

// max redirect times
func (defaultRequest *DefaultRequest) GetRedirectTimes() int {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.RedirectTimes
}

// select Surf ro PhomtomJS
func (defaultRequest *DefaultRequest) GetDownloaderID() int {
	defaultRequest.once.Do(defaultRequest.prepare)
	return defaultRequest.DownloaderID
}
