// [spider frame (golang)] Mobal (Phantom Spider) is a pure Go language prepared by the high-level, distributed, heavyweight reptile software, support stand-alone, server, client three operating modes, with Web, GUI (Mysql / mongodb / csv / excel, etc.), a large number of Demo sharing; at the same time she also supports horizontal and vertical two crawling mode, support for simulated login (mysql / mongodb / csv / excel, etc.), a large number of Demo shared; And task suspension, cancellation and a series of advanced features;
// (official QQ group: Go large data 42731170, welcome to join our discussion).
// GUI interface version.
package gui

import (
	"log"

	"github.com/lxn/walk"
	"github.com/lxn/walk/declarative"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/app/spider"
	. "gitlab.com/abibino-lab/mobal/gui/model"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

// execute the entry
func Run() {
	app.LogicApp.SetAppConf("Mode", status.OFFLINE)

	outputList = func() (o []declarative.RadioButton) {
		// set the default selection
		Input.AppConf.OutType = app.LogicApp.GetOutputLib()[0]
		// Get the output options
		for _, out := range app.LogicApp.GetOutputLib() {
			o = append(o, declarative.RadioButton{Text: out, Value: out})
		}
		return
	}()

	spiderMenu = NewSpiderMenu(spider.Species)

	runmodeWindow()
}

func Init() {
	app.LogicApp.Init(Input.Mode, Input.Port, Input.Master)
}

func SetTaskConf() {
	// correct the number of joins
	if Input.ThreadNum == 0 {
		Input.ThreadNum = 1
	}
	app.LogicApp.SetAppConf("ThreadNum", Input.ThreadNum).
		SetAppConf("Pausetime", Input.Pausetime).
		SetAppConf("ProxyMinute", Input.ProxyMinute).
		SetAppConf("OutType", Input.OutType).
		SetAppConf("DockerCap", Input.DockerCap).
		SetAppConf("Limit", Input.Limit).
		SetAppConf("KeyIns", Input.KeyIns)
}

func SpiderPrepare() {
	sps := []*spider.Spider{}
	for _, sp := range Input.Spiders {
		sps = append(sps, sp.Spider)
	}
	app.LogicApp.SpiderPrepare(sps)
}

func SpiderNames() (names []string) {
	for _, sp := range Input.Spiders {
		names = append(names, sp.Spider.GetName())
	}
	return
}

func setWindow() {
	// Bind the log output interface
	lv, err := NewLogView(mw)
	if err != nil {
		panic(err)
	}
	app.LogicApp.SetLog(lv)
	log.SetOutput(lv)
	// set the upper left corner icon
	if icon, err := walk.NewIconFromResourceId(3); err == nil {
		mw.SetIcon(icon)
	}
}
