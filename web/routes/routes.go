package routes

import (
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"gitlab.com/abibino-lab/mobal/logs"
	gmw "gitlab.com/abibino-lab/mobal/web/api/middleware"

	api "gitlab.com/abibino-lab/mobal/web/api/controllers"
	"gitlab.com/abibino-lab/mobal/web/api/models"
	// front "gitlab.com/abibino-lab/mobal/web/front/controllers"
)

// Init is a func for ...
func Init(e *echo.Echo) {
	logs.Log.App("------------------")
	api.AppInit()
	// ====================================================
	// Set static file
	// ====================================================
	e.Static("/", "web/public/static")
	e.File("/favicon.ico", "web/public/static/images/favicon.ico")
	// ====================================================
	// Set the route
	// ====================================================
	// Start route
	// ====================================================
	// Unauthenticated route
	// ====================================================

	// ====================================================
	// ROUTES FRONT
	// ====================================================
	// h := front.Handler{}
	// h.Init()

	// e.GET("/", h.PageIndex)
	// e.GET("/crawler", h.Crawler)
	// e.GET("/search", h.GetSearch)
	// e.GET("/user/profile", h.Profile)
	// e.GET("/user/history", h.Profile)
	// e.GET("/user/management", h.Management)

	// e.GET("/administrator/", h.Dashboard)
	// e.GET("/administrator/user", h.User)
	// e.GET("/administrator/location", h.Location)
	// e.GET("/administrator/item", h.Item)
	// e.GET("/administrator/category", h.Category)
	// e.GET("/administrator/tsukamoto", h.Category)
	// e.GET("/administrator/report", h.Category)

	// ====================================================
	// ROUTES API
	// ====================================================
	e.POST("/api/v1/user/auth", api.Auth)
	e.POST("/api/v1/user/register", api.Register)
	e.GET("/api/v1/ws", api.WSHandler)        // Set websocket to request routing
	e.GET("/api/v1/ws/log", api.WSLogHandler) // Set websocket to report print-only routing

	// Restricted group
	r := e.Group("/api/v1")

	// Get signingKey
	err := gmw.RSA.New()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Configure middleware with the custom claims type
	config := mw.JWTConfig{
		SigningMethod: "RS256",
		ContextKey:    "user",
		TokenLookup:   "header:" + echo.HeaderAuthorization,
		Claims:        &models.JwtCustomClaims{},
		SigningKey:    gmw.RSA.VerifyKey,
	}
	r.Use(mw.JWTWithConfig(config))

	// ====================================================
	// Authenticated route
	// ====================================================
	r.GET("/search", api.Search)
	r.GET("/user", api.Restricted)
	r.GET("/user/:id", api.Accessible)

	// Route level middleware
	track := func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			println("request to /users")
			return next(c)
		}
	}
	r.GET("/users", func(c echo.Context) error {
		return c.String(http.StatusOK, "/users")
	}, track)

	r.POST("/user/logout", api.Logout)
}
