package proxy

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/app/downloader/surfer"
	"gitlab.com/abibino-lab/mobal/common/ping"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

// Proxy is a struct to used for managing proxy
type Proxy struct {
	ipRegexp    *regexp.Regexp
	proxyRegexp *regexp.Regexp
	allIps      map[string]string
	all         map[string]bool
	online      int64
	usable      map[string]*UsableProxy
	ticker      *time.Ticker
	tickMinute  int64
	threadPool  chan bool
	surf        surfer.Surfer
	sync.Mutex
}

const (
	// ConnectionTimeout is a interval for waiting to connect
	ConnectionTimeout = 4 //4s
	// DialTimeout is a interval for waiting to dial
	DialTimeout = 4 //4s
	// TryTimes is a time try again
	TryTimes = 3
	// MaxThreadNum is the maximum concurrency of IP speed
	MaxThreadNum = 1000
)

// New is a function for create proxy
func New() *Proxy {
	p := &Proxy{
		ipRegexp:    regexp.MustCompile("[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+"),
		proxyRegexp: regexp.MustCompile("http[s]?://[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+:[0-9]+"),
		allIps:      map[string]string{},
		all:         map[string]bool{},
		usable:      make(map[string]*UsableProxy),
		threadPool:  make(chan bool, MaxThreadNum),
		surf:        surfer.New(),
	}
	go p.Update()
	return p
}

// Count is a funtion for get count of proxy IP
func (proxy *Proxy) Count() int64 {
	return proxy.online
}

// Update is a funtion for update proxy IP list
func (proxy *Proxy) Update() *Proxy {
	f, err := os.Open(config.PROXY)
	if err != nil {
		// logs.Log.Error("Error: %v", err)
		return proxy
	}
	b, _ := ioutil.ReadAll(f)
	f.Close()

	proxys := proxy.proxyRegexp.FindAllString(string(b), -1)
	for _, p := range proxys {
		proxy.allIps[p] = proxy.ipRegexp.FindString(p)
		proxy.all[p] = false
		// fmt.Printf("+ Proxy IP %v:%v\n", i, proxy)
	}
	logs.Log.Informational("Read proxy IP: %v Item", len(proxy.all))

	proxy.findOnline()

	return proxy
}

// findOnline is a function for filter online proxy IP
func (proxy *Proxy) findOnline() *Proxy {
	logs.Log.Informational("Filtering online proxy IP ...")
	proxy.online = 0
	for p := range proxy.all {
		proxy.threadPool <- true
		go func(p string) {
			alive, _, _ := ping.Ping(proxy.allIps[p], ConnectionTimeout)
			proxy.Lock()
			proxy.all[p] = alive
			proxy.Unlock()
			if alive {
				atomic.AddInt64(&proxy.online, 1)
			}
			<-proxy.threadPool
		}(p)
	}
	for len(proxy.threadPool) > 0 {
		time.Sleep(0.2e9)
	}
	logs.Log.Informational("Online proxy IP filtering is complete, total:%v Item", proxy.online)

	return proxy
}

// UpdateTicker is a function for update timer
func (proxy *Proxy) UpdateTicker(tickMinute int64) {
	proxy.tickMinute = tickMinute
	proxy.ticker = time.NewTicker(time.Duration(proxy.tickMinute) * time.Minute)
	for _, proxyForHost := range proxy.usable {
		proxyForHost.curIndex++
		proxyForHost.isEcho = true
	}
}

// GetOne is a fucntion for get unused proxy IP and response the time for loop
func (proxy *Proxy) GetOne(u string) (curProxy string) {
	if proxy.online == 0 {
		return
	}
	u2, _ := url.Parse(u)
	if u2.Host == "" {
		logs.Log.Informational("[%v]Setting proxy IP failed and target url is incorrect", u)
		return
	}
	var key = u2.Host
	if strings.Count(key, ".") > 1 {
		key = key[strings.Index(key, ".")+1:]
	}

	proxy.Lock()
	defer proxy.Unlock()

	var ok = true
	var proxyForHost = proxy.usable[key]

	select {
	case <-proxy.ticker.C:
		proxyForHost.curIndex++
		if proxyForHost.curIndex >= proxyForHost.Len() {
			_, ok = proxy.testAndSort(key, u2.Scheme+"://"+u2.Host)
		}
		proxyForHost.isEcho = true

	default:
		if proxyForHost == nil {
			proxy.usable[key] = &UsableProxy{
				proxys:    []string{},
				timeDelay: []time.Duration{},
				isEcho:    true,
			}
			proxyForHost, ok = proxy.testAndSort(key, u2.Scheme+"://"+u2.Host)
		} else if l := proxyForHost.Len(); l == 0 {
			ok = false
		} else if proxyForHost.curIndex >= l {
			_, ok = proxy.testAndSort(key, u2.Scheme+"://"+u2.Host)
			proxyForHost.isEcho = true
		}
	}
	if !ok {
		logs.Log.Informational("[%v]Setting proxy IP failed with no proxy IP available", key)
		return
	}
	curProxy = proxyForHost.proxys[proxyForHost.curIndex]
	if proxyForHost.isEcho {
		logs.Log.Informational("Set the proxy IP to [%v](%v)",
			curProxy,
			proxyForHost.timeDelay[proxyForHost.curIndex],
		)
		proxyForHost.isEcho = false
	}
	return
}

// testAndSort is function for test and sort
func (proxy *Proxy) testAndSort(key string, testHost string) (*UsableProxy, bool) {
	logs.Log.Informational("[%v]Testing and sorting proxy IP ...", key)
	proxyMangement := proxy.usable[key]
	proxyMangement.proxys = []string{}
	proxyMangement.timeDelay = []time.Duration{}
	proxyMangement.curIndex = 0
	for p, online := range proxy.all {
		if !online {
			continue
		}
		proxy.threadPool <- true
		go func(p string) {
			alive, timeDelay := proxy.findUsable(p, testHost)
			if alive {
				proxyMangement.Mutex.Lock()
				proxyMangement.proxys = append(proxyMangement.proxys, p)
				proxyMangement.timeDelay = append(proxyMangement.timeDelay, timeDelay)
				proxyMangement.Mutex.Unlock()
			}
			<-proxy.threadPool
		}(p)
	}
	for len(proxy.threadPool) > 0 {
		time.Sleep(0.2e9)
	}
	if proxyMangement.Len() > 0 {
		sort.Sort(proxyMangement)
		logs.Log.Informational("[%v]Test and sort proxy IP complete, available:%v Item", key, proxyMangement.Len())
		return proxyMangement, true
	}
	logs.Log.Informational("[%v]The test and sort proxy IP completes, no proxy IP is available", key)
	return proxyMangement, false
}

// findUsable is funtion for test proxy ip availability
func (proxy *Proxy) findUsable(p string, testHost string) (alive bool, timeDelay time.Duration) {
	t0 := time.Now()
	req := &request.Request{
		URL:         testHost,
		Method:      "HEAD",
		Header:      make(http.Header),
		DialTimeout: time.Second * time.Duration(DialTimeout),
		ConnTimeout: time.Second * time.Duration(ConnectionTimeout),
		TryTimes:    TryTimes,
	}
	req.SetProxy(p)
	_, err := proxy.surf.Download(req)
	return err == nil, time.Since(t0)
}
