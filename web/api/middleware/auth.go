package middleware

import (
	"crypto/rsa"
	"io/ioutil"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/abibino-lab/mobal/config"
)

// RSAKeys is a struct for store key
type (
	RSAKeys struct {
		VerifyKey *rsa.PublicKey
		SignKey   *rsa.PrivateKey
	}
)

// RSA is struct for store...
var RSA RSAKeys

// New is a func for ...
func (keys *RSAKeys) New() (err error) {
	signBytes, err := ioutil.ReadFile(config.PRIVATEKEY_PATH)
	if err != nil {
		return
	}

	keys.SignKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return
	}

	verifyBytes, err := ioutil.ReadFile(config.PUBLICKEY_PATH)
	if err != nil {
		return
	}

	keys.VerifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)

	return
}
