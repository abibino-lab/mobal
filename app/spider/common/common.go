package common

import (
	"math"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/abibino-lab/mobal/common/goquery"
	"gitlab.com/abibino-lab/mobal/common/mahonia"
	"gitlab.com/abibino-lab/mobal/common/ping"
)

// clear the label
func CleanHtml(str string, depth int) string {
	if depth > 0 {
		// Convert all HTML tags to lowercase
		re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
		str = re.ReplaceAllStringFunc(str, strings.ToLower)
	}
	if depth > 1 {
		// remove STYLE
		re, _ := regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
		str = re.ReplaceAllString(str, "")
	}
	if depth > 2 {
		// remove SCRIPT
		re, _ := regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
		str = re.ReplaceAllString(str, "")
	}
	if depth > 3 {
		// remove all HTML code in angle brackets and replace it with newlines
		re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
		str = re.ReplaceAllString(str, "\n")
	}
	if depth > 4 {
		// Remove successive line breaks
		re, _ := regexp.Compile("\\s{2,}")
		str = re.ReplaceAllString(str, "\n")
	}
	return str
}

func fieldSet(fields ...string) map[string]bool {
	set := make(map[string]bool, len(fields))
	for _, s := range fields {
		set[s] = true
	}
	return set
}

// RemoveNilOrEmptyValue is a ...
func RemoveNilOrEmptyValue(data map[string]interface{}) map[string]interface{} {
	out := make(map[string]interface{})
	temp := reflect.Value{}

	for k, v := range data {
		if v != nil {
			temp = reflect.ValueOf(v)
			switch temp.Kind() {
			case reflect.String:
				if v != "" {
					out[k] = v
				}
			case reflect.Slice, reflect.Map, reflect.Array:
				if temp.Len() > 0 {
					out[k] = v
				}
			default:
				out[k] = v
			}
		}
	}

	if len(out) > 0 {
		return out
	}

	return nil
}

// ExtractArticle -> extract the text of the article page
// Thinking: Think the parent of the longest label of the text node is the text of the article
func ExtractArticle(html string) string {
	// Convert all HTML tags to lowercase
	re := regexp.MustCompile("<[\\S\\s]+?>")
	html = re.ReplaceAllStringFunc(html, strings.ToLower)
	// remove the head
	re = regexp.MustCompile("<head[\\S\\s]+?</head>")
	html = re.ReplaceAllString(html, "")
	// remove STYLE
	re = regexp.MustCompile("<style[\\S\\s]+?</style>")
	html = re.ReplaceAllString(html, "")
	// remove SCRIPT
	re = regexp.MustCompile("<script[\\S\\s]+?</script>")
	html = re.ReplaceAllString(html, "")
	// remove the comment
	re = regexp.MustCompile("<![\\S\\s]+?>")
	html = re.ReplaceAllString(html, "")
	// fmt.Println (html)

	// Get each subtab
	re = regexp.MustCompile("<[A-Za-z]+[^<]*>([^<>]+)</[A-Za-z]+>")
	ss := re.FindAllStringSubmatch(html, -1)
	// fmt.Printf ("all subtabs: \ n% # v \ n", ss)

	var maxLen int
	var idx int
	for k, v := range ss {
		l := len([]rune(v[1]))
		if l > maxLen {
			maxLen = l
			idx = k
		}
	}
	// fmt.Println ("longest paragraph:", ss [idx] [0])

	html = strings.Replace(html, ss[idx][0], `<af id="mobal">`+ss[idx][1]+`</af>`, -1)
	r := strings.NewReader(html)
	dom, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		return ""
	}
	return dom.Find("af#af").Parent().Text()
}

// remove common escape characters
func Deprive(s string) string {
	s = strings.Replace(s, "\n", "", -1)
	s = strings.Replace(s, "\r", "", -1)
	s = strings.Replace(s, "\t", "", -1)
	s = strings.Replace(s, ` `, "", -1)
	return s
}

// remove common escape characters
func Deprive2(s string) string {
	s = strings.Replace(s, "\n", "", -1)
	s = strings.Replace(s, "\r", "", -1)
	s = strings.Replace(s, "\t", "", -1)
	s = strings.Replace(s, `\n`, "", -1)
	s = strings.Replace(s, `\r`, "", -1)
	s = strings.Replace(s, `\t`, "", -1)
	s = strings.Replace(s, ` `, "", -1)
	return s
}

// the mantissa
func Floor(f float64, n int) float64 {
	pow10_n := math.Pow10(n)
	return math.Trunc((f)*pow10_n) / pow10_n
}

// cookies string to [] * http.Cookie, (eg "mt = ci% 3D-1_0; thw = cn; sec = 5572dc7c40ce07d4e8c67e4879a; v = 0;")
func SplitCookies(cookieStr string) (cookies []*http.Cookie) {
	slice := strings.Split(cookieStr, ";")
	for _, v := range slice {
		oneCookie := &http.Cookie{}
		s := strings.Split(v, "=")
		if len(s) == 2 {
			oneCookie.Name = strings.Trim(s[0], " ")
			oneCookie.Value = strings.Trim(s[1], " ")
			cookies = append(cookies, oneCookie)
		}
	}
	return
}

func DecodeString(src, charset string) string {
	return mahonia.NewDecoder(charset).ConvertString(src)
}

func EncodeString(src, charset string) string {
	return mahonia.NewEncoder(charset).ConvertString(src)
}

func ConvertToString(src string, srcCode string, tagCode string) string {
	srcCoder := mahonia.NewDecoder(srcCode)
	srcResult := srcCoder.ConvertString(src)
	tagCoder := mahonia.NewDecoder(tagCode)
	_, cdata, _ := tagCoder.Translate([]byte(srcResult), true)
	result := string(cdata)
	return result
}

// func GBKToUTF8 (src string) string {
// return DecodeString (EncodeString (src, "ISO-8859-1"), "GBK")
//}

func GBKToUTF8(src string) string {
	return DecodeString(src, "GB18030")
}

// turn "brown" green "to" brown | green "
func UnicodeToUTF8(str string) string {
	str = strings.TrimLeft(str, "&#")
	str = strings.TrimRight(str, ";")
	strSlice := strings.Split(str, ";&#")

	for k, s := range strSlice {
		if i, err := strconv.Atoi(s); err == nil {
			strSlice[k] = string(i)
		}
	}
	return strings.Join(strSlice, "")
}

// turn `{" area ": [[" quanguo "," \ "u") "[" } `
func Unicode16ToUTF8(str string) string {
	i := 0
	if strings.Index(str, "\\u") > 0 {
		i = 1
	}
	strSlice := strings.Split(str, "\\u")
	last := len(strSlice) - 1
	if len(strSlice[last]) > 4 {
		strSlice = append(strSlice, string(strSlice[last][4:]))
		strSlice[last] = string(strSlice[last][:4])
	}
	for ; i <= last; i++ {
		if x, err := strconv.ParseInt(strSlice[i], 16, 32); err == nil {
			strSlice[i] = string(x)
		}
	}
	return strings.Join(strSlice, "")
}

// @SchemeAndHost https://www.baidu.com
// @Path / search? W = x
func MakeURL(path string, schemeAndHost ...string) (string, bool) {
	if string(path[0]) != "/" && strings.ToLower(string(path[0])) != "h" {
		path = "/" + path
	}
	u := path
	idx := strings.Index(path, ":// ")
	if idx < 0 {
		if len(schemeAndHost) > 0 {
			u = schemeAndHost[0] + u
		} else {
			return u, false
		}
	}
	_, err := url.Parse(u)
	if err != nil {
		return u, false
	}
	return u, true
}

func Pinger(address string, timeoutSecond int) error {
	return ping.Pinger(address, timeoutSecond)
}

func Ping(address string, timeoutSecond int) (alive bool, err error, timeDelay time.Duration) {
	return ping.Ping(address, timeoutSecond)
}

// html filter comments
// var htmlReg = regexp.MustCompile ("(<! - (.) * ->) | ([\ s \ v] + / * ([^ (/ \ *)] | [^ (\ * / )] * \ * /) | ([\ T \ n \ f \ r \ v \ v] + [# | //] [^ \ t \ n \ f \ r \ v] +) ([\ R | \ f | \ t | \ n | \ v])
var htmlReg = regexp.MustCompile("(\\*{1,2}[\\s\\S]*?\\*)|(<!-[\\s\\S]*?-->)|(^\\s*\\n)") // (// [\ s \ S] *? \ N) |

// handle html files - add by lyken 20160512
func ProcessHtml(html string) string {
	// remove the comment
	html = htmlReg.ReplaceAllString(html, "")
	// re: = regexp.MustCompile ("<! [\\ s \\ s] +?>")
	// html = re.ReplaceAllString (html, "")

	// Convert all HTML tags to lowercase
	// re, _ = regexp.Compile ("\\ <[\\ s \\ s] +? \\>")
	// html = re.ReplaceAllStringFunc (html, strings.ToLower)

	// Remove successive line breaks
	// re, _ = regexp.Compile ("\\ s {2,}")
	// html = re.ReplaceAllString (html, "\ n")

	return html
}

// Clear linefeed - by by lyken 20160512
func DepriveBreak(s string) string {
	s = strings.Replace(s, "\n", "", -1)
	s = strings.Replace(s, "\r", "", -1)
	s = strings.Replace(s, "\t", "", -1)
	s = strings.Replace(s, `\n`, "", -1)
	s = strings.Replace(s, `\r`, "", -1)
	s = strings.Replace(s, `\t`, "", -1)
	return s
}

// extra linefeeds - by by lyken 20160819
func DepriveMutiBreak(s string) string {
	re, _ := regexp.Compile("([^\n\f\r\t 　 ]*)([ 　 ]*[\n\f\r\t]+[ 　 ]*)+")
	return re.ReplaceAllString(s, "${1}\n")

}

// add parameters to the original URL --add by lyken 20160901
func HrefSub(src string, sub string) string {
	if len(sub) > 0 {
		if strings.Index(src, "?") > -1 {
			src += "&" + sub
		} else {
			src += "?" + sub
		}
	}
	return src
}

// domain name to get Reg
var domainReg = regexp.MustCompile(`([a-zA-Z0-9]+:// ([a-zA-Z0-9 \: \ _ \ - \.]) + (/)?) (.) * `)

// URL combination - add by lyken 20160512
func GetHerf(baseurl string, url string, herf string, mustBase bool) string {
	if strings.HasPrefix(herf, `javascript:`) {
		return ``
	}
	result := ""
	herf = Deprive2(herf)
	if !strings.HasSuffix(baseurl, "/") {
		baseurl += "/"
	}

	if !mustBase && !strings.HasPrefix(url, baseurl) {
		baseurl = domainReg.ReplaceAllString(url, "$1")
	}

	refIndex := strings.LastIndex(url, "/") + 1
	/*sub := url[refIndex:]
	if !strings.HasSuffix(url, "/") {
		if strings.Index(sub, ".") == -1 &&
			strings.Index(sub, "?") == -1 &&
			strings.Index(sub, "#") == -1 {
			url = url + `/`
		} else {
			url = url[:refIndex]
		}
	}*/
	url = url[:refIndex]

	/*refIndex = strings.LastIndex(herf, "/") + 1
	sub = herf[refIndex:]
	if len(sub) > 0 &&
		strings.Index(sub, ".") == -1 &&
		strings.Index(sub, "?") == -1 &&
		strings.Index(sub, "#") == -1 {
		herf = herf + `/`
	}*/

	if strings.HasPrefix(herf, "./../") {
		herf = strings.Replace(herf, "./", "", 1)
	}

	if len(herf) == 0 {
		result = ""
	} else if herf == "/" {
		result = baseurl
	} else if strings.HasPrefix(herf, "./") {
		/*reg := regexp.MustCompile("^(./)(.*)")
		result = url + strings.Trim(reg.ReplaceAllString(herf, "$2"), " ")*/
		result = url + strings.Replace(herf, "./", "", 1)
	} else if strings.HasPrefix(herf, "/") {
		// reg = regexp.MustCompile ("^ (http) (s)? (: //) ([0-9A-Za-z. \ -_] +) (/) (. *)")
		result = strings.Trim(baseurl, " ") + herf[1:]
	} else if mustBase && !strings.HasPrefix(herf, baseurl) &&
		(strings.Index(herf, ":// ") > -1 ||
			(strings.Index(herf, "/") == -1 &&
				strings.Count(herf, ".") > 3)) { // IP

		result = ""
	} else if strings.Index(herf, ":// ") > -1 ||
		(strings.Index(herf, "/") == -1 && strings.Count(herf, ".") > 3) { // IP
		result = herf
	} else {
		count := strings.Count(herf, "../")
		if count > 0 {
			urlArr := strings.SplitAfter(url, "/")
			len := cap(urlArr) - count - 1
			if len > 2 {
				preURL := ""
				for i, str := range urlArr {
					if len > i {
						preURL += str
					}
				}
				result = preURL + strings.Replace(herf, "../", "", -1)
			}
		} else {
			result = url + herf
		}
	}

	/*if strings.Count(result, ":// ")> 1 {
		result = strings.SplitN(result, ":// ", 2) [1]
	}*/

	return result
}
