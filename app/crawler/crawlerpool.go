package crawler

import (
	"sync"
	"time"

	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

type (
	// Pool is a engine pool collection
	Pool interface {
		Reset(spiderNum int) int
		Use() Crawler
		Free(Crawler)
		Stop()
	}
	cq struct {
		capacity int
		count    int
		usable   chan Crawler
		all      []Crawler
		status   int
		sync.RWMutex
	}
)

// NewCrawlerPool is a function for create new crawler pool
func NewCrawlerPool() Pool {
	return &cq{
		status: status.RUN,
		all:    make([]Crawler, 0, config.CRAWLS_CAP),
	}
}

// Set CrawlerPool according to the number of spiders to be executed
// In the second use of the Pool instance, according to the capacity of efficient conversion
func (cq *cq) Reset(spiderNum int) int {
	cq.Lock()
	defer cq.Unlock()
	var wantNum int
	if spiderNum < config.CRAWLS_CAP {
		wantNum = spiderNum
	} else {
		wantNum = config.CRAWLS_CAP
	}
	if wantNum <= 0 {
		wantNum = 1
	}
	cq.capacity = wantNum
	cq.count = 0
	cq.usable = make(chan Crawler, wantNum)
	for _, crawler := range cq.all {
		if cq.count < cq.capacity {
			cq.usable <- crawler
			cq.count++
		}
	}
	cq.status = status.RUN
	return wantNum
}

// Concurrent use of resources safely
func (cq *cq) Use() Crawler {
	var crawler Crawler
	for {
		cq.Lock()
		if cq.status == status.STOP {
			cq.Unlock()
			return nil
		}
		select {
		case crawler = <-cq.usable:
			cq.Unlock()
			return crawler
		default:
			if cq.count < cq.capacity {
				crawler = New(cq.count)
				cq.all = append(cq.all, crawler)
				cq.count++
				cq.Unlock()
				return crawler
			}

		}
		cq.Unlock()
		time.Sleep(time.Second)
	}
	//return nil
}

func (cq *cq) Free(crawler Crawler) {
	cq.RLock()
	defer cq.RUnlock()
	if cq.status == status.STOP || !crawler.CanStop() {
		return
	}
	cq.usable <- crawler
}

// Take the initiative to terminate all crawling tasks
func (cq *cq) Stop() {
	cq.Lock()
	// println("CrawlerPool^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	if cq.status == status.STOP {
		cq.Unlock()
		return
	}
	cq.status = status.STOP
	close(cq.usable)
	cq.usable = nil
	cq.Unlock()

	for _, crawler := range cq.all {
		crawler.Stop()
	}
	// println("CrawlerPool$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
}
