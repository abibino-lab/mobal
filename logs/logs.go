package logs

import (
	"fmt"
	"io"
	"os"
	"path"

	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs/logs"
)

type (
	// Logs is an interface for handle data log
	Logs interface {
		// Set the real-time log information display terminal
		SetOutput(show io.Writer) Logs
		// Pause the output log
		Rest()
		// Resumes the paused state and continues outputting the log
		GoOn()
		// whether to open the log capture copy mode
		EnableStealOne(bool)
		// In real time to capture a copy of the log, each return 1, normal mark log is closed
		StealOne() (level int, msg string, normal bool)
		// Normal shutdown log output
		Close()
		// return to the running state, such as 0, "RUN"
		Status() (int, string)
		DelLogger(adaptername string) error
		SetLogger(adaptername string, config map[string]interface{}) error

		// The following print method In addition to the normal log output, if the client or server mode will also send socket information
		Debug(format string, v ...interface{})
		Informational(format string, v ...interface{})
		App(format string, v ...interface{})
		Notice(format string, v ...interface{})
		Warning(format string, v ...interface{})
		Error(format string, v ...interface{})
		Critical(format string, v ...interface{})
		Alert(format string, v ...interface{})
		Emergency(format string, v ...interface{})
	}
	mylog struct {
		*logs.BeeLogger
	}
)

// Log is a variable for store data logs
var Log = func() Logs {
	p, _ := path.Split(config.LOG)
	// do not exist when the directory is created
	d, err := os.Stat(p)
	if err != nil || !d.IsDir() {
		if err := os.MkdirAll(p, 0777); err != nil {
			fmt.Printf("Error: %v\n", err)
		}
	}

	ml := &mylog{BeeLogger: logs.NewLogger(config.LOG_CAP, config.LOG_FEEDBACK_LEVEL)}

	// print line information
	ml.BeeLogger.EnableFuncCallDepth(config.LOG_LINEINFO)
	// global log print level (also log file output level)
	ml.BeeLogger.SetLevel(config.LogLevel)
	// whether to output logs asynchronously
	ml.BeeLogger.Async(config.LOG_ASYNC)
	// Set the log display location
	ml.BeeLogger.SetLogger("console", map[string]interface{}{
		"level": config.LOG_CONSOLE_LEVEL,
	})

	// Save all logs to local files
	if config.LOG_SAVE {
		err = ml.BeeLogger.SetLogger("file", map[string]interface{}{
			"filename": config.LOG,
		})
		if err != nil {
			fmt.Printf("Log document creation failed:%v", err)
		}
	}
	return ml
}()

func (ml *mylog) SetOutput(show io.Writer) Logs {
	ml.BeeLogger.SetLogger("console", map[string]interface{}{
		"writer": show,
		"level":  config.LOG_CONSOLE_LEVEL,
	})
	return ml
}
