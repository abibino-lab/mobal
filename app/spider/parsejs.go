package spider

import (
	"encoding/xml"
	"io/ioutil"
	"path"
	"path/filepath"

	"github.com/robertkrimen/otto"

	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

type (

	// SpiderModel is rule interpreter model
	SpiderModel struct {
		Name            string      `xml:"Name"`
		Description     string      `xml:"Description"`
		Pausetime       int64       `xml:"Pausetime"`
		EnableLimit     bool        `xml:"EnableLimit"`
		EnableKeyIn     bool        `xml:"EnableKeyIn"`
		EnableCookie    bool        `xml:"EnableCookie"`
		NotDefaultField bool        `xml:"NotDefaultField"`
		Namespace       string      `xml:"Namespace>Script"`
		SubNamespace    string      `xml:"SubNamespace>Script"`
		Root            string      `xml:"Root>Script"`
		Trunk           []RuleModel `xml:"Rule"`
	}

	// RuleModel is a strcut for ...
	RuleModel struct {
		Name      string `xml:"name,attr"`
		ParseFunc string `xml:"ParseFunc>Script"`
		AidFunc   string `xml:"AidFunc>Script"`
	}
)

func init() {
	for _, _m := range getSpiderModels() {
		m := _m // Guaranteed closure variables
		var sp = &Spider{
			Name:            m.Name,
			Description:     m.Description,
			Pausetime:       m.Pausetime,
			EnableCookie:    m.EnableCookie,
			NotDefaultField: m.NotDefaultField,
			RuleTree:        &RuleTree{Trunk: map[string]*Rule{}},
		}
		if m.EnableLimit {
			sp.Limit = Limit
		}
		if m.EnableKeyIn {
			sp.KeyIn = KeyIn
		}

		if m.Namespace != "" {
			sp.Namespace = func(spider *Spider) string {
				vm := otto.New()
				vm.Set("self", spider)
				val, err := vm.Eval(m.Namespace)
				if err != nil {
					logs.Log.Error("Dynamic rules  [Namespace]: %v", err)
				}
				s, _ := val.ToString()
				return s
			}
		}

		if m.SubNamespace != "" {
			sp.SubNamespace = func(spider *Spider, dataCell map[string]interface{}) string {
				vm := otto.New()
				vm.Set("self", spider)
				vm.Set("dataCell", dataCell)
				val, err := vm.Eval(m.SubNamespace)
				if err != nil {
					logs.Log.Error("Dynamic rules  [SubNamespace]: %v", err)
				}
				s, _ := val.ToString()
				return s
			}
		}

		sp.RuleTree.Root = func(ctx *Context) {
			vm := otto.New()
			vm.Set("ctx", ctx)
			_, err := vm.Eval(m.Root)
			if err != nil {
				logs.Log.Error("Dynamic rules  [Root]: %v", err)
			}
		}

		for _, rule := range m.Trunk {
			r := new(Rule)
			r.ParseFunc = func(parse string) func(*Context) {
				return func(ctx *Context) {
					vm := otto.New()
					vm.Set("ctx", ctx)
					_, err := vm.Eval(parse)
					if err != nil {
						logs.Log.Error("Dynamic rules  [ParseFunc]: %v", err)
					}
				}
			}(rule.ParseFunc)

			r.AidFunc = func(parse string) func(*Context, map[string]interface{}) interface{} {
				return func(ctx *Context, aid map[string]interface{}) interface{} {
					vm := otto.New()
					vm.Set("ctx", ctx)
					vm.Set("aid", aid)
					val, err := vm.Eval(parse)
					if err != nil {
						logs.Log.Error("Dynamic rules  [AidFunc]: %v", err)
					}
					return val
				}
			}(rule.ParseFunc)
			sp.RuleTree.Trunk[rule.Name] = r
		}
		sp.Register()
	}
}

func getSpiderModels() (ms []*SpiderModel) {
	defer func() {
		if p := recover(); p != nil {
			logs.Log.Error("HTMLDynamic rule analysis: %v", p)
		}
	}()
	files, _ := filepath.Glob(path.Join(config.SPIDER_DIR, "*"+config.SPIDER_EXT))
	for _, filename := range files {
		b, err := ioutil.ReadFile(filename)
		if err != nil {
			logs.Log.Error("HTMLDynamic rules[%s]: %v", filename, err)
			continue
		}
		var m SpiderModel
		err = xml.Unmarshal(b, &m)
		if err != nil {
			logs.Log.Error("HTMLDynamic rules[%s]: %v", filename, err)
			continue
		}
		ms = append(ms, &m)
	}
	return
}
