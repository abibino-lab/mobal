package distribute

import (
	"encoding/json"

	"gitlab.com/abibino-lab/teleport"

	"gitlab.com/abibino-lab/mobal/logs"
)

// MasterAPI the master node API
func MasterAPI(n Distributer) teleport.API {
	return teleport.API{
		// assign tasks to the client
		"task": &masterTaskHandle{n},

		// print the received log
		"log": &masterLogHandle{},
	}
}

// The master node automatically assigns the task
type masterTaskHandle struct {
	Distributer
}

func (masterTaskHandle *masterTaskHandle) Process(receive *teleport.NetData) *teleport.NetData {
	b, _ := json.Marshal(masterTaskHandle.Send(masterTaskHandle.CountNodes()))
	return teleport.ReturnData(string(b))
}

// The master node automatically receives the message from the node and prints the operation
type masterLogHandle struct{}

func (*masterLogHandle) Process(receive *teleport.NetData) *teleport.NetData {
	logs.Log.Informational(" * ")
	logs.Log.Informational("[ %s ]    %s", receive.From, receive.Body)
	logs.Log.Informational(" * ")
	return nil
}
