package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// Update all matching data
type UpdateAll struct {
	Database   string                 // database
	Collection string                 // collection
	Selector   map[string]interface{} // document selector
	Change     map[string]interface{} // document update content
}

func (updateAll *UpdateAll) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	resultPtr2 := resultPtr.(*map[string]interface{})
	*resultPtr2 = map[string]interface{}{}

	err = Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(updateAll.Database).C(updateAll.Collection)

		if id, ok := updateAll.Selector["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be string type!")
			} else {
				updateAll.Selector["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		info, err := c.UpdateAll(updateAll.Selector, updateAll.Change)
		if err != nil {
			return err
		}

		(*resultPtr2)["Updated"] = info.Updated
		(*resultPtr2)["Removed"] = info.Removed
		(*resultPtr2)["UpsertedId"] = info.UpsertedId

		return err
	})

	return
}
