package mongo

import (
	"time"

	mgo "gopkg.in/mgo.v2"
)

// DataStore containing a pointer to a mgo session
type DataStore struct {
	Addrs      []string
	Database   string
	Collection string
	Timeout    time.Duration
	Session    *mgo.Session
}

// NewDataStore is constructor
func NewDataStore() *DataStore {
	return &DataStore{
		Addrs:    []string{"mongodb://root:root@localhost:27017"},
		Database: "mobal",
		Timeout:  60 * time.Second,
	}
}

// ConnectToTagServer is a helper method that connections to pubgears' tagserver
// database
func (ds *DataStore) ConnectToTagServer() {
	//	mongoDBDialInfo := &mgo.DialInfo{
	//		Addrs:    ds.Addrs,
	//		Timeout:  ds.Timeout,
	//		Database: ds.Database,
	//	}
	sess, err := mgo.Dial(ds.Addrs[0]) //mongoDBDialInfo)
	if err != nil {
		panic(err)
	}
	sess.SetMode(mgo.Monotonic, true)
	ds.Session = sess
}

// GetCollection is data store collection
func (ds *DataStore) GetCollection(col string) *mgo.Collection {
	if len(ds.Collection) > 0 {
		return ds.Session.DB(ds.Database).C(ds.Collection)
	}

	if len(col) > 0 {
		return ds.Session.DB(ds.Database).C(col)
	}

	return nil
}

// Close is a helper method that ensures the session is properly terminated
func (ds *DataStore) Close() {
	ds.Session.Close()
}
