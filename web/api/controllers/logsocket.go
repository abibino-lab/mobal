package controllers

import (
	"time"

	"github.com/labstack/echo"
	"gitlab.com/abibino-lab/mobal/logs"
	ws "golang.org/x/net/websocket"
)

// WSLogHandler is a func for log send api ...
func WSLogHandler(c echo.Context) (err error) {
	sess, _ := GlobalSessions.SessionStart(nil, c.Request())
	sessID := sess.SessionID()
	logs.Log.Informational("Session ID (% v)!", sessID)

	ws.Handler(func(conn *ws.Conn) {
		defer conn.Close()
		defer func() {
			if p := recover(); p != nil {
				logs.Log.Error("%v", p)
			}
		}()

		if Lsc.connPool[sessID] == nil {
			Lsc.Add(sessID, conn)
		}
		go func() {
			defer func() {
				// Close the web front end log output and disconnect the websocket connection
				Lsc.Remove(sessID, conn)
			}()
			for {
				if err = ws.JSON.Receive(conn, nil); err != nil {
					logs.Log.Debug("websocket log receive error disconnected (% v)!", err)
					break
				}
				time.Sleep(10 * time.Second)
			}
		}()

		for msg := range Lsc.lvPool[sessID].logChan {
			if err = ws.Message.Send(conn, msg); err != nil {
				logs.Log.Debug("websocket send error disconnected (% v)!", err)
				break
			}
		}

	}).ServeHTTP(c.Response(), c.Request())
	return
}

// LogSocketController :
type LogSocketController struct {
	connPool map[string]*ws.Conn
	lvPool   map[string]*LogView
}

func (logSocketController *LogSocketController) Write(p []byte) (int, error) {
	for sessID, lv := range logSocketController.lvPool {
		if logSocketController.connPool[sessID] != nil {
			lv.Write(p)
		}
	}
	return len(p), nil
}

// Add :
func (logSocketController *LogSocketController) Add(sessID string, conn *ws.Conn) {
	logSocketController.connPool[sessID] = conn
	logSocketController.lvPool[sessID] = newLogView()
}

// Remove :
func (logSocketController *LogSocketController) Remove(sessID string, conn *ws.Conn) {
	defer func() {
		recover()
	}()
	if logSocketController.connPool[sessID] == nil {
		return
	}
	lv := logSocketController.lvPool[sessID]
	lv.closed = true
	close(lv.logChan)
	conn.Close()
	delete(logSocketController.connPool, sessID)
	delete(logSocketController.lvPool, sessID)
}

var Lsc = &LogSocketController{
	connPool: make(map[string]*ws.Conn),
	lvPool:   make(map[string]*LogView),
}

// set all log output locations to Log
type LogView struct {
	closed  bool
	logChan chan string
}

func newLogView() *LogView {
	return &LogView{
		logChan: make(chan string, 1024),
		closed:  false,
	}
}

func (logView *LogView) Write(p []byte) (int, error) {
	if logView.closed {
		goto end
	}
	defer func() { recover() }()
	logView.logChan <- (string(p) + "\r\n")
end:
	return len(p), nil
}

func (logView *LogView) Sprint() string {
	return <-logView.logChan
}
