package mysql

import (
	"database/sql"
	"errors"
	"strings"
	"sync"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

// ************************ MySQL Output *************************** //
// sql conversion structure
type MyTable struct {
	tableName        string
	columnNames      [][2]string   // the title field
	rowsCount        int           // Rows
	args             []interface{} // data
	sqlCode          string
	customPrimaryKey bool
	size             int // approximate size of the content size
}

var (
	err                error
	db                 *sql.DB
	once               sync.Once
	max_allowed_packet = config.MYSQL_MAX_ALLOWED_PACKET - 1024
	maxConnChan        = make(chan bool, config.MYSQL_CONN_CAP) // maximum execution limit
)

func DB() (*sql.DB, error) {
	return db, err
}

func Refresh() {
	once.Do(func() {
		db, err = sql.Open("mysql", config.MYSQL_CONN_STR+"/"+config.DB_NAME+"?charset=utf8")
		if err != nil {
			logs.Log.Error("Mysql:%v", err)
			return
		}
		db.SetMaxOpenConns(config.MYSQL_CONN_CAP)
		db.SetMaxIdleConns(config.MYSQL_CONN_CAP)
	})
	if err = db.Ping(); err != nil {
		logs.Log.Error("Mysql:%v", err)
	}
}

func New() *MyTable {
	return &MyTable{}
}

func (m *MyTable) Clone() *MyTable {
	return &MyTable{
		tableName:        m.tableName,
		columnNames:      m.columnNames,
		customPrimaryKey: m.customPrimaryKey,
	}
}

// set the table name
func (myTable *MyTable) SetTableName(name string) *MyTable {
	myTable.tableName = wrapSqlKey(name)
	return myTable
}

// set the form column
func (myTable *MyTable) AddColumn(names ...string) *MyTable {
	for _, name := range names {
		name = strings.Trim(name, " ")
		idx := strings.Index(name, " ")
		myTable.columnNames = append(myTable.columnNames, [2]string{wrapSqlKey(name[:idx]), name[idx+1:]})
	}
	return myTable
}

// set the primary key statement (optional)
func (myTable *MyTable) CustomPrimaryKey(primaryKeyCode string) *MyTable {
	myTable.AddColumn(primaryKeyCode)
	myTable.customPrimaryKey = true
	return myTable
}

// generate a "create a form" statement, before implementation to ensure that SetTableName (), AddColumn () has been implemented
func (myTable *MyTable) Create() error {
	if len(myTable.columnNames) == 0 {
		return errors.New("Column can not be empty")
	}
	myTable.sqlCode = `CREATE TABLE IF NOT EXISTS ` + myTable.tableName + " ("
	if !myTable.customPrimaryKey {
		myTable.sqlCode += `id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,`
	}
	for _, title := range myTable.columnNames {
		myTable.sqlCode += title[0] + ` ` + title[1] + `,`
	}
	myTable.sqlCode = myTable.sqlCode[:len(myTable.sqlCode)-1] + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;"

	maxConnChan <- true
	defer func() {
		myTable.sqlCode = ""
		<-maxConnChan
	}()

	// debug
	// println ("Create ():", myTable.sqlCode)

	_, err := db.Exec(myTable.sqlCode)
	return err
}

// Empty the form before execution to ensure that SetTableName () has been executed
func (myTable *MyTable) Truncate() error {
	maxConnChan <- true
	defer func() {
		<-maxConnChan
	}()
	_, err := db.Exec("TRUNCATE TABLE " + myTable.tableName)
	return err
}

// set the inserted 1 line of data
func (myTable *MyTable) addRow(value []string) *MyTable {
	for i, count := 0, len(value); i < count; i++ {
		myTable.args = append(myTable.args, value[i])
	}
	myTable.rowsCount++
	return myTable
}

// Smart insert data, one line at a time
func (myTable *MyTable) AutoInsert(value []string) *MyTable {
	var nsize int
	for _, v := range value {
		nsize += len(v)
	}
	if nsize > max_allowed_packet {
		logs.Log.Error("%v", "packet for query is too large. Try adjusting the 'maxallowedpacket'variable in the 'config.ini'")
		return myTable
	}
	myTable.size += nsize
	if myTable.size > max_allowed_packet {
		util.CheckErr(myTable.FlushInsert())
		return myTable.AutoInsert(value)
	}
	return myTable.addRow(value)
}

// Add sqlCode "insert data" statement, before implementation to ensure that Create (), AutoInsert () has been implemented
func (myTable *MyTable) FlushInsert() error {
	if myTable.rowsCount == 0 {
		return nil
	}

	colCount := len(myTable.columnNames)
	if colCount == 0 {
		return nil
	}

	myTable.sqlCode = `INSERT INTO ` + myTable.tableName + `("

	for _, v := range myTable.columnNames {
		myTable.sqlCode += v[0] + ","
	}

	myTable.sqlCode = myTable.sqlCode[:len(myTable.sqlCode)-1] + ") VALUES `

	blank := ",(" + strings.Repeat(",?", colCount)[1:] + ")"
	myTable.sqlCode += strings.Repeat(blank, myTable.rowsCount)[1:] + `;`

	defer func() {
		// Clear temporary data
		myTable.args = []interface{}{}
		myTable.rowsCount = 0
		myTable.size = 0
		myTable.sqlCode = ""
	}()

	maxConnChan <- true
	defer func() {
		<-maxConnChan
	}()

	// debug
	// println ("FlushInsert ():", myTable.sqlCode)

	_, err := db.Exec(myTable.sqlCode, myTable.args...)
	return err
}

// Get all the data
func (myTable *MyTable) SelectAll() (*sql.Rows, error) {
	if myTable.tableName == "" {
		return nil, errors.New("The table name can not be empty")
	}
	myTable.sqlCode = `SELECT * FROM ` + myTable.tableName + `;`

	maxConnChan <- true
	defer func() {
		<-maxConnChan
	}()
	return db.Query(myTable.sqlCode)
}

func wrapSqlKey(s string) string {
	return "`" + strings.Replace(s, "`", "", -1) + "`"
}
