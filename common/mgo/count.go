package mgo

// basic query
import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// incoming database and collection name | return the total number of documents
type Count struct {
	Database   string                 // database
	Collection string                 // collection
	Query      map[string]interface{} // Check for phrases
}

func (count *Count) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	resultPtr2 := resultPtr.(*int)
	*resultPtr2 = 0

	err = Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(count.Database).C(count.Collection)

		if id, ok := count.Query["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be a string type!")
			} else {
				count.Query["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		*resultPtr2, err = c.Find(count.Query).Count()
		return err
	})
	return
}
