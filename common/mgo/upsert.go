package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// update the first matching data, if no match is inserted
type Upsert struct {
	Database   string                 // database
	Collection string                 // collection
	Selector   map[string]interface{} // document selector
	Change     map[string]interface{} // document update content
}

func (upsert *Upsert) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	resultPtr2 := resultPtr.(*map[string]interface{})
	*resultPtr2 = map[string]interface{}{}

	err = Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(upsert.Database).C(upsert.Collection)

		if id, ok := upsert.Selector["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be a string type!")
			} else {
				upsert.Selector["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		info, err := c.Upsert(upsert.Selector, upsert.Change)
		if err != nil {
			return err
		}

		(*resultPtr2)["Updated"] = info.Updated
		(*resultPtr2)["Removed"] = info.Removed
		(*resultPtr2)["UpsertedId"] = info.UpsertedId

		return err
	})

	return
}
