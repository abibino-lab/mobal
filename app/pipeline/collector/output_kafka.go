package collector

import (
	"fmt"
	"sync"

	"gitlab.com/abibino-lab/mobal/common/kafka"
	"gitlab.com/abibino-lab/mobal/common/util"
)

/************************ Kafka Output ***************************/
func init() {
	var (
		kafkaSenders    = map[string]*kafka.KafkaSender{}
		kafkaSenderLock sync.RWMutex
	)

	var getKafkaSender = func(name string) (*kafka.KafkaSender, bool) {
		kafkaSenderLock.RLock()
		tab, ok := kafkaSenders[name]
		kafkaSenderLock.RUnlock()
		return tab, ok
	}

	var setKafkaSender = func(name string, tab *kafka.KafkaSender) {
		kafkaSenderLock.Lock()
		kafkaSenders[name] = tab
		kafkaSenderLock.Unlock()
	}

	DataOutput["kafka"] = func(collector *Collector) error {
		_, err := kafka.GetProducer()

		if err != nil {
			return fmt.Errorf("Kafka producer failed: %v", err)
		}
		var (
			kafkas    = make(map[string]*kafka.KafkaSender)
			namespace = util.FileNameReplace(collector.namespace())
		)
		for _, datacell := range collector.dataDocker {
			subNamespace := util.FileNameReplace(collector.subNamespace(datacell))
			topicName := joinNamespaces(namespace, subNamespace)
			sender, ok := kafkas[topicName]
			if !ok {
				sender, ok = getKafkaSender(topicName)
				if ok {
					kafkas[topicName] = sender
				} else {
					sender = kafka.New()
					sender.SetTopic(topicName)
					setKafkaSender(topicName, sender)
					kafkas[topicName] = sender
				}
			}
			data := make(map[string]interface{})
			for _, title := range collector.MustGetRule(datacell["ruleName"].(string)).ItemFields {
				vd := datacell["data"].(map[string]interface{})
				if v, ok := vd[title].(string); ok || vd[title] == nil {
					data[title] = v
				} else {
					data[title] = util.JsonString(vd[title])
				}
			}
			if collector.Spider.OutDefaultField() {
				data["currentLink"] = datacell["currentLink"].(string)
				data["parentLink"] = datacell["parentLink"].(string)
				data["downloadTime"] = datacell["downloadTime"].(string)
			}
			err := sender.Push(data)
			util.CheckErr(err)
		}
		kafkas = nil
		return nil
	}
}
