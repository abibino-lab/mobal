package data

import (
	"sync"
)

type (
	// data storage unit
	DataCell map[string]interface{}
	// file storage unit
	// FileCell stores the full file name: file / "Dir" / "ruleName" / "time" / "name"
	FileCell map[string]interface{}
)

var (
	dataCellPool = &sync.Pool{
		New: func() interface{} {
			return DataCell{}
		},
	}
	fileCellPool = &sync.Pool{
		New: func() interface{} {
			return FileCell{}
		},
	}
)

func GetDataCell(ruleName string, data map[string]interface{}, url string, parentUrl string, downloadTime string) DataCell {
	cell := dataCellPool.Get().(DataCell)
	cell["ruleName"] = ruleName    // Specifies the key in Data
	cell["data"] = data            // data store, key to be consistent with Rule Fields
	cell["currentLink"] = url      // for indexing
	cell["parentLink"] = parentUrl // DataCell's parent url
	cell["downloadTime"] = downloadTime
	return cell
}

func GetFileCell(ruleName, name string, bytes []byte) FileCell {
	cell := fileCellPool.Get().(FileCell)
	cell["ruleName"] = ruleName // store a portion of the path
	cell["name"] = name         // Specifies the file name
	cell["bytes"] = bytes       //document content
	return cell
}

func PutDataCell(cell DataCell) {
	cell["ruleName"] = nil
	cell["data"] = nil
	cell["currentLink"] = nil
	cell["parentLink"] = nil
	cell["downloadTime"] = nil
	dataCellPool.Put(cell)
}

func PutFileCell(cell FileCell) {
	cell["ruleName"] = nil
	cell["name"] = nil
	cell["bytes"] = nil
	fileCellPool.Put(cell)
}
