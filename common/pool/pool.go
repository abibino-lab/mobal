// Package pool is a package ...
// General resource pool, dynamically increase the resource instance, and support the idle resource timing recovery function.
package pool

import (
	"errors"
	"fmt"
	"runtime"
	"sync"
	"time"
)

type (
	// Pool is interface ...
	// resource pool (maximum capacity should be set)
	Pool interface {
		// call the resource in the resource pool
		Call(func(Src) error) error
		// Destroy resource pool
		Close()
		// returns the current number of resources
		Len() int
	}
	// classic resource pool
	classic struct {
		srcs     chan Src      // resource list (Src must be pointer type)
		capacity int           // resource pool capacity
		maxIdle  int           // Maximum number of free resources
		len      int           // the current number of resources
		factory  Factory       // Create a resource method
		gctime   time.Duration // free resource recovery time
		closed   bool          // mark whether the resource pool has been closed
		sync.RWMutex
	}
	// Src is interface
	// resource interface
	Src interface {
		// determine whether the resource is available
		Usable() bool
		// After using the reset method
		Reset()
		// By the resource pool before the self-destruction method
		Close()
	}
	// factory is interface ...
	// Create a resource method
	Factory func() (Src, error)
)

const (
	// gcTime is a constanta ...
	GcTime = 60e9
)

var (
	// closedError is a variable ...
	errClosed = errors.New("The resource pool is closed")
)

// classicPool is a function ...
// build a classic resource pool
func ClassicPool(capacity, maxIdle int, factory Factory, gctime ...time.Duration) Pool {
	if len(gctime) == 0 {
		gctime = append(gctime, GcTime)
	}
	pool := &classic{
		srcs:     make(chan Src, capacity),
		capacity: capacity,
		maxIdle:  maxIdle,
		factory:  factory,
		gctime:   gctime[0],
		closed:   false,
	}
	go pool.gc()
	return pool
}

// call the resource in the resource pool
func (classic *classic) Call(callback func(Src) error) (err error) {
	var src Src
wait:
	classic.RLock()
	if classic.closed {
		classic.RUnlock()
		return errClosed
	}
	select {
	case src = <-classic.srcs:
		classic.RUnlock()
		if !src.Usable() {
			classic.del(src)
			goto wait
		}
	default:
		classic.RUnlock()
		err = classic.incAuto()
		if err != nil {
			return err
		}
		runtime.Gosched()
		goto wait
	}
	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("%v", p)
		}
		classic.recover(src)
	}()
	err = callback(src)
	return err
}

// Destroy resource pool
func (classic *classic) Close() {
	classic.Lock()
	defer classic.Unlock()
	if classic.closed {
		return
	}
	classic.closed = true
	for i := len(classic.srcs); i >= 0; i-- {
		(<-classic.srcs).Close()
	}
	close(classic.srcs)
	classic.len = 0
}

// returns the current number of resources
func (classic *classic) Len() int {
	classic.RLock()
	defer classic.RUnlock()
	return classic.len
}

// idle resource recovery link
func (classic *classic) gc() {
	for !classic.isClosed() {
		classic.Lock()
		extra := len(classic.srcs) - classic.maxIdle
		if extra > 0 {
			classic.len -= extra
			for ; extra > 0; extra-- {
				(<-classic.srcs).Close()
			}
		}
		classic.Unlock()
		time.Sleep(classic.gctime)
	}
}

func (classic *classic) incAuto() error {
	classic.Lock()
	defer classic.Unlock()
	if classic.len >= classic.capacity {
		return nil
	}
	src, err := classic.factory()
	if err != nil {
		return err
	}
	classic.srcs <- src
	classic.len++
	return nil
}

func (classic *classic) del(src Src) {
	src.Close()
	classic.Lock()
	classic.len--
	classic.Unlock()
}

func (classic *classic) recover(src Src) {
	classic.RLock()
	defer classic.RUnlock()
	if classic.closed {
		return
	}
	src.Reset()
	classic.srcs <- src
}

func (classic *classic) isClosed() bool {
	classic.RLock()
	defer classic.RUnlock()
	return classic.closed
}
