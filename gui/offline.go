package gui

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

func offlineWindow() {
	mw.Close()

	if err := (MainWindow{
		AssignTo: &mw,
		DataBinder: DataBinder{
			AssignTo:       &db,
			DataSource:     Input,
			ErrorPresenter: ErrorPresenterRef{&ep},
		},
		Title:   config.FULL_NAME + "                                                          [Operation mode -> stand-alone]",
		MinSize: Size{1100, 700},
		Layout:  VBox{MarginsZero: true},
		Children: []Widget{

			Composite{
				AssignTo: &setting,
				Layout:   Grid{Columns: 2},
				Children: []Widget{
					// 任务列表
					TableView{
						ColumnSpan:            1,
						MinSize:               Size{550, 450},
						AlternatingRowBGColor: walk.RGB(255, 255, 224),
						CheckBoxes:            true,
						ColumnsOrderable:      true,
						Columns: []TableViewColumn{
							{Title: "#", Width: 45},
							{Title: "Task", Width: 110 /*, Format: "%.2f", Alignment: AlignFar*/},
							{Title: "Description", Width: 370},
						},
						Model: spiderMenu,
					},

					VSplitter{
						ColumnSpan: 1,
						MinSize:    Size{550, 450},
						Children: []Widget{

							VSplitter{
								Children: []Widget{
									Label{
										Text: "Custom configuration (multi-tasking, please pack a layer of \"<>\"）:",
									},
									LineEdit{
										Text: Bind("KeyIns"),
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Collection limit (default limit URL):",
									},
									NumberEdit{
										Value:    Bind("Limit"),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Concurrent coroutines:（1~99999）",
									},
									NumberEdit{
										Value:    Bind("ThreadNum", Range{1, 99999}),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Batch output size:（1~5,000,000 Item data",
									},
									NumberEdit{
										Value:    Bind("DockerCap", Range{1, 5000000}),
										Suffix:   "",
										Decimals: 0,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Pause time reference:",
									},
									ComboBox{
										Value:         Bind("Pausetime", SelRequired{}),
										DisplayMember: "Key",
										BindingMember: "Int64",
										Model:         GuiOpt.Pausetime,
									},
								},
							},

							VSplitter{
								Children: []Widget{
									Label{
										Text: "*Proxy IP replacement frequency:",
									},
									ComboBox{
										Value:         Bind("ProxyMinute", SelRequired{}),
										DisplayMember: "Key",
										BindingMember: "Int64",
										Model:         GuiOpt.ProxyMinute,
									},
								},
							},

							RadioButtonGroupBox{
								ColumnSpan: 1,
								Title:      "*Output method",
								Layout:     HBox{},
								DataMember: "OutType",
								Buttons:    outputList,
							},
						},
					},
				},
			},

			Composite{
				Layout: HBox{},
				Children: []Widget{
					VSplitter{
						Children: []Widget{
							// Required error check
							LineErrorPresenter{
								AssignTo: &ep,
							},
						},
					},

					HSplitter{
						MaxSize: Size{220, 50},
						Children: []Widget{
							Label{
								Text: "Inherit and save success records",
							},
							CheckBox{
								Checked: Bind("SuccessInherit"),
							},
						},
					},

					HSplitter{
						MaxSize: Size{220, 50},
						Children: []Widget{
							Label{
								Text: "Inherit and save the failed record",
							},
							CheckBox{
								Checked: Bind("FailureInherit"),
							},
						},
					},

					VSplitter{
						MaxSize: Size{90, 50},
						Children: []Widget{
							PushButton{
								Text:      "Pause/Resume",
								AssignTo:  &pauseRecoverBtn,
								OnClicked: offlinePauseRecover,
							},
						},
					},
					VSplitter{
						MaxSize: Size{90, 50},
						Children: []Widget{
							PushButton{
								Text:      "Start running",
								AssignTo:  &runStopBtn,
								OnClicked: offlineRunStop,
							},
						},
					},
				},
			},
		},
	}.Create()); err != nil {
		panic(err)
	}

	setWindow()

	pauseRecoverBtn.SetVisible(false)

	// Initialize the application
	Init()

	// Run the form program
	mw.Run()
}

// Pause/Resume
func offlinePauseRecover() {
	switch app.LogicApp.Status() {
	case status.RUN:
		pauseRecoverBtn.SetText("Resume operation")
	case status.PAUSE:
		pauseRecoverBtn.SetText("Timeout")
	}
	app.LogicApp.PauseRecover()
}

// Start/Stop control
func offlineRunStop() {
	if !app.LogicApp.IsStopped() {
		go func() {
			runStopBtn.SetEnabled(false)
			runStopBtn.SetText("Stop")
			pauseRecoverBtn.SetVisible(false)
			pauseRecoverBtn.SetText("Timeout")
			app.LogicApp.Stop()
			offlineResetBtn()
		}()
		return
	}

	if err := db.Submit(); err != nil {
		logs.Log.Error("%v", err)
		return
	}

	// Read the task
	Input.Spiders = spiderMenu.GetChecked()

	// if len(Input.Spiders) == 0 {
	// 	logs.Log.Warning("- Task list can not be empty")
	// 	return
	// }

	runStopBtn.SetText("Stop")

	// Record configuration information
	SetTaskConf()

	// Update spider queue
	SpiderPrepare()

	go func() {
		pauseRecoverBtn.SetText("Timeout")
		pauseRecoverBtn.SetVisible(true)
		app.LogicApp.Run()
		offlineResetBtn()
		pauseRecoverBtn.SetVisible(false)
		pauseRecoverBtn.SetText("Timeout")
	}()
}

// Offline 模式下按钮状态控制
func offlineResetBtn() {
	runStopBtn.SetEnabled(true)
	runStopBtn.SetText("Start running")
}
