package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// insert new data
type Insert struct {
	Database   string                   // database
	Collection string                   // collection
	Docs       []map[string]interface{} // documentation
}

const (
	MaxLen int = 5000 // allocate the insert
)

func (insert *Insert) Exec(resultPtr interface{}) (err error) {
	defer func() {
		if re := recover(); re != nil {
			err = fmt.Errorf("%v", re)
		}
	}()
	var (
		resultPtr2 = new([]string)
		count      = len(insert.Docs)
		docs       = make([]interface{}, count)
	)
	if resultPtr != nil {
		resultPtr2 = resultPtr.(*[]string)
	}
	*resultPtr2 = make([]string, count)

	return Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(insert.Database).C(insert.Collection)
		for i, doc := range insert.Docs {
			var _id string
			if doc["_id"] == nil || doc["_id"] == interface{}("") || doc["_id"] == interface{}(0) {
				objId := bson.NewObjectId()
				_id = objId.Hex()
				doc["_id"] = objId
			} else {
				_id = doc["_id"].(string)
			}

			if resultPtr != nil {
				(*resultPtr2)[i] = _id
			}
			docs[i] = doc
		}
		loop := count / MaxLen
		for i := 0; i < loop; i++ {
			err := c.Insert(docs[i*MaxLen : (i+1)*MaxLen]...)
			if err != nil {
				return err
			}
		}
		if count%MaxLen == 0 {
			return nil
		}
		return c.Insert(docs[loop*MaxLen:]...)
	})
}
