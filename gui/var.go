package gui

import (
	"github.com/lxn/walk"
	"github.com/lxn/walk/declarative"

	. "gitlab.com/abibino-lab/mobal/gui/model"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

// GUI input
type Inputor struct {
	Spiders []*GUISpider
	*cache.AppConf
	Pausetime   int64
	ProxyMinute int64
}

var (
	runStopBtn      *walk.PushButton
	pauseRecoverBtn *walk.PushButton
	setting         *walk.Composite
	mw              *walk.MainWindow
	runMode         *walk.GroupBox
	db              *walk.DataBinder
	ep              walk.ErrorPresenter
	mode            *walk.GroupBox
	host            *walk.Splitter
	spiderMenu      *SpiderMenu
)

var Input = &Inputor{
	AppConf:     cache.Task,
	Pausetime:   cache.Task.Pausetime,
	ProxyMinute: cache.Task.ProxyMinute,
}

// **************************************** GUI Content Display Configuration ******************************************* \\

// Output options
var outputList []declarative.RadioButton

// KV drop-down menu auxiliary structure
type KV struct {
	Key   string
	Int   int
	Int64 int64
}

// GuiOpt Pause time options and run mode options
var GuiOpt = struct {
	Mode        []*KV
	Pausetime   []*KV
	ProxyMinute []*KV
}{
	Mode: []*KV{
		{Key: "Stand-Alone", Int: status.OFFLINE},
		{Key: "Server", Int: status.SERVER},
		{Key: "Client", Int: status.CLIENT},
	},
	Pausetime: []*KV{
		{Key: "No pause", Int64: 0},
		{Key: "0.1 second", Int64: 100},
		{Key: "0.3 second", Int64: 300},
		{Key: "0.5 second", Int64: 500},
		{Key: "1 second", Int64: 1000},
		{Key: "3 seconds", Int64: 3000},
		{Key: "5 seconds", Int64: 5000},
		{Key: "10 seconds", Int64: 10000},
		{Key: "15 seconds", Int64: 15000},
		{Key: "20 seconds", Int64: 20000},
		{Key: "30 seconds", Int64: 30000},
		{Key: "60 seconds", Int64: 60000},
	},
	ProxyMinute: []*KV{
		{Key: "Do not use proxy", Int64: 0},
		{Key: "1 minute", Int64: 1},
		{Key: "3 minutes", Int64: 3},
		{Key: "5 minutes", Int64: 5},
		{Key: "10 minutes", Int64: 10},
		{Key: "15 minutes", Int64: 15},
		{Key: "20 minutes", Int64: 20},
		{Key: "30 minutes", Int64: 30},
		{Key: "45 minutes", Int64: 45},
		{Key: "60 minutes", Int64: 60},
		{Key: "120 minutes", Int64: 120},
		{Key: "180 minutes", Int64: 180},
	},
}
