var source = mongodb({
  "uri": "mongodb://localhost:27017/mobal",
  // "timeout": "30s",
  // "tail": true,
  // "ssl": false,
  // "cacerts": ["/path/to/cert.pem"],
  // "wc": 1,
  // "fsync": false,
  // "bulk": true,
  // "collection_filters": "{\"items\"}"
});

var sink = elasticsearch({
  "uri": "http://localhost:9200/mobal"
  // "timeout": "10s", // defaults to 30s
  // "aws_access_key": "ABCDEF", // used for signing requests to AWS Elasticsearch service
  // "aws_access_secret": "ABCDEF" // used for signing requests to AWS Elasticsearch service
});

t.Source("source", source, "/.*/").Transform(
    omit({"fields":["updatedAt"]})
).Transform(
    goja({"filename": "transform.js"})
).Save("sink", sink, "/.*/");
