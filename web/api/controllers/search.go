package controllers

import (
	"github.com/labstack/echo"

	"net/http"

	"github.com/erikdubbelboer/qtos"
	"gitlab.com/abibino-lab/mobal/web/api/models"
)

//Search is a function for ....
func Search(c echo.Context) (err error) {
	s := models.Search{}
	if err = qtos.Unmarshal(c.QueryParams(), &s); err != nil {
		return
	}

	s.Init()
	result, err := s.Find()

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, result)

}
