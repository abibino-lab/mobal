package gui

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"

	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

func runmodeWindow() {
	if err := (MainWindow{
		AssignTo: &mw,
		DataBinder: DataBinder{
			AssignTo:       &db,
			DataSource:     Input,
			ErrorPresenter: ErrorPresenterRef{&ep},
		},
		Title:   config.FULL_NAME,
		MinSize: Size{450, 350},
		Layout:  VBox{ /*MarginsZero: true*/ },
		Children: []Widget{

			RadioButtonGroupBox{
				AssignTo: &mode,
				Title:    "*Running mode",
				Layout:   HBox{},
				MinSize:  Size{0, 70},

				DataMember: "Mode",
				Buttons: []RadioButton{
					{Text: GuiOpt.Mode[0].Key, Value: GuiOpt.Mode[0].Int},
					{Text: GuiOpt.Mode[1].Key, Value: GuiOpt.Mode[1].Int},
					{Text: GuiOpt.Mode[2].Key, Value: GuiOpt.Mode[2].Int},
				},
			},

			VSplitter{
				AssignTo: &host,
				MaxSize:  Size{0, 120},
				Children: []Widget{
					Label{
						Text: "Distributed port: (Stand-alone mode is not filled)",
					},
					NumberEdit{
						Value:    Bind("Port"),
						Suffix:   "",
						Decimals: 0,
					},

					Label{
						Text: "Master node URL: (Required for client mode)",
					},
					LineEdit{
						Text: Bind("Master"),
					},
				},
			},

			PushButton{
				Text:     "Confirmation starts",
				MinSize:  Size{0, 30},
				AssignTo: &runStopBtn,
				OnClicked: func() {
					if err := db.Submit(); err != nil {
						logs.Log.Error("%v", err)
						return
					}

					switch Input.Mode {
					case status.OFFLINE:
						offlineWindow()

					case status.SERVER:
						serverWindow()

					case status.CLIENT:
						clientWindow()
					}

				},
			},
		},
	}.Create()); err != nil {
		panic(err)
	}

	if icon, err := walk.NewIconFromResourceId(3); err == nil {
		mw.SetIcon(icon)
	}
	// Run the form program
	mw.Run()
}
