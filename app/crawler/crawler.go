package crawler

import (
	"bytes"
	"math/rand"
	"runtime"
	"time"

	"gitlab.com/abibino-lab/mobal/app/downloader"
	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/app/pipeline"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

// Crawler is a acquisition engine
type (
	Crawler interface {
		Init(*spider.Spider) Crawler // Initialize the collection engine
		Run()                        // Run the task
		Stop()                       // Take the initiative to terminate
		CanStop() bool               // Can you stop
		GetID() int                  // Get the engine ID
	}
	crawler struct {
		*spider.Spider                 // Execute the collection rules
		downloader.Downloader          // Global public downloader
		pipeline.Pipeline              // Result collection and output pipeline
		id                    int      // Engine ID
		pause                 [2]int64 // The minimum length of the request interval, the length of the request interval
	}
)

// New is a function for create new crawler
func New(id int) Crawler {
	return &crawler{
		id:         id,
		Downloader: downloader.SurferDownloader,
	}
}

func (crawler *crawler) Init(sp *spider.Spider) Crawler {
	crawler.Spider = sp.ReqmatrixInit()
	crawler.Pipeline = pipeline.New(sp)
	crawler.pause[0] = cache.Task.Pausetime / 2
	if crawler.pause[0] > 0 {
		crawler.pause[1] = crawler.pause[0] * 3
	} else {
		crawler.pause[1] = 1
	}
	return crawler
}

// Run is a function for task execution entry
func (crawler *crawler) Run() {
	// Pre-start the data collection / output pipeline
	crawler.Pipeline.Start()

	// Run the processing
	c := make(chan bool)
	go func() {
		crawler.run()
		close(c)
	}()

	// Start the task
	crawler.Spider.Start()

	<-c // Wait for processing to exit

	// Stop the data collection / output pipeline
	crawler.Pipeline.Stop()
}

// Take the initiative to terminate
func (crawler *crawler) Stop() {
	// Active crash crawler runs the contract
	crawler.Spider.Stop()
	crawler.Pipeline.Stop()
}

func (crawler *crawler) run() {
	for {
		// Queue takes a request and handles it
		req := crawler.GetOne()
		if req == nil {
			// Stop the task
			if crawler.Spider.CanStop() {
				break
			}
			time.Sleep(20 * time.Millisecond)
			continue
		}

		// Execute the request
		crawler.UseOne()
		go func(req *request.Request) {
			defer func() {
				crawler.FreeOne()
			}()
			logs.Log.Debug("Start: %v", req.GetURL())
			crawler.Process(req)
		}(req)

		// Random wait
		crawler.sleep()
	}

	// Waiting for the task to be completed
	crawler.Spider.Defer()
}

// Process is a core processer for this crawler engine
func (crawler *crawler) Process(req *request.Request) {
	var (
		downloadURL = req.GetURL()
		sp          = crawler.Spider
	)
	defer func() {
		if p := recover(); p != nil {
			if sp.IsStopping() {
				// println("Process$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
				return
			}
			// Returns whether the request was added as a new failure to the end of the queue
			if sp.DoHistory(req, false) {
				// Statistical failure count
				cache.PageFailCount()
			}
			// Prompt error
			stack := make([]byte, 4<<10) //4KB
			length := runtime.Stack(stack, true)
			start := bytes.Index(stack, []byte("/src/runtime/panic.go"))
			stack = stack[start:length]
			start = bytes.Index(stack, []byte("\n")) + 1
			stack = stack[start:]
			if end := bytes.Index(stack, []byte("\ngoroutine ")); end != -1 {
				stack = stack[:end]
			}
			stack = bytes.Replace(stack, []byte("\n"), []byte("\r\n"), -1)
			logs.Log.Error("Panic  [process][%s]: %s\r\n[TRACE]\r\n%s", downloadURL, p, stack)
		}
	}()

	var ctx = crawler.Downloader.Download(sp, req) // download page

	if err := ctx.GetError(); err != nil {
		// Returns whether the request was added as a new failure to the end of the queue
		if sp.DoHistory(req, false) {
			// Statistical failure count
			cache.PageFailCount()
		}
		// Prompt error
		logs.Log.Error("Fail  [download][%v]: %v", downloadURL, err)
		return
	}

	// Processing, refining data
	ctx.Parse(req.GetRuleName())

	// The request file is stored in the pipeline
	for _, f := range ctx.PullFiles() {
		if crawler.Pipeline.CollectFile(f) != nil {
			break
		}
	}

	// The request text is stored in the pipeline
	for _, item := range ctx.PullItems() {
		if crawler.Pipeline.CollectData(item) != nil {
			break
		}
	}

	// Process request record successfully
	sp.DoHistory(req, true)

	// Count the number of successful pages
	cache.PageSuccCount()

	// Prompt to crawl success
	logs.Log.Informational("Success: %v", downloadURL)

	// Release ctx ready to reuse
	spider.PutContext(ctx)
}

// Commonly used basic methods
func (crawler *crawler) sleep() {
	sleeptime := crawler.pause[0] + rand.Int63n(crawler.pause[1])
	time.Sleep(time.Duration(sleeptime) * time.Millisecond)
}

// Read a request from a schedule
func (crawler *crawler) GetOne() *request.Request {
	return crawler.Spider.RequestPull()
}

// Use a resource space from a schedule
func (crawler *crawler) UseOne() {
	crawler.Spider.RequestUse()
}

//Release a resource space from the schedule
func (crawler *crawler) FreeOne() {
	crawler.Spider.RequestFree()
}

func (crawler *crawler) SetID(id int) {
	crawler.id = id
}

func (crawler *crawler) GetID() int {
	return crawler.id
}
