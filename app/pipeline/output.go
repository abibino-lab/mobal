package pipeline

import (
	"sort"

	"gitlab.com/abibino-lab/mobal/app/pipeline/collector"
	"gitlab.com/abibino-lab/mobal/common/kafka"
	"gitlab.com/abibino-lab/mobal/common/mgo"
	"gitlab.com/abibino-lab/mobal/common/mysql"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

// Initialize the output mode list collector.DataOutputLib
func init() {
	for out, _ := range collector.DataOutput {
		collector.DataOutputLib = append(collector.DataOutputLib, out)
	}
	sort.Strings(collector.DataOutputLib)
}

// Refresh the status of the output method
func RefreshOutput() {
	switch cache.Task.OutType {
	case "mgo":
		mgo.Refresh()
	case "mysql":
		mysql.Refresh()
	case "kafka":
		kafka.Refresh()
	}
}
