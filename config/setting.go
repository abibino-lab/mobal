package config

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/abibino-lab/mobal/common/config"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

// configuration file involves the default configuration.
const (
	crawlcap int = 50 // Spider pool maximum capacity
	// datachancap int = 2 << 14 // Collector capacity (default 65536)
	logcap                  int64  = 10000                                     // The size of the log cache
	loglevel                string = "debug"                                   // global log print level (also log file output level)
	logconsolelevel         string = "info"                                    // log at the display level of the console
	logfeedbacklevel        string = "error"                                   // The client feeds back to the server's log level
	loglineinfo             bool   = false                                     // whether the log prints line information
	logsave                 bool   = true                                      // Save all logs to local files
	phantomjs               string = WORK_ROOT + "/phantomjs"                  // phantomjs file path
	proxylib                string = WORK_ROOT + "/proxy.lib"                  // proxy ip file path
	spiderdir               string = WORK_ROOT + "/spiders"                    // Dynamic rules directory
	fileoutdir              string = WORK_ROOT + "/file_out"                   // file (image, HTML, etc.) the output directory of the result
	textoutdir              string = WORK_ROOT + "/text_out"                   // excel or csv output mode, the output of the text results directory
	dbname                  string = TAG                                       // Name database
	mgoconnstring           string = "mongodb://root:root@localhost:27017/mobal" // mongodb connection string
	esconnstring            string = "localhost:9200"                          // es connection string
	mgoconncap              int    = 1024                                      // mongodb connection pool capacity
	mgoconngcsecond         int64  = 600                                       // mongodb connection pool GC time in seconds
	mysqlconnstring         string = "root:@tcp(127.0.0.1:3306)"               // mysql connection string
	mysqlconncap            int    = 2048                                      // mysql connection pool capacity
	mysqlmaxallowedpacketmb int    = 1                                         // mysql The maximum length of the communication buffer, in MB, defaults to 1MB
	kafkabrokers            string = "127.0.0.1:9092"                          // kafka broker string, comma split

	mode           int    = status.UNSET  // node role
	port           int    = 2015          // The master node port
	master         string = "127.0.0.1"   // server (master node) address, no port
	thread         int    = 20            // global maximum concurrency
	pause          int64  = 300           // pause duration reference / ms (random: Pausetime / 2 ~ Pausetime * 2)
	outtype        string = "csv"         // output method
	dockercap      int    = 10000         // Subdivide container capacity
	limit          int64  = 0             // set the upper limit, 0 is not limited, if the rule is set to the initial value of Limit is a custom limit, otherwise the default limit request number
	proxyminute    int64  = 0             // The number of minutes of proxy IP replacement
	success        bool   = true          // inherit history success record
	failure        bool   = true          // inherit history failure record
	privatekeypath string = "key.rsa"     //
	publickeypath  string = "key.rsa.pub" //
	runmode        string = "prod"        //
)

var setting = func() config.Configer {
	os.MkdirAll(filepath.Clean(HISTORY_DIR), 0777)
	os.MkdirAll(filepath.Clean(CACHE_DIR), 0777)
	os.MkdirAll(filepath.Clean(PHANTOMJS_TEMP), 0777)

	iniconf, err := config.NewConfig("ini", CONFIG)
	if err != nil {
		file, err := os.Create(CONFIG)
		file.Close()
		iniconf, err = config.NewConfig("ini", CONFIG)
		if err != nil {
			panic(err)
		}
		defaultConfig(iniconf)
		iniconf.SaveConfigFile(CONFIG)
	} else {
		trySet(iniconf)
	}

	os.MkdirAll(filepath.Clean(iniconf.String("spiderdir")), 0777)
	os.MkdirAll(filepath.Clean(iniconf.String("fileoutdir")), 0777)
	os.MkdirAll(filepath.Clean(iniconf.String("textoutdir")), 0777)

	return iniconf
}()

func defaultConfig(iniconf config.Configer) {
	iniconf.Set("crawlcap", strconv.Itoa(crawlcap))
	// iniconf.Set ("datachancap", strconv.Itoa (datachancap))
	iniconf.Set("log::cap", strconv.FormatInt(logcap, 10))
	iniconf.Set("log::level", loglevel)
	iniconf.Set("log::consolelevel", logconsolelevel)
	iniconf.Set("log::feedbacklevel", logfeedbacklevel)
	iniconf.Set("log::lineinfo", fmt.Sprint(loglineinfo))
	iniconf.Set("log::save", fmt.Sprint(logsave))
	iniconf.Set("phantomjs", phantomjs)
	iniconf.Set("proxylib", proxylib)
	iniconf.Set("spiderdir", spiderdir)
	iniconf.Set("fileoutdir", fileoutdir)
	iniconf.Set("textoutdir", textoutdir)
	iniconf.Set("dbname", dbname)
	iniconf.Set("runmode", runmode)
	iniconf.Set("mgo::connstring", mgoconnstring)
	iniconf.Set("mgo::conncap", strconv.Itoa(mgoconncap))
	iniconf.Set("mgo::conngcsecond", strconv.FormatInt(mgoconngcsecond, 10))
	iniconf.Set("es::connstring", esconnstring)
	iniconf.Set("mysql::connstring", mysqlconnstring)
	iniconf.Set("mysql::conncap", strconv.Itoa(mysqlconncap))
	iniconf.Set("mysql::maxallowedpacketmb", strconv.Itoa(mysqlmaxallowedpacketmb))
	iniconf.Set("kafka::brokers", kafkabrokers)
	iniconf.Set("run::mode", strconv.Itoa(mode))
	iniconf.Set("run::port", strconv.Itoa(port))
	iniconf.Set("run::master", master)
	iniconf.Set("run::thread", strconv.Itoa(thread))
	iniconf.Set("run::pause", strconv.FormatInt(pause, 10))
	iniconf.Set("run::outtype", outtype)
	iniconf.Set("run::dockercap", strconv.Itoa(dockercap))
	iniconf.Set("run::limit", strconv.FormatInt(limit, 10))
	iniconf.Set("run::proxyminute", strconv.FormatInt(proxyminute, 10))
	iniconf.Set("run::success", fmt.Sprint(success))
	iniconf.Set("run::failure", fmt.Sprint(failure))
	iniconf.Set("api::privatekeypath", fmt.Sprint(privatekeypath))
	iniconf.Set("api::publickeypath", fmt.Sprint(publickeypath))
}

func trySet(iniconf config.Configer) {
	if v, e := iniconf.Int("crawlcap"); v <= 0 || e != nil {
		iniconf.Set("crawlcap", strconv.Itoa(crawlcap))
	}

	// if v, e: = iniconf.Int ("datachancap"); v <= 0 || e! = Nil {
	// iniconf.Set ("datachancap", strconv.Itoa (datachancap))
	//}

	if v, e := iniconf.Int64("log::cap"); v <= 0 || e != nil {
		iniconf.Set("log::cap", strconv.FormatInt(logcap, 10))
	}

	level := iniconf.String("log::level")
	if logLevel(level) == -10 {
		level = loglevel
	}
	iniconf.Set("log::level", level)

	consolelevel := iniconf.String("log::consolelevel")
	if logLevel(consolelevel) == -10 {
		consolelevel = logconsolelevel
	}
	iniconf.Set("log::consolelevel", logLevel2(consolelevel, level))

	feedbacklevel := iniconf.String("log::feedbacklevel")
	if logLevel(feedbacklevel) == -10 {
		feedbacklevel = logfeedbacklevel
	}
	iniconf.Set("log::feedbacklevel", logLevel2(feedbacklevel, level))

	if _, e := iniconf.Bool("log::lineinfo"); e != nil {
		iniconf.Set("log::lineinfo", fmt.Sprint(loglineinfo))
	}

	if _, e := iniconf.Bool("log::save"); e != nil {
		iniconf.Set("log::save", fmt.Sprint(logsave))
	}

	if v := iniconf.String("phantomjs"); v == "" {
		iniconf.Set("phantomjs", phantomjs)
	}

	if v := iniconf.String("proxylib"); v == "" {
		iniconf.Set("proxylib", proxylib)
	}

	if v := iniconf.String("spiderdir"); v == "" {
		iniconf.Set("spiderdir", spiderdir)
	}

	if v := iniconf.String("fileoutdir"); v == "" {
		iniconf.Set("fileoutdir", fileoutdir)
	}

	if v := iniconf.String("textoutdir"); v == "" {
		iniconf.Set("textoutdir", textoutdir)
	}

	if v := iniconf.String("dbname"); v == "" {
		iniconf.Set("dbname", dbname)
	}

	if v := iniconf.String("mgo::connstring"); v == "" {
		iniconf.Set("mgo::connstring", mgoconnstring)
	}

	if v, e := iniconf.Int("mgo::conncap"); v <= 0 || e != nil {
		iniconf.Set("mgo::conncap", strconv.Itoa(mgoconncap))
	}

	if v, e := iniconf.Int64("mgo::conngcsecond"); v <= 0 || e != nil {
		iniconf.Set("mgo::conngcsecond", strconv.FormatInt(mgoconngcsecond, 10))
	}

	if v := iniconf.String("mysql::connstring"); v == "" {
		iniconf.Set("mysql::connstring", mysqlconnstring)
	}

	if v, e := iniconf.Int("mysql::conncap"); v <= 0 || e != nil {
		iniconf.Set("mysql::conncap", strconv.Itoa(mysqlconncap))
	}

	if v, e := iniconf.Int("mysql::maxallowedpacketmb"); v <= 0 || e != nil {
		iniconf.Set("mysql::maxallowedpacketmb", strconv.Itoa(mysqlmaxallowedpacketmb))
	}

	if v := iniconf.String("kafka::brokers"); v == "" {
		iniconf.Set("kafka::brokers", kafkabrokers)
	}

	if v, e := iniconf.Int("run::mode"); v < status.UNSET || v > status.CLIENT || e != nil {
		iniconf.Set("run::mode", strconv.Itoa(mode))
	}

	if v, e := iniconf.Int("run::port"); v <= 0 || e != nil {
		iniconf.Set("run::port", strconv.Itoa(port))
	}

	if v := iniconf.String("run::master"); v == "" {
		iniconf.Set("run::master", master)
	}

	if v, e := iniconf.Int("run::thread"); v <= 0 || e != nil {
		iniconf.Set("run::thread", strconv.Itoa(thread))
	}

	if v, e := iniconf.Int64("run::pause"); v < 0 || e != nil {
		iniconf.Set("run::pause", strconv.FormatInt(pause, 10))
	}

	if v := iniconf.String("run::outtype"); v == "" {
		iniconf.Set("run::outtype", outtype)
	}

	if v, e := iniconf.Int("run::dockercap"); v <= 0 || e != nil {
		iniconf.Set("run::dockercap", strconv.Itoa(dockercap))
	}

	if v, e := iniconf.Int64("run::limit"); v < 0 || e != nil {
		iniconf.Set("run::limit", strconv.FormatInt(limit, 10))
	}

	if v, e := iniconf.Int64("run::proxyminute"); v <= 0 || e != nil {
		iniconf.Set("run::proxyminute", strconv.FormatInt(proxyminute, 10))
	}

	if _, e := iniconf.Bool("run::success"); e != nil {
		iniconf.Set("run::success", fmt.Sprint(success))
	}

	if _, e := iniconf.Bool("run::failure"); e != nil {
		iniconf.Set("run::failure", fmt.Sprint(failure))
	}

	if v := iniconf.String("api::privatekeypath"); v == "" {
		iniconf.Set("api::privatekeypath", fmt.Sprint(privatekeypath))
	}

	if v := iniconf.String("api::publickeypath"); v == "" {
		iniconf.Set("api::publickeypath", fmt.Sprint(publickeypath))
	}

	if v := iniconf.String("runmode"); v == "" {
		iniconf.Set("runmode", fmt.Sprint(runmode))
	}

	iniconf.SaveConfigFile(CONFIG)
}

func logLevel2(l string, g string) string {
	a, b := logLevel(l), logLevel(g)
	if a < b {
		return l
	}
	return g
}
