package mgo

import (
	"errors"
	"reflect"
	"strings"
)

// Create a unified method of checking and removing operations
// count operation resultPtr type is * int
// list operation The resultPtr type is * map [string] [] string
// find operation resultPtr type * map [string] interface {}
// insert operation resultPtr type is * [] string, allowed to nil (do not receive id list)
// The update operation resultPtr is nil
// remove operation resultPtr is nil
func Mgo(resultPtr interface{}, operate string, option map[string]interface{}) error {
	o := getOperator(operate)
	if o == nil {
		return errors.New("the db-operate " + operate + " does not exist!")
	}

	v := reflect.ValueOf(o).Elem()
	for key, val := range option {
		value := v.FieldByName(key)
		if value == (reflect.Value{}) || !value.CanSet() {
			continue
		}
		value.Set(reflect.ValueOf(val))
	}

	return o.Exec(resultPtr)
}

// add or delete operations
type Operator interface {
	Exec(resultPtr interface{}) (err error)
}

// Add or delete the action list
func getOperator(operate string) Operator {
	switch strings.ToLower(operate) {
	// incoming database list | return database and its collection tree
	case "list":
		return new(List)

	// incoming database and collection name | return the total number of documents
	case "count":
		return new(Count)

	// in the specified set of conditions for inquiries
	case "find":
		return new(Find)

	// insert new data
	case "insert":
		return new(Insert)

	// update the first matching data
	case "update":
		return new(Update)

	// Update all matching data
	case "update_all":
		return new(UpdateAll)

	// update the first matching data, if no match is inserted
	case "upsert":
		return new(Upsert)

	// delete data
	case "remove":
		return new(Remove)

	default:
		return nil
	}
}
