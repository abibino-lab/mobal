package spider

import (
	"fmt"

	"gitlab.com/abibino-lab/mobal/common/pinyin"
)

// List of spider species
type SpiderSpecies struct {
	list   []*Spider
	hash   map[string]*Spider
	sorted bool
}

// Examples of global spider species
var Species = &SpiderSpecies{
	list: []*Spider{},
	hash: map[string]*Spider{},
}

// Add a new category to the spider list
func (spiderSpecies *SpiderSpecies) Add(sp *Spider) *Spider {
	name := sp.Name
	for i := 2; true; i++ {
		if _, ok := spiderSpecies.hash[name]; !ok {
			sp.Name = name
			spiderSpecies.hash[sp.Name] = sp
			break
		}
		name = fmt.Sprintf("%s(%d)", sp.Name, i)
	}
	sp.Name = name
	spiderSpecies.list = append(spiderSpecies.list, sp)
	return sp
}

// Get all spider species
func (spiderSpecies *SpiderSpecies) Get() []*Spider {
	if !spiderSpecies.sorted {
		l := len(spiderSpecies.list)
		initials := make([]string, l)
		newlist := map[string]*Spider{}
		for i := 0; i < l; i++ {
			initials[i] = spiderSpecies.list[i].GetName()
			newlist[initials[i]] = spiderSpecies.list[i]
		}
		pinyin.SortInitials(initials)
		for i := 0; i < l; i++ {
			spiderSpecies.list[i] = newlist[initials[i]]
		}
		spiderSpecies.sorted = true
	}
	return spiderSpecies.list
}

func (spiderSpecies *SpiderSpecies) GetByName(name string) *Spider {
	return spiderSpecies.hash[name]
}
