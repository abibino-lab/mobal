package mgo

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/abibino-lab/mobal/common/pool"
)

// update the first matching data
type Update struct {
	Database   string                 // database
	Collection string                 // collection
	Selector   map[string]interface{} // document selector
	Change     map[string]interface{} // document update content
}

func (update *Update) Exec(_ interface{}) error {
	return Call(func(src pool.Src) error {
		c := src.(*MgoSrc).DB(update.Database).C(update.Collection)

		if id, ok := update.Selector["_id"]; ok {
			if idStr, ok2 := id.(string); !ok2 {
				return fmt.Errorf("%v", "Parameter \"_id\" must be a string type!")
			} else {
				update.Selector["_id"] = bson.ObjectIdHex(idStr)
			}
		}

		return c.Update(update.Selector, update.Change)
	})
}
