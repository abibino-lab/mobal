package models

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/renstrom/fuzzysearch/fuzzy"
	"github.com/sulthonzh/fuzzy/algorithm"
	"gitlab.com/abibino-lab/mobal/common/util"
	"gitlab.com/abibino-lab/mobal/web/api/middleware/myes"
	elastic "gopkg.in/olivere/elastic.v5"
)

// Init for Search
func (s *Search) Init() {
	if s.Paging == nil {
		s.Paging = map[string]int{
			"page":  1,
			"limit": 100,
		}
	} else {
		// max page limit
		if s.Paging["limit"] > 1000 {
			s.Paging["limit"] = 1000
		} else if s.Paging["limit"] < 100 {
			s.Paging["limit"] = 100
		}
	}
}

func (s *Search) getPageOffer() (ret int) {
	ret = (s.Paging["page"] - 1) * s.Paging["limit"]
	return
}

// FindSame :
func (s *Search) FindSame() (items Items, err error) {
	di := myes.DataIndex{}
	di.ConnectToES()
	defer di.Close("mobal-*")

	client := di.ESClient
	s.Query = strings.ToLower(s.Query)

	query := elastic.NewBoolQuery()

	q0 := elastic.NewSimpleQueryStringQuery(s.Query)
	q0.Analyzer("snowball")
	q0.Field("attributes.globalAttribute.keywords")
	q0.Field("_all")
	q0.DefaultOperator("and")
	// q0.Boost(4)

	q1 := elastic.NewMatchQuery("attributes.saleAttribute.price", s.Algorithm["price"])
	q2 := elastic.NewMatchQuery("attributes.spesification.mileage", s.Algorithm["mileage"])
	q3 := elastic.NewMatchQuery("attributes.spesification.year", s.Algorithm["year"])

	query.Should(q0)
	query.Must(q1)
	query.Must(q2)
	query.Must(q3)

	eQuery := client.Search().
		Index("mobal-*").
		Query(query)

	searchResult, err := eQuery.Pretty(true).Do(context.Background())

	if err != nil {
		return
	}

	// Here's how you iterate through results with full control over each step.
	if searchResult.Hits.TotalHits > 0 {

		for _, item := range searchResult.Hits.Hits {

			// hit.Index contains the name of the index
			var tempItem map[string]interface{}
			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			err := json.Unmarshal(*item.Source, &tempItem)
			if err != nil {
				// Deserialization failed
				continue
			}

			// Add new item
			items = append(items, tempItem)
		}
	}

	return
}

// Find is a func for ...
func (s *Search) Find() (result map[string]interface{}, err error) {
	di := myes.DataIndex{}
	di.ConnectToES()
	defer di.Close("mobal-*")

	client := di.ESClient
	s.Query = strings.ToLower(s.Query)

	query := elastic.NewBoolQuery()

	// q0 := elastic.NewMatchQuery("attributes.globalAttribute.categories", s.Filter["categories"])
	// q0.Boost(4)

	q1 := elastic.NewSimpleQueryStringQuery(s.Query)
	q1.Analyzer("snowball")
	q1.Field("attributes.globalAttribute.keywords")
	q1.Field("_all")
	q1.DefaultOperator("and")

	// q1.Boost(1)

	// query.Must(q0)
	query.Should(q1)

	eQuery := client.Search().
		Index("mobal-*").
		Query(query).
		From(s.getPageOffer()).Size(s.Paging["limit"]) // take documents 0-9

	searchResult, err := eQuery.Pretty(true).Do(context.Background())

	if err != nil {
		return
	}

	items := Items{}

	mape := 0.0
	minYear := float64(-1)
	maxYear := float64(-1)
	minMileage := float64(-1)
	maxMileage := float64(-1)
	minPrice := float64(-1)
	maxPrice := float64(-1)
	prices := []float64{}
	fuzValid := 0

	// Here's how you iterate through results with full control over each step.
	if searchResult.Hits.TotalHits > 0 {

		for _, item := range searchResult.Hits.Hits {

			// hit.Index contains the name of the index
			var tempItem map[string]interface{}
			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			err := json.Unmarshal(*item.Source, &tempItem)
			if err != nil {
				// Deserialization failed
				continue
			}

			tempItem["score_default"] = *item.Score
			tempItem["score_matching"] = float64(fuzzy.RankMatch(s.Query, strings.ToLower(tempItem["attributes"].(map[string]interface{})["globalAttribute"].(map[string]interface{})["keywords"].(string))))

			if tempItem["score_matching"].(float64) >= 0 {
				attr := tempItem["attributes"]
				// Check existing attributes
				if attr != nil {

					tempAttr := attr.(map[string]interface{})
					saleAttr := tempAttr["saleAttribute"].(map[string]interface{})
					spec := tempAttr["spesification"].(map[string]interface{})

					// Check data train
					if len(prices) > 1 {
						// Check valid price
						if ok := s.isValidPrice(prices, saleAttr["price"].(float64)); !ok {
							// If not ok, than set score -1
							tempItem["score_matching"] = float64(-1)
						} else {
							// Add new data train
							prices = append(prices, saleAttr["price"].(float64))
						}
					} else {
						// Add new data train
						prices = append(prices, saleAttr["price"].(float64))
					}

					if tempItem["score_matching"].(float64) >= 0 {
						d := 0.0
						if spec["year"] != nil {
							d = s.diff(spec["year"])
						} else if tempItem["postedAt"] != nil {
							d = s.diff(tempItem["postedAt"])
						}

						if d == 0 {
							tempItem["score_matching"] = float64(-1)
						} else {

							minYearTemp, okMinYear := s.min(0, 20, minYear, d)
							minMileageTemp, okMinMileage := s.min(1000, 200000, minMileage, spec["mileage"].(float64))
							minPriceTemp, okMinPrice := s.min(1500000, 1000000000, minPrice, saleAttr["price"].(float64))
							maxYearTemp, okMaxYear := s.max(0, 20, maxYear, d)
							maxMileageTemp, okMaxMileage := s.max(1000, 200000, maxMileage, spec["mileage"].(float64))
							maxPriceTemp, okMaxPrice := s.max(1500000, 1000000000, maxPrice, saleAttr["price"].(float64))

							if okMinYear && okMinMileage && okMinPrice && okMaxYear && okMaxMileage && okMaxPrice {
								minYear = minYearTemp
								minMileage = minMileageTemp
								minPrice = minPriceTemp
								maxYear = maxYearTemp
								maxMileage = maxMileageTemp
								maxPrice = maxPriceTemp
							} else {
								tempItem["score_matching"] = float64(-1)
							}
						}
					}
				}
			}

			if tempItem["score_matching"].(float64) >= 0 {
				fuzValid++
			}

			// Add new item
			items = append(items, tempItem)
		}

		mape, items = s.checkAdvanced(
			map[string]float64{
				"minPrice":   minPrice,
				"maxPrice":   maxPrice,
				"minYear":    minYear,
				"maxYear":    maxYear,
				"minMileage": minMileage,
				"maxMileage": maxMileage,
			},
			items)

	}

	// Hitung tsukamoto
	f := algorithm.Tsukamoto{}
	tr := make([]float64, 2)

	if s.Algorithm["year"] != "" && s.Algorithm["mileage"] != "" && s.Algorithm["tolerance"] != "" {

		if fuzValid > 0 {
			y, _ := strconv.ParseFloat(s.Algorithm["year"], 64)
			d := s.diff(y)
			m, _ := strconv.ParseFloat(s.Algorithm["mileage"], 64)
			f, err = s.tsukamoto(m, d, minMileage, maxMileage, minYear, maxYear, minPrice, maxPrice)

			// fmt.Print(m, "=", d, "=", minMileage, "=", maxMileage, "=", minYear, "=", maxYear, "=", minPrice, "=", maxPrice)
			if err == nil {
				data := map[string]float64{
					"Kilometer": m,
					"Tahun":     d,
				}

				f.Calc(data)
				t := 0.0
				if mape > 0 {
					t = mape
				} else {
					t, _ = strconv.ParseFloat(s.Algorithm["tolerance"], 64)
				}

				if t > 0 && f.Output.Data > 0 {
					tr[0] = f.Output.Data - (f.Output.Data * t / 100 / 2)
					tr[1] = f.Output.Data + (f.Output.Data * t / 100 / 2)
				}

				sort.Sort(items)
			}
		}
	}

	// graphInputs := []map[string]interface{}{}
	// for i, input := range f.Inputs {
	// 	cPoint := len(input.Domains) + 2
	// 	vPoint := (input.Min + input.Max) / float64(cPoint)

	// 	xAxis := []float64{}
	// 	for i := 1; i <= cPoint; i++ {
	// 		xAxis = append(xAxis, vPoint*float64(i))
	// 	}
	// 	lines := map[string]string{}

	// }

	result = map[string]interface{}{
		"error":       searchResult.Error,
		"suggest":     searchResult.Suggest,
		"isTimeout":   searchResult.TimedOut,
		"scrollId":    searchResult.ScrollId,
		"took":        searchResult.TookInMillis,
		"totalSearch": searchResult.TotalHits(),
		"totalItems":  len(items),
		"items":       items,
		"page":        s.Paging["page"],
		"limit":       s.Paging["limit"],
		"algorithm": map[string]interface{}{

			// "graph": map[string]interface{}{
			// 	"inputs": graphInputs,
			// },
			"mape":       fmt.Sprintf("%.2f %s", mape, "%"),
			"tsukamoto":  f,
			"params":     s,
			"dataSample": fuzValid,
			"toleran": map[string]interface{}{
				"valid": (tr[0] != tr[1]),
				"range": tr,
			},
			"error": err,
		},
	}
	return result, nil
}

func (s *Search) checkAdvanced(minmax map[string]float64, items []map[string]interface{}) (mape float64, res []map[string]interface{}) {
	res = items
	fmt.Printf("\ns_ori1:%v\n", util.JsonString(s))

	sumApe := 0.0
	cApe := 0.0
	for i, item := range items {
		item["isValidTest"] = false
		// Check valid test

		price := item["attributes"].(map[string]interface{})["saleAttribute"].(map[string]interface{})["price"].(float64)
		mileage := item["attributes"].(map[string]interface{})["spesification"].(map[string]interface{})["mileage"].(float64)
		yearTemp := item["attributes"].(map[string]interface{})["spesification"].(map[string]interface{})["year"]
		year := 0.0
		if yearTemp != nil {
			year = s.diff(yearTemp)
		} else if item["postedAt"] != nil {
			year = s.diff(item["postedAt"])
		}

		if price > minmax["minPrice"] && price < minmax["maxPrice"] && year > minmax["minYear"] && year < minmax["maxYear"] && mileage > minmax["minMileage"] && mileage < minmax["maxMileage"] && item["score_matching"].(float64) >= 0 {
			item["isValidTest"] = true

			if s.Algorithm["isTest"] == "yes" {

				searchTemp := Search{
					Fields: s.Fields,
					Filter: s.Filter,
					Query:  s.Query,
					Paging: s.Paging,
					Sort:   s.Sort,
					Algorithm: map[string]string{
						"isTest":    "no",
						"tolerance": "0",
						"year":      fmt.Sprintf("%v", yearTemp),
						"mileage":   fmt.Sprintf("%v", mileage),
					},
				}

				// fmt.Printf("\ns_ori2:%v\n", util.JsonString(s))
				// fmt.Printf("\ns:%v\n", util.JsonString(searchTemp))
				resFuzz, err := searchTemp.Find()
				// fmt.Printf("\nfuzzy:%v\n", util.JsonString(resFuzz["algorithm"].(map[string]interface{})["tsukamoto"].(algorithm.Tsukamoto).Output.Data))
				if err == nil {
					item["estimatePrice"] = resFuzz["algorithm"].(map[string]interface{})["tsukamoto"].(algorithm.Tsukamoto).Output.Data
				}

				item["ape"] = math.Abs(((price - item["estimatePrice"].(float64)) / price) * 100)
				sumApe += item["ape"].(float64)
				cApe++
				// println("Estimasi(", price, "):", util.JsonString(item["estimatePrice"]), "\n")
				// resSame, err := searchTemp.FindSame()
				// if err == nil {
				// 	println(resSame[0]["attributes"].(map[string]interface{})["saleAttribute"].(map[string]interface{})["price"].(float64))
				// }
			}
		}

		res[i] = item
	}

	mape = sumApe / cApe
	return
}

func (s *Search) isValidPrice(data []float64, price float64) (ok bool) {
	avg := average(data)
	tol := avg * 50.0 / 100.0
	if (price >= avg-tol) && (price <= avg+tol) {
		ok = true
	}
	fmt.Println(avg - tol)
	fmt.Println(avg + tol)
	return
}

func (s *Search) monthYearDiff(a, b time.Time) (years, months int) {
	m := a.Month()
	for a.Before(b) {
		a = a.Add(time.Hour * 24)
		m2 := a.Month()
		if m2 != m {
			months++
		}
		m = m2
	}
	years = months / 12
	months = months % 12
	return
}

func average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

func (s *Search) diff(year interface{}) (d float64) {
	t, _ := time.Parse("2006-01-02", fmt.Sprintf("%.0f", year)+"-01-01")
	y, m := s.monthYearDiff(t, time.Now())
	d, _ = strconv.ParseFloat(fmt.Sprintf("%v.%v", y, m), 64)
	return
}

func (s *Search) min(min, max, c, i float64) (r float64, ok bool) {
	r = c

	if r >= 0 {
		if i >= min && i <= max {
			if i < r {
				r = i
			}
			ok = true
		}
	} else {
		if i >= min && i <= max {
			r = i
			ok = true
		} else {
			r = float64(-1)
		}
	}

	return
}

func (s *Search) max(min, max, c, i float64) (r float64, ok bool) {
	r = c

	if r >= 0 {
		if i >= min && i <= max {
			if i > r {
				r = i

			}
			ok = true
		}
	} else {
		if i >= min && i <= max {
			r = i
			ok = true
		} else {
			r = float64(-1)
		}
	}
	return
}

func (s *Search) tsukamoto(inputMilage, inputYear, minMilage, maxMilage, minYear, maxYear, minPrice, maxPrice float64) (f algorithm.Tsukamoto, err error) {

	if inputMilage < 1000 || inputMilage > 200000 {
		return f, errors.New("Input Milage must (greater than equal 1000) or (less than equal 200000)")
	}
	if inputYear < 0 || inputYear > 20 {
		return f, errors.New("Input year must (greater than equal 0) or (less than equal 20)")
	}
	// Check -1
	if minPrice < 0 || maxPrice < 0 {
		return f, errors.New("Min or max price must greater than equal 0")
	}

	if minMilage < 0 || maxMilage < 0 {
		return f, errors.New("Min or max milage must greater than equal 0")
	}
	if minYear < 0 || maxYear < 0 {
		return f, errors.New("Min or max year must greater than equal 0")
	}

	// Check min == max
	if minPrice == maxPrice {
		return f, errors.New("Min and max price must be a difference")
	}

	if minMilage == maxMilage {
		return f, errors.New("Min and max milage must be a difference")
	}

	if minYear == maxYear {
		return f, errors.New("Min and max year must be a difference")
	}

	// Memenuhi syarat
	f = algorithm.Tsukamoto{

		Inputs: []algorithm.Variable{
			algorithm.Variable{
				Denomination: "Km",
				Name:         "Kilometer",
				Mode:         1,
				Min:          minMilage,
				Max:          maxMilage,
				Domains:      []string{"Dekat", "Sedang", "Jauh"},
			},
			algorithm.Variable{
				Denomination: "Yo",
				Name:         "Tahun",
				Mode:         1,
				Min:          minYear,
				Max:          maxYear,
				Domains:      []string{"Baru", "Sedang", "Lama"},
			},
		},
		Rules: []string{
			"Kilometer_Jauh {{And}} Tahun_Baru == Harga_Murah ",
			"Kilometer_Jauh {{And}} Tahun_Sedang == Harga_Murah",
			"Kilometer_Jauh {{And}} Tahun_Lama == Harga_Murah",
			"Kilometer_Sedang {{And}} Tahun_Baru == Harga_Sedang",
			"Kilometer_Sedang {{And}} Tahun_Sedang == Harga_Sedang",
			"Kilometer_Sedang {{And}} Tahun_Lama == Harga_Murah",
			"Kilometer_Dekat {{And}} Tahun_Baru == Harga_Mahal",
			"Kilometer_Dekat {{And}} Tahun_Sedang == Harga_Sedang",
			"Kilometer_Dekat {{And}} Tahun_Lama == Harga_Murah",
		},
		Output: algorithm.Variable{
			Name:    "Harga",
			Mode:    1,
			Min:     minPrice,
			Max:     maxPrice,
			Domains: []string{"Murah", "Sedang", "Mahal"},
		},
	}
	return f, nil
}
