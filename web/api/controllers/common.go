package controllers

import "gitlab.com/abibino-lab/mobal/common/session"

// GlobalSessions is a variable for store session in this system
var GlobalSessions *session.Manager

func init() {
	GlobalSessions, _ = session.NewManager(`memory`, `{"cookieName":"mobalSession", "enableSetCookie,omitempty": true, "secure": false, "sessionIDHashFunc": "sha1", "sessionIDHashKey": "", "cookieLifeTime": 157680000, "providerConfig": ""}`)
	//go GlobalSessions.GC()
}

// // handle web page requests
// func web(c echo.Context) (err error) {
// 	// sess, _ := globalSessions.SessionStart(c.Response.ResponseWriter, c.Request)
// 	// defer sess.SessionRelease(rw)
// 	// index, _ := viewsIndexHtmlBytes()
// 	// t, err := template.New("index").Parse(string(index)) // parse the template file
// 	// t, err: = template.ParseFiles ("web / views / index.html") // parse the template file

// 	t, err := template.New("index.html").ParseFiles("web/views/index.html") // parse the template file
// 	if err != nil {
// 		logs.Log.Error("%v", err)
// 	}
// 	// Get mobal information
// 	data := map[string]interface{}{
// 		"title":   config.NAME,
// 		"logo":    config.ICON_PNG,
// 		"version": config.VERSION,
// 		"author":  config.AUTHOR,
// 		"mode": map[string]int{
// 			"offline": status.OFFLINE,
// 			"server":  status.SERVER,
// 			"client":  status.CLIENT,
// 			"unset":   status.UNSET,
// 			"curr":    app.LogicApp.GetAppConf("mode").(int),
// 		},
// 		"status": map[string]int{
// 			"stopped": status.STOPPED,
// 			"stop":    status.STOP,
// 			"run":     status.RUN,
// 			"pause":   status.PAUSE,
// 		},
// 		"port": app.LogicApp.GetAppConf("port").(int),
// 		"ip":   app.LogicApp.GetAppConf("master").(string),
// 	}
// 	t.Execute(rw, data) // Execute the copy of the template
// }

// func importFromJSON(rw http.ResponseWriter, req *http.Request) {
// 	sess, _ := globalSessions.SessionStart(rw, req)
// 	defer sess.SessionRelease(rw)
// 	print("-----")
// 	importProcess()
// }

// func importCategoriesFromJSON(rw http.ResponseWriter, req *http.Request) {
// 	sess, _ := globalSessions.SessionStart(rw, req)
// 	defer sess.SessionRelease(rw)
// 	print("-----")
// 	importCategories()
// }

// func importLocationFromJSON(rw http.ResponseWriter, req *http.Request) {
// 	sess, _ := globalSessions.SessionStart(rw, req)
// 	defer sess.SessionRelease(rw)
// 	print("-----")
// 	importLocation()
// }

// func importProcess() {
// 	raw, err := ioutil.ReadFile("mobal_pkg/items.json")
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		os.Exit(1)
// 	}

// 	var c []models.Item
// 	err = json.Unmarshal(raw, &c)
// 	var code int

// 	// The request text is stored in the pipeline
// 	for _, item := range c {
// 		item.New()
// 		code, err = item.Insert()
// 		//fmt.Println("name:", item.Name, "--> code:", code, "--> err:", err)
// 		fmt.Print(code)
// 	}
// }

// func importCategories() {
// 	raw, err := ioutil.ReadFile("mobal_pkg/categories.json")
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		os.Exit(1)
// 	}

// 	var c []models.Category
// 	err = json.Unmarshal(raw, &c)
// 	var code int

// 	// The request text is stored in the pipeline
// 	for _, cat := range c {
// 		cat.New()
// 		code, err = cat.Insert()
// 		//fmt.Println("name:", item.Name, "--> code:", code, "--> err:", err)
// 		fmt.Print(code)
// 	}
// }

// func importLocation() {
// 	raw, err := ioutil.ReadFile("mobal_pkg/locations.json")
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		os.Exit(1)
// 	}

// 	var c []models.Location
// 	err = json.Unmarshal(raw, &c)
// 	var code int

// 	// The request text is stored in the pipeline
// 	for _, loc := range c {
// 		loc.New()
// 		code, err = loc.Insert()
// 		//fmt.Println("name:", item.Name, "--> code:", code, "--> err:", err)
// 		fmt.Print(code)
// 	}
// }
