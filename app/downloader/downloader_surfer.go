package downloader

import (
	"errors"
	"net/http"

	"gitlab.com/abibino-lab/mobal/app/downloader/request"
	"gitlab.com/abibino-lab/mobal/app/downloader/surfer"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/config"
)

// Surfer is a struct ...
type Surfer struct {
	surf    surfer.Surfer
	phantom surfer.Surfer
}

// SurferDownloader is a struct ...
var SurferDownloader = &Surfer{
	surf:    surfer.New(),
	phantom: surfer.NewPhantom(config.PHANTOMJS, config.PHANTOMJS_TEMP),
}

// Download is a struct ...
func (surfer *Surfer) Download(sp *spider.Spider, cReq *request.Request) *spider.Context {
	ctx := spider.GetContext(sp, cReq)

	var resp *http.Response
	var err error

	switch cReq.GetDownloaderID() {
	case request.SurfID:
		resp, err = surfer.surf.Download(cReq)

	case request.PhantomJSID:
		resp, err = surfer.phantom.Download(cReq)
	}

	if resp.StatusCode >= 400 {
		err = errors.New("Response state " + resp.Status)
	}

	ctx.SetResponse(resp).SetError(err)

	return ctx
}
