// result collection and output
package collector

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/abibino-lab/mobal/app/pipeline/collector/data"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
)

// Collector is a result collection and output
type Collector struct {
	*spider.Spider                    // Bind the collection rules
	DataChan       chan data.DataCell // text data collection channel
	FileChan       chan data.FileCell // file collection channel
	dataDocker     []data.DataCell    // Output the result cache in batches
	outType        string             //output method
	// size [2] uint64 // data total output traffic statistics [text, file], text temporarily not counted
	dataBatch   uint64 // current text output batch
	fileBatch   uint64 // The current file is output batch
	wait        sync.WaitGroup
	sum         [4]uint64 // Total number of data collected [total number of texts after the last output, total number of texts after this output, total number of files after the last output, total number of files after this output], non-concurrent security
	dataSumLock sync.RWMutex
	fileSumLock sync.RWMutex
}

func NewCollector(sp *spider.Spider) *Collector {
	var collector = &Collector{}
	collector.Spider = sp
	collector.outType = cache.Task.OutType
	if cache.Task.DockerCap < 1 {
		cache.Task.DockerCap = 1
	}
	collector.DataChan = make(chan data.DataCell, cache.Task.DockerCap)
	collector.FileChan = make(chan data.FileCell, cache.Task.DockerCap)
	collector.dataDocker = make([]data.DataCell, 0, cache.Task.DockerCap)
	collector.sum = [4]uint64{}
	// collector.size = [2] uint64 {}
	collector.dataBatch = 0
	collector.fileBatch = 0
	return collector
}

func (collector *Collector) CollectData(dataCell data.DataCell) error {
	var err error
	defer func() {
		if recover() != nil {
			err = fmt.Errorf("The output protocol has been terminated")
		}
	}()
	collector.DataChan <- dataCell
	return err
}

func (collector *Collector) CollectFile(fileCell data.FileCell) error {
	var err error
	defer func() {
		if recover() != nil {
			err = fmt.Errorf("The output protocol has been terminated")
		}
	}()
	collector.FileChan <- fileCell
	return err
}

// stop
func (collector *Collector) Stop() {
	go func() {
		defer func() {
			recover()
		}()
		close(collector.DataChan)
	}()
	go func() {
		defer func() {
			recover()
		}()
		close(collector.FileChan)
	}()
}

// Start the data collection / output pipeline
func (collector *Collector) Start() {
	// start the output protocol
	go func() {
		dataStop := make(chan bool)
		fileStop := make(chan bool)

		go func() {
			defer func() {
				recover()
				// println ("DataChanStop $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
			}()
			for data := range collector.DataChan {
				// cache the batch data
				collector.dataDocker = append(collector.dataDocker, data)

				// Continue to collect data when the set batches are not reached
				if len(collector.dataDocker) < cache.Task.DockerCap {
					continue
				}

				// Execute the output
				collector.dataBatch++
				collector.outputData()
			}
			// The remaining data is collected but not output
			collector.dataBatch++
			collector.outputData()
			close(dataStop)
		}()

		go func() {
			defer func() {
				recover()
				// println ("FileChanStop $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
			}()
			// Only when the exit notification is received and there is no data in the channel, the loop is exited
			for file := range collector.FileChan {
				atomic.AddUint64(&collector.fileBatch, 1)
				collector.wait.Add(1)
				go collector.outputFile(file)
			}
			close(fileStop)
		}()

		<-dataStop
		<-fileStop
		// println ("OutputWaitStopping +++++++++++++++++++++++++++++++")

		// wait for all output to finish
		collector.wait.Wait()
		// println ("OutputStopped $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")

		// return report
		collector.Report()
	}()
}

func (collector *Collector) resetDataDocker() {
	for _, cell := range collector.dataDocker {
		data.PutDataCell(cell)
	}
	collector.dataDocker = collector.dataDocker[:0]
}

// Get the total amount of text data
func (collector *Collector) dataSum() uint64 {
	collector.dataSumLock.RLock()
	defer collector.dataSumLock.RUnlock()
	return collector.sum[1]
}

// update the total amount of text data
func (collector *Collector) addDataSum(add uint64) {
	collector.dataSumLock.Lock()
	defer collector.dataSumLock.Unlock()
	collector.sum[0] = collector.sum[1]
	collector.sum[1] += add
}

// Get the total amount of file data
func (collector *Collector) fileSum() uint64 {
	collector.fileSumLock.RLock()
	defer collector.fileSumLock.RUnlock()
	return collector.sum[3]
}

// update the total amount of file data
func (collector *Collector) addFileSum(add uint64) {
	collector.fileSumLock.Lock()
	defer collector.fileSumLock.Unlock()
	collector.sum[2] = collector.sum[3]
	collector.sum[3] += add
}

// Get the text output flow
// func (collector * Collector) dataSize () uint64 {
// return collector.size [0]
//}

// update the text output flow record
// func (collector * Collector) addDataSize (add uint64) {
// collector.size [0] + = add
//}

// Get the file output traffic
// func (collector * Collector) fileSize () uint64 {
// return collector.size [1]
//}

// update the text output flow record
// func (collector * Collector) addFileSize (add uint64) {
// collector.size [1] + = add
//}

// return the report
func (collector *Collector) Report() {
	cache.ReportChan <- &cache.Report{
		SpiderName: collector.Spider.GetName(),
		KeyIn:      collector.GetKeyIn(),
		DataNum:    collector.dataSum(),
		FileNum:    collector.fileSum(),
		// DataSize: collector.dataSize (),
		// FileSize: collector.fileSize (),
		Time: time.Since(cache.StartTime),
	}
}
