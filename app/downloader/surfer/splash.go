package surfer

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/abibino-lab/mobal/logs"
)

type (
	//Splash is struct for represent splash API
	Splash struct {
		SplashServer     string            // Splash Server host and port
		TempLuaScriptDir string            // Temporary lua script storage directory
		luaScriptFileMap map[string]string // Already existing lua script file
	}
)

// NewSplash is a funtion for create downloader via splash API
func NewSplash(splashServer, tempLuaScriptDir string) Surfer {
	splash := Splash{
		SplashServer:     splashServer,
		TempLuaScriptDir: tempLuaScriptDir,
		luaScriptFileMap: make(map[string]string),
	}

	if !filepath.IsAbs(splash.TempLuaScriptDir) {
		splash.TempLuaScriptDir, _ = filepath.Abs(splash.TempLuaScriptDir)
	}

	// create / open the directory
	err := os.MkdirAll(splash.TempLuaScriptDir, 0777)
	if err != nil {
		logs.Log.Error("Surfer: %v", err)
		return nil
	}
	splash.createLuaScriptFile("lua", lua)
	return nil
}

// Download is a function for implement the surfer downloader interface
func (splash *Splash) Download(req Request) (resp *http.Response, err error) {

	req.GetHeader().Del("Content-Type")

	param, err := NewParam(req)
	if err != nil {
		return nil, err
	}
	resp = param.writeback(resp)

	url := fmt.Sprintf(splash.SplashServer)

	// Build the request
	req1, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	resp, err = client.Do(req1)
	if err != nil {
		return nil, err
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()

	if err == nil {
		resp.StatusCode = http.StatusOK
		resp.Status = http.StatusText(http.StatusOK)
	} else {
		resp.StatusCode = http.StatusBadGateway
		resp.Status = http.StatusText(http.StatusBadGateway)
	}
	return
}

// DestroyLuaScriptFiles is a funtion for destroy js temporary files
func (splash *Splash) DestroyLuaScriptFiles() {
	p, _ := filepath.Split(splash.TempLuaScriptDir)
	if p == "" {
		return
	}
	for _, filename := range splash.luaScriptFileMap {
		os.Remove(filename)
	}
	if len(WalkDir(p)) == 1 {
		os.Remove(p)
	}
}

func (splash *Splash) createLuaScriptFile(fileName, luaScript string) {
	fullFileName := filepath.Join(splash.TempLuaScriptDir, fileName)
	// Create and write files
	f, _ := os.Create(fullFileName)
	f.Write([]byte(luaScript))
	f.Close()
	splash.luaScriptFileMap[fileName] = fullFileName
}

const lua string = ``
