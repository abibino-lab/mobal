// [spider frame (golang)] Mobal (Phantom Spider) is a pure Go language prepared by the high-level, distributed, heavyweight reptile software, support stand-alone, server, client three operating modes, with Web, GUI (Mysql / mongodb / csv / excel, etc.), a large number of Demo sharing; at the same time she also supports horizontal and vertical two crawling mode, support for simulated login (mysql / mongodb / csv / excel, etc.), a large number of Demo shared; And task suspension, cancellation and a series of advanced features;
// (official QQ group: Go large data 42731170, welcome to join our discussion).
// command line interface version.
package cmd

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/abibino-lab/mobal/app"
	"gitlab.com/abibino-lab/mobal/app/spider"
	"gitlab.com/abibino-lab/mobal/logs"
	"gitlab.com/abibino-lab/mobal/runtime/cache"
	"gitlab.com/abibino-lab/mobal/runtime/status"
)

var (
	spiderflag *string
)

// Get external parameters
func Flag() {
	// classification instructions
	flag.String("c ******************************************** Only for cmd ******************************************** -c", "", "")

	// spider list
	spiderflag = flag.String(
		"c_spider",
		"",
		func() string {
			var spiderlist string
			for k, v := range app.LogicApp.GetSpiderLib() {
				spiderlist += "   [" + strconv.Itoa(k) + "] " + v.GetName() + "  " + v.GetDescription() + "\r\n"
			}
			return "   <Spider list: choose multiple spiders with \",\" interval> \r \n" + spiderlist
		}())

	// instruction manual
	flag.String(
		"c_z",
		"",
		"CMD-EXAMPLE: $ mobal -_ui=cmd -a_mode="+strconv.Itoa(status.OFFLINE)+" -c_spider=3,8 -a_outtype=csv -a_thread=20 -a_dockercap=5000 -a_pause=300 -a_proxyminute=0 -a_keyins=\"<af><golang>\" -a_limit=10 -a_success=true -a_failure=true\n",
	)
}

// execute the entry
func Run() {
	app.LogicApp.Init(cache.Task.Mode, cache.Task.Port, cache.Task.Master)
	if cache.Task.Mode == status.UNSET {
		return
	}
	switch app.LogicApp.GetAppConf("Mode").(int) {
	case status.SERVER:
		for {
			parseInput()
			run()
		}
	case status.CLIENT:
		run()
		select {}
	default:
		run()
	}
}

// run
func run() {
	// create a spider queue
	sps := []*spider.Spider{}
	*spiderflag = strings.TrimSpace(*spiderflag)
	if *spiderflag == "*" {
		sps = app.LogicApp.GetSpiderLib()

	} else {
		for _, idx := range strings.Split(*spiderflag, ",") {
			idx = strings.TrimSpace(idx)
			if idx == "" {
				continue
			}
			i, _ := strconv.Atoi(idx)
			sps = append(sps, app.LogicApp.GetSpiderLib()[i])
		}
	}

	app.LogicApp.SpiderPrepare(sps).Run()
}

// Receive the parameters of the add task in server mode
func parseInput() {
	logs.Log.Informational("\nAdd task parameters - Required: %v \n Add task parameters - Required: %v ", " -c_spider", "-c_spider", []string{
		"-a_keyins",
		"-a_limit",
		"-a_outtype",
		"-a_thread",
		"-a_pause",
		"-a_proxyminute",
		"-a_dockercap",
		"-a_success",
		"-a_failure"})
	logs.Log.Informational("\nAdd a task:")
retry:
	*spiderflag = ""
	input := [12]string{}
	fmt.Scanln(&input[0], &input[1], &input[2], &input[3], &input[4], &input[5], &input[6], &input[7], &input[8], &input[9])
	if strings.Index(input[0], "=") < 4 {
		logs.Log.Informational("\nThe parameters for the add task are incorrect. Please re-enter:")
		goto retry
	}
	for _, v := range input {
		i := strings.Index(v, "=")
		if i < 4 {
			continue
		}
		key, value := v[:i], v[i+1:]
		switch key {
		case "-a_keyins":
			cache.Task.KeyIns = value
		case "-a_limit":
			limit, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				break
			}
			cache.Task.Limit = limit
		case "-a_outtype":
			cache.Task.OutType = value
		case "-a_thread":
			thread, err := strconv.Atoi(value)
			if err != nil {
				break
			}
			cache.Task.ThreadNum = thread
		case "-a_pause":
			pause, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				break
			}
			cache.Task.Pausetime = pause
		case "-a_proxyminute":
			proxyminute, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				break
			}
			cache.Task.ProxyMinute = proxyminute
		case "-a_dockercap":
			dockercap, err := strconv.Atoi(value)
			if err != nil {
				break
			}
			if dockercap < 1 {
				dockercap = 1
			}
			cache.Task.DockerCap = dockercap
		case "-a_success":
			if value == "true" {
				cache.Task.SuccessInherit = true
			} else if value == "false" {
				cache.Task.SuccessInherit = false
			}
		case "-a_failure":
			if value == "true" {
				cache.Task.FailureInherit = true
			} else if value == "false" {
				cache.Task.FailureInherit = false
			}
		case "-c_spider":
			*spiderflag = value
		default:
			logs.Log.Informational("\nCan not contain unknown parameters, required parameters: %v \n optional parameters: %v ", "-c_spider", []string{
				"-a_keyins",
				"-a_limit",
				"-a_outtype",
				"-a_thread",
				"-a_pause",
				"-a_proxyminute",
				"-a_dockercap",
				"-a_success",
				"-a_failure"})
			goto retry
		}
	}
}
