package models

type (
	// Result is a struct for ...
	Result struct {
		Errors []Error `json:"errors"`
	}
	// Error is a struct for ...
	Error struct {
		UserMessage     string
		InternalMessage string
		StatusCode      int
		Code            int
		MoreInfo        string
	}
)
