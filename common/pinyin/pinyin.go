package pinyin

import (
	"regexp"
	"strings"
)

// Meta
const (
	Version   = "0.2.1"
	Author    = "mozillazg, 闲耘"
	License   = "MIT"
	Copyright = "Copyright (c) 2014 mozillazg, 闲耘"
)

// Pinyin style (recommended)
const (
	Normal      = 0 // Normal style, without tones (default style). Such as: pin yin
	Tone        = 1 // tone style 1, phonetic tone in the first letter of the vowel. Such as: pīn yīn
	Tone2       = 2 // tone style 2, that is, phonetic tone after each pinyin, with the number [0-4] said. Such as: pi1n yi1n
	Initials    = 3 // initials style, only return the initial part of each pinyin. Such as: Chinese Pinyin zh g
	FirstLetter = 4 // The first letter style, only the first letter part of the phonetic alphabet. Such as: p y
	Finals      = 5 // Vowel style 1, only return the vowel part of each pinyin, without tones. Such as: ong uo
	FinalsTone  = 6 // Vowel style 2, with tone, tone in the first letter of the finals. Such as: ōng uó
	FinalsTone2 = 7 // Vowel style 2, with tone, tone after each pinyin, with the number [0-4] said. Such as: o1ng uo2
)

// Pinyin style (compatible with previous versions)
const (
	NORMAL       = 0 // Normal style, without tones (default style). Such as: pin yin
	TONE         = 1 // tone style 1, phonetic tone in the first letter of the vowel. Such as: pīn yīn
	TONE2        = 2 // tone style 2, that is, phonetic tone after each pinyin, with the number [0-4] said. Such as: pi1n yi1n
	INITIALS     = 3 // initials style, only return the initial part of each pinyin. Such as: Chinese Pinyin zh g
	FIRST_LETTER = 4 // The first letter style, only the first letter part of the phonetic alphabet. Such as: p y
	FINALS       = 5 // Vowel style 1, only return the vowel part of each pinyin, without tones. Such as: ong uo
	FINALS_TONE  = 6 // Vowel style 2, with tone, tone in the first letter of the finals. Such as: ōng uó
	FINALS_TONE2 = 7 // Vowel style 2, with tone, tone after each pinyin, with the number [0-4] said. Such as: o1ng uo2
)

// initials
var initials = strings.Split(
	"b,p,m,f,d,t,n,l,g,k,h,j,q,x,r,zh,ch,sh,z,c,s",
	",",
)

// all characters with tones
var rePhoneticSymbolSource = func(m map[string]string) string {
	s := ""
	for k := range m {
		s = s + k
	}
	return s
}(phoneticSymbol)

// match the regular expression with a tonal character
var rePhoneticSymbol = regexp.MustCompile("[" + rePhoneticSymbolSource + "]")

// Match a regular expression that uses a character that identifies a tone
var reTone2 = regexp.MustCompile("([aeoiuvnm])([0-4])$")

// Args configuration information
type Args struct {
	Style     int    // Pinyin style (default: Normal)
	Heteronym bool   // enable polyphonic mode (default: disabled)
	Separator string // the delimiter used in Slug (default :-)
}

// default configuration: style
var Style = Normal

// default configuration: whether to enable polyphonic mode
var Heteronym = false

// default configuration: `Slug` join in the use of the separator
var Separator = "-"

// NewArgs returns `Args` containing the default configuration
func NewArgs() Args {
	return Args{Style, Heteronym, Separator}
}

// Get the initials in a single pinyin
func initial(p string) string {
	s := ""
	for _, v := range initials {
		if strings.HasPrefix(p, v) {
			s = v
			break
		}
	}
	return s
}

// Get the finals in a single pinyin
func final(p string) string {
	i := initial(p)
	if i == "" {
		return p
	}
	return strings.Join(strings.SplitN(p, i, 2), "")
}

func toFixed(p string, a Args) string {
	if a.Style == Initials {
		return initial(p)
	}

	// replace the phonetic characters in the phonetic alphabet
	py := rePhoneticSymbol.ReplaceAllStringFunc(p, func(m string) string {
		symbol, _ := phoneticSymbol[m]
		switch a.Style {
		// does not contain tones
		case Normal, FirstLetter, Finals:
			// remove the tone: a1 -> a
			m = reTone2.ReplaceAllString(symbol, "$1")
		case Tone2, FinalsTone2:
			// Returns the character that uses the digit to identify the tone
			m = symbol
		default:
			// the tone is on the head
		}
		return m
	})

	switch a.Style {
	// Initials
	case FirstLetter:
		py = string([]byte(py)[0])
	// The finals
	case Finals, FinalsTone, FinalsTone2:
		py = final(py)
	}
	return py
}

func applyStyle(p []string, a Args) []string {
	newP := []string{}
	for _, v := range p {
		newP = append(newP, toFixed(v, a))
	}
	return newP
}

// SinglePinyin converts a single `rune` type of Chinese characters into pinyin.
func SinglePinyin(r rune, a Args) []string {
	value, ok := PinyinDict[int(r)]
	pys := []string{}
	if ok {
		pys = strings.Split(value, ",")
		if !a.Heteronym {
			pys = strings.Split(value, ",")[:1]
		}
	}
	return applyStyle(pys, a)
}

// Pinyin Chinese characters to Pinyin, support multi-tone mode.
func Pinyin(s string, a Args) [][]string {
	hans := []rune(s)
	pys := [][]string{}
	for _, r := range hans {
		pys = append(pys, SinglePinyin(r, a))
	}
	return pys
}

// LazyPinyin Chinese characters to Pinyin, and `Pinyin` difference is:
// return value type is different, and does not support multi-tone mode, each Chinese word only take the first sound.
func LazyPinyin(s string, a Args) []string {
	a.Heteronym = false
	pys := []string{}
	for _, v := range Pinyin(s, a) {
		pys = append(pys, v[0])
	}
	return pys
}

// Slug join `LazyPinyin` 's return value.
func Slug(s string, a Args) string {
	separator := a.Separator
	return strings.Join(LazyPinyin(s, a), separator)
}
