package mgo

import (
	"time"

	mgo "gopkg.in/mgo.v2"

	"gitlab.com/abibino-lab/mobal/common/pool"
	"gitlab.com/abibino-lab/mobal/config"
	"gitlab.com/abibino-lab/mobal/logs"
)

type MgoSrc struct {
	*mgo.Session
}

var (
	connGcSecond = time.Duration(config.MGO_CONN_GC_SECOND) * 1e9
	session      *mgo.Session
	err          error
	MgoPool      = pool.ClassicPool(
		config.MGO_CONN_CAP,
		config.MGO_CONN_CAP/5,
		func() (pool.Src, error) {
			// if err! = Nil || session.Ping ()! = Nil {
			// session, err = newSession ()
			//}
			return &MgoSrc{session.Clone()}, err
		},
		connGcSecond)
)

func Refresh() {
	session, err = mgo.Dial(config.MGO_CONN_STR)
	if err != nil {
		logs.Log.Error("MongoDB:%v", err)
	} else if err = session.Ping(); err != nil {
		logs.Log.Error("MongoDB:%v", err)
	} else {
		session.SetPoolLimit(config.MGO_CONN_CAP)
	}
}

// determine whether the resource is available
func (mgoSrc *MgoSrc) Usable() bool {
	if mgoSrc.Session == nil || mgoSrc.Session.Ping() != nil {
		return false
	}
	return true
}

// After using the reset method
func (*MgoSrc) Reset() {}

// By the resource pool before the self-destruction method
func (mgoSrc *MgoSrc) Close() {
	if mgoSrc.Session == nil {
		return
	}
	mgoSrc.Session.Close()
}

func Error() error {
	return err
}

// call the resource in the resource pool
func Call(fn func(pool.Src) error) error {
	return MgoPool.Call(fn)
}

// Destroy resource pool
func Close() {
	MgoPool.Close()
}

// returns the current number of resources
func Len() int {
	return MgoPool.Len()
}

// Get all the data
func DatabaseNames() (names []string, err error) {
	err = MgoPool.Call(func(src pool.Src) error {
		names, err = src.(*MgoSrc).DatabaseNames()
		return err
	})
	return
}

// Get the list of database collections
func CollectionNames(dbname string) (names []string, err error) {
	MgoPool.Call(func(src pool.Src) error {
		names, err = src.(*MgoSrc).DB(dbname).CollectionNames()
		return err
	})
	return
}
