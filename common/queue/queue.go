package queue

// Queue :
type Queue struct {
	PoolSize int
	PoolChan chan interface{}
}

// NewQueue :
func NewQueue(size int) *Queue {
	return &Queue{
		PoolSize: size,
		PoolChan: make(chan interface{}, size),
	}
}

// Init :
func (q *Queue) Init(size int) *Queue {
	q.PoolSize = size
	q.PoolChan = make(chan interface{}, size)
	return q
}

// Push :
func (q *Queue) Push(i interface{}) bool {
	if len(q.PoolChan) == q.PoolSize {
		return false
	}
	q.PoolChan <- i
	return true
}

// PushSlice :
func (q *Queue) PushSlice(s []interface{}) {
	for _, i := range s {
		q.Push(i)
	}
}

// Pull :
func (q *Queue) Pull() interface{} {
	return <-q.PoolChan
}

// Exchange : When the Queue instance is used twice, efficient conversion is made based on capacity requirements
func (q *Queue) Exchange(num int) (add int) {
	last := len(q.PoolChan)

	if last >= num {
		add = int(0)
		return
	}

	if q.PoolSize < num {
		pool := []interface{}{}
		for i := 0; i < last; i++ {
			pool = append(pool, <-q.PoolChan)
		}
		// Redefine and assign values
		q.Init(num).PushSlice(pool)
	}

	add = num - last
	return
}
